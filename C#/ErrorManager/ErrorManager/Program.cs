﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace ErrorManager
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read data
            var data = Data.ReadErrorData();
            var results = new Dictionary<string, int>();

            for(int i=1;i<=data.Count;i++)
            {
                var error = data[i].Error;
                ErrorHandler.PerformPreProcessing(ref error);
                if (!results.ContainsKey(error))
                {
                    results.Add(error, 1);
                }
                else
                {
                    results[error]++;
                }
            }
            Data.WriteData(data);

        }


    }
}
