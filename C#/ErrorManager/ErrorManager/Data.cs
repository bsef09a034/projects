﻿using CsvHelper;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ErrorManager
{
    class Data
    {
        public static List<ErrorInput> ReadErrorData()
        {
            var records = new List<ErrorInput>();
            using (var reader = new StreamReader("Data/Errors.csv"))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {

                csv.Read();
                csv.ReadHeader();
                while (csv.Read())
                {
                    var record = new ErrorInput
                    {
                        Error = csv.GetField("Error")
                    };

                    records.Add(record);
                }
            }
            return records;
        }

        public static void WriteData(List<ErrorInput> data)
        {
            var connectionString = "Server=localhost;Database=myDataBase;User=root;Password=;";
            var query = "";
            MySqlHelper.ExecuteNonQuery(connectionString, query);
        }

        public class ErrorInput
        {
            public string Error { get; set; }
        }
    }
}
