﻿using Api.Models;
using ChatSystemApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessageController
    {
        ICommunication communication;

        public MessageController(ICommunication communication)
        {
            this.communication = communication;
        }
        /// <summary>
        /// This message is used to get the message which is present on a topic on nats
        /// </summary>
        /// <returns>Message on nats topic</returns>
        [HttpGet]
        public Message GetMessage()
        {
            return communication.Subscribe("fun");
        }
        
        /// <summary>
        /// This function is end point to publish a toppic on nats
        /// </summary>
        /// <param name="message">Message class object</param>
        [HttpPost]
        public void PostMessage(Message message)
        {
            communication.Publish(message);
        }
    }
}
