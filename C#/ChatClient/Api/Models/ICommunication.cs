﻿using ChatSystemApi.Models;

namespace Api.Models
{
    public interface ICommunication
    {
        void Publish(Message message);
        Message Subscribe(string subject);
    }
}
