﻿using System.Text;
using System.Threading;
using ChatSystemApi.Models;
using NATS.Client;

namespace Api.Models
{
    public class NatsCommunication : ICommunication
    {
        IConnection connection;
        private static int sendIntervalMs = 100;

        /// <summary>
        /// Creates nats connection
        /// </summary>
        public void BuildConnection()
        {
            ConnectionFactory factory = new ConnectionFactory();
            var options = ConnectionFactory.GetDefaultOptions();
            options.Url = "nats://localhost:4222";
            connection =  factory.CreateConnection(options);
        }
        
        /// <summary>
        /// Publish message to Nats server
        /// </summary>
        /// <param name="message">Input message</param>
        public void Publish(Message message)
        {
            BuildConnection();
            byte[] data = Encoding.UTF8.GetBytes($"{message.Author}: {message.MessageContents}");
            connection.Publish("fun", data);
            Thread.Sleep(sendIntervalMs);
            connection.Drain(5000);
            connection.Close();
        }

        /// <summary>
        /// This function is used to check the Nats message on the topic and return it.
        /// </summary>
        /// <param name="subject">Input subject for the message</param>
        /// <returns>Message published on the given subject</returns>
        public Message Subscribe(string subject)
        {
            var message = new Message();
            message.Subject = "fun";

            BuildConnection();
            ISyncSubscription sub = connection.SubscribeSync(message.Subject);
            string data = string.Empty;
            var messageContents = sub.NextMessage();
            if (messageContents != null)
            {
                data += Encoding.UTF8.GetString(messageContents.Data);
            }
            message.MessageContents = data;
            return message;
        }
    }
}
