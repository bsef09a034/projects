﻿namespace ChatSystemApi.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Author { get; set; }
        public string MessageContents { get; set; }
    }
}