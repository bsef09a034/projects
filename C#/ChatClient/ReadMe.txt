Steps to run the project:

1. Run NATs server present in folder in nats-server-v2.6.1-windows-amd64.
2. In ChatClient\ChatClient\bin\Debug\netcoreapp3.1; run two instances of Chatclient.exe. If folder is missing please try to compile by opening solution file, and rebuilding solution, and then perform this step.
3. Enter username different on both instances, and start chatting.
4. Press Q to quit; and the any key to close the screen.
5. Open visual studio and run Api project. 
6. Use this uri to for the Get and Post methods. https://localhost:44321/message/
7. Use Postman or any other client and send message in following format in the request:
{
  "id": 100,
  "subject": "fun",
  "author": "hira",
  "messageContents": "Fun content"
}


Things which can be improved if time allowed:
Logging to console.
Get end point testing
Configurable things like topics etc.
Error handling

Thanks for your time.