﻿namespace ChatClient.Commons
{
    public static class Helper
    {
        /// <summary>
        /// This enum stores loggers types
        /// </summary>
        public enum LoggerTypes
        {
            File,
            Console,
            Sumo
        }

        /// <summary>
        /// This enum is used to store logging level
        /// </summary>
        public enum LoggerLevels
        {
            Error,
            Info,
            Debug
        }
        public static LoggerLevels LoggingLevel;
    }
}
