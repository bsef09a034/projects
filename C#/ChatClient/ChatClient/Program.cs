﻿using Autofac;
using ChatClient.Interfaces;

namespace ChatClient
{
    class Program
    {
        /// <summary>
        /// This function is an starting point of the chat client module
        /// </summary>
        /// <param name="args">default arguments; value is null</param>
        static void Main(string[] args)
        {
            //Setting up config file values
            ContainerConfiguration.ConfigureConfigFile();

            //Setting up IoC using AutoFac; Start of main flow
            var container = ContainerConfiguration.Configure();
            using (var scope = container.BeginLifetimeScope())
            {
                var service = scope.Resolve<IService>();
                service.Start();
            }

        }
    }
}
