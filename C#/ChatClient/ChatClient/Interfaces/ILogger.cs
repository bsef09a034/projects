﻿using System;
using ChatClient.Commons;

namespace ChatClient.Interfaces
{
    public interface ILogger
    {
        public Helper.LoggerTypes Logger { get; set; }
        public void Error(Exception ex, string info="");
        public void Info(string info);
        public void Debug(string info);
    }
}
