﻿using System;
using System.Text;
using System.Threading.Tasks;
using ChatClient.Interfaces;
using NATS.Client;

namespace ChatClient.Models
{
    class Service : IService
    {
        private IConnection connection = null;
        private ILogger logger = null;
        string subject = "fun";
        string userName = "anonymous";

        /// <summary>
        /// Constructor of the class and also responsible to establish nats connection.
        /// </summary>
        public Service(ILogger logger)
        {
            this.logger = logger;
            var connectionFactory = new ConnectionFactory();
            connection = connectionFactory.CreateConnection("localhost:4222");
        }

        /// <summary>
        /// Starting function; in which pub sub is performed.
        /// </summary>
        public void Start()
        {
            Subscribe();
            SetUserData();
            logger.Info("Welcome to chat. Enter Q to quit.");
            Publish();
            Console.ReadKey(true);
        }

        /// <summary>
        /// Function to set username from input
        /// </summary>
        private void SetUserData()
        {
            logger.Info("Please enter user name:");
            userName = Console.ReadLine();
        }


        /// <summary>
        /// Function to subscribe to a topic and receive continues info on the same topic.
        /// </summary>
        public void Subscribe()
        {
            Task.Run(() =>
            {
                ISyncSubscription sub = connection.SubscribeSync(subject);
                while (true)
                {
                    var message = sub.NextMessage();
                    if (message != null)
                    {
                        string data = Encoding.UTF8.GetString(message.Data);
                        LogMessage(data);
                    }
                }
            });
        }


        /// <summary>
        /// Write message to console.
        /// </summary>
        /// <param name="message">message to print</param>
        private void LogMessage(string message)
        {
            logger.Info($"{DateTime.Now.ToString("HH:mm:ss.fffffff")} - {message}");
        }

        /// <summary>
        /// This function is used to publish message to a nats topic; based on user input.
        /// </summary>
        private void Publish()
        {
            bool isContinued = true;
            while (isContinued)
            {
                string message = Console.ReadLine();
                if(message.Trim().ToUpper().Equals("Q"))
                {
                    message = "Leaving chat.";
                    isContinued = false;
                }
                byte[] data = Encoding.UTF8.GetBytes($"{userName}: {message}");
                
                ClearCurrentConsoleLine();
                connection.Publish("fun", data);
            }
        }

        /// <summary>
        /// This function clears last entered line present on console. used it to make conversation more realistic.
        /// </summary>
        public static void ClearCurrentConsoleLine()
        {
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

    }
}
