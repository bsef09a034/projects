﻿using System;
using ChatClient.Commons;
using ChatClient.Interfaces;

namespace ChatClient.Models
{
    /// <summary>
    /// This class is used to log on console
    /// </summary>
    public class ConsoleLogger : ILogger
    {
        public Helper.LoggerTypes Logger { get; set; }
        public ConsoleLogger()
        {
            this.Logger = Helper.LoggerTypes.Console;
        }

        /// <summary>
        /// Log debug level info on console
        /// </summary>
        /// <param name="info">message to log</param>
        public void Debug(string info)
        {
            if(Helper.LoggingLevel.Equals(Helper.LoggerLevels.Debug))
            {
                Console.WriteLine(info);
            }
        }

        /// <summary>
        /// Log error on console
        /// </summary>
        /// <param name="info">message to log</param>
        public void Error(Exception ex, string info="")
        {
            Console.WriteLine($"{ex}. {info}");
        }

        /// <summary>
        /// Log information on console
        /// </summary>
        /// <param name="info">message to log</param>
        public void Info(string info)
        {
            if (Helper.LoggingLevel.Equals(Helper.LoggerLevels.Info))
            {
                Console.WriteLine(info);
            }
        }
    }
}
