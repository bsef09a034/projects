﻿using Autofac;
using ChatClient.Commons;
using ChatClient.Interfaces;
using ChatClient.Models;
using Microsoft.Extensions.Configuration;

namespace ChatClient
{
    public class ContainerConfiguration
    {
        /// <summary>
        /// This function is setting up dependencies and for the entire project.
        /// </summary>
        /// <returns></returns>
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ConsoleLogger>().As<ILogger>();
            builder.RegisterType<Service>().As<IService>();
            return builder.Build();                
        }
        /// <summary>
        /// This function is setting up config file values to be used in project.
        /// </summary>
        public static void ConfigureConfigFile()
        {
            IConfiguration Config = new ConfigurationBuilder()
                .AddJsonFile("AppSettings.json")
                .Build();
            Helper.LoggingLevel = Config.GetSection("LoggingLevel").Value.ToLower() == "info" ? Helper.LoggerLevels.Info :
                                    Config.GetSection("LoggingLevel").Value.ToLower() == "debug" ? Helper.LoggerLevels.Debug : Helper.LoggerLevels.Error;

        }


    }
}
