﻿using System;
using System.IO;
using Autofac;
using ObjectCreator.Commons;
using ObjectCreator.Interfaces;

namespace ObjectCreator
{
    class Program
    {
        /// <summary>
        /// This function is an starting point of the object creation module
        /// </summary>
        /// <param name="args">default arguments; value is null</param>
        static void Main(string[] args)
        {
            //Setting up config file values
            ContainerConfiguration.ConfigureConfigFile();

            //Setting up IoC using AutoFac; Start of main flow
            var container = ContainerConfiguration.Configure();
            using (var scope = container.BeginLifetimeScope())
            {
                var service = scope.Resolve<IObjectBuilder>();
                foreach (var sourceUrl in Helper.SeedUrl.Split(','))
                {
                    service.BuildObject(sourceUrl.Trim());
                }
            }

        }
    }
}
