﻿using System;
using System.Collections.Generic;
using System.Text;
using static ObjectCreator.Commons.Helper;

namespace ObjectCreator.Interfaces
{
    public interface IInputHandler
    {
        public InputTypes InputType { get; set; }
        public string InputString { get; set; }

        public void DownloadInputString(string sourceUrl);
    }
}
