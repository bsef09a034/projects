﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObjectCreator.Interfaces
{
    /// <summary>
    /// Interface for Object Creation
    /// </summary>
    public interface IObjectBuilder
    {
        public void BuildObject(string url);
    }
}
