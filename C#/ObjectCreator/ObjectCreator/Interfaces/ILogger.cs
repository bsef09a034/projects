﻿using System;
using System.Collections.Generic;
using System.Text;
using static ObjectCreator.Commons.Helper;

namespace ObjectCreator.Interfaces
{
    public interface ILogger
    {
        public LoggerTypes Logger { get; set; }
        public void Error(Exception ex, string info="");
        public void Info(string info);
        public void Debug(string info);
    }
}
