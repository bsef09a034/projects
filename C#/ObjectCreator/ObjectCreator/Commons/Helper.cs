﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

namespace ObjectCreator.Commons
{
    public static class Helper
    {
        /// <summary>
        /// This enum stores input types
        /// </summary>
        public enum InputTypes
        {
            Json,
            Xml
        }

        /// <summary>
        /// This enum stores loggers types
        /// </summary>
        public enum LoggerTypes
        {
            File,
            Console,
            Sumo
        }

        /// <summary>
        /// This enum is used to store logging level
        /// </summary>
        public enum LoggerLevels
        {
            Error,
            Info,
            Debug
        }

        /// <summary>
        /// This variable stores runtime dll name contianing c# class. This is populated from AppSettings config file.
        /// </summary>
        public static string RuntimeDllName = "ObjectAssembly.dll";

        /// <summary>
        /// This variable stores runtime dll name contianing c# class. This is populated from AppSettings config file.
        /// </summary>
        public static string SeedUrl { get;  set; }

        public static LoggerLevels LoggingLevel { get; set; }

        public static string ClassCodeCleanerRegex = @"(\[.+?\])|(^\s*(#|//).+?$)|(private System.Collections.Generic.IDictionary<string, object> _additionalProperties[\w\W\s]+?\}\s+\})";

        /// <summary>
        /// This function is used to downlaod data from url.
        /// </summary>
        /// <param name="url">Url to fetch data</param>
        /// <returns>string data received from url</returns>
        public static string ReadData(string url)
        {
            //Downloading data from url
            var data = string.Empty;
            using (var client = new WebClient())
            {
                data = client.DownloadString(url);
            }
            return data;
        }

        /// <summary>
        /// This function is used to read data from file.
        /// </summary>
        /// <param name="path">source file path</param>
        /// <param name="fileName">source file name</param>
        /// <returns></returns>
        public static string ReadData(string path, string fileName)
        {
            //Reading data from file
            return File.ReadAllText(Path.Combine(path,fileName));
        }
    }
}
