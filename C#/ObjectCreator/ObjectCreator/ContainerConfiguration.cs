﻿using Autofac;
using Microsoft.Extensions.Configuration;
using ObjectCreator.Commons;
using ObjectCreator.Interfaces;
using ObjectCreator.Models;

namespace ObjectCreator
{
    public class ContainerConfiguration
    {
        /// <summary>
        /// This function is setting up dependencies and fo rthe entire project.
        /// </summary>
        /// <returns></returns>
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<JsonInputHandler>().As<IInputHandler>();
            builder.RegisterType<CSharpObjectBuilder>().As<IObjectBuilder>();
            builder.RegisterType<ConsoleLogger>().As<ILogger>();
            return builder.Build();                
        }
        /// <summary>
        /// This function is setting up config file values to be used in project.
        /// </summary>
        public static void ConfigureConfigFile()
        {
            IConfiguration Config = new ConfigurationBuilder()
                .AddJsonFile("AppSettings.json")
                .Build();
            Helper.SeedUrl = Config.GetSection("SeedUrl").Value;
            Helper.LoggingLevel = Config.GetSection("LoggingLevel").Value.ToLower() == "info" ? Helper.LoggerLevels.Info : 
                                    Config.GetSection("LoggingLevel").Value.ToLower() == "debug" ? Helper.LoggerLevels.Debug : Helper.LoggerLevels.Error;

        }


    }
}
