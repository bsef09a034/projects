﻿using System;
using System.Collections.Generic;
using System.Text;
using ObjectCreator.Commons;
using ObjectCreator.Interfaces;
using static ObjectCreator.Commons.Helper;

namespace ObjectCreator.Models
{
    /// <summary>
    /// This class is used to log on console
    /// </summary>
    public class ConsoleLogger : ILogger
    {
        public LoggerTypes Logger { get; set; }
        public ConsoleLogger()
        {
            this.Logger = LoggerTypes.Console;
        }

        /// <summary>
        /// Log debug level info on console
        /// </summary>
        /// <param name="info">message to log</param>
        public void Debug(string info)
        {
            if(Helper.LoggingLevel.Equals(Helper.LoggerLevels.Debug))
            {
                Console.WriteLine(info);
            }
        }

        /// <summary>
        /// Log error on console
        /// </summary>
        /// <param name="info">message to log</param>
        public void Error(Exception ex, string info="")
        {
            Console.WriteLine($"{ex}. {info}");
        }

        /// <summary>
        /// Log information on console
        /// </summary>
        /// <param name="info">message to log</param>
        public void Info(string info)
        {
            if (Helper.LoggingLevel.Equals(Helper.LoggerLevels.Info))
            {
                Console.WriteLine(info);
            }
        }
    }
}
