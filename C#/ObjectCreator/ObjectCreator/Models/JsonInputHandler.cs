﻿using System;
using System.Collections.Generic;
using System.Text;
using ObjectCreator.Commons;
using ObjectCreator.Interfaces;
using static ObjectCreator.Commons.Helper;

namespace ObjectCreator.Models
{
    /// <summary>
    /// This class is used to get Json input from the source
    /// </summary>
    internal class JsonInputHandler : IInputHandler
    {
        public InputTypes InputType { get; set; }
        public string InputString { get; set; }

        /// <summary>
        /// This function instantiate reads json data from the url
        /// </summary>
        public void DownloadInputString(string sourceUrl)
        {
            this.InputType = InputTypes.Json;
            this.InputString = Helper.ReadData(sourceUrl);
        }
    }
}
