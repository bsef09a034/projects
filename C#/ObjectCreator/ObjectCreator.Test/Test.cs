using Moq;
using NUnit.Framework;
using ObjectCreator.Interfaces;
using ObjectCreator.Models;

namespace ObjectCreator.Test
{
    public class Tests
    {
        Mock<IInputHandler> InputMock;

        [SetUp]
        public void Setup()
        {
            ContainerConfiguration.ConfigureConfigFile();
            
            InputMock = new Mock<IInputHandler>();
            InputMock.Setup(p => p.InputString).Returns("[{\"userId\": 1,\"id\": 1,\"title\":\"quidem molestiae enim\"}]");
        }

        [Test]
        public void TestClassCreation()
        {
            CSharpObjectBuilder builder = new CSharpObjectBuilder(InputMock.Object, new ConsoleLogger());
            string result = builder.GenerateClass();
            Assert.True(result.Contains("public int UserId") && result.Contains("public int Id") && result.Contains("public string Title"));
        }

        [Test]
        public void TestClassObjectCreation()
        {
            var classCode = @"namespace ObjectCreator
            {   
                public partial class Anonymous 
                {
		            public int UserId { get; set; }   
        
                    public int Id { get; set; }    
        
                    public string Title { get; set; }
	            }
            }";
            var builder = new CSharpObjectBuilder(InputMock.Object, new ConsoleLogger());
            var result = builder.CreateObjects(classCode);
            Assert.True(result[0].UserId == 1  && result[0].Id == 1 && result[0].Title == "quidem molestiae enim");
        }

    }
}