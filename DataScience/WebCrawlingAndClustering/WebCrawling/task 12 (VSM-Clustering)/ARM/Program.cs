﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    partial class Program
    {
        static File obj = new File();
        static List<string> data;
        static Dictionary<string, Node> lexicon=new Dictionary<string,Node> ();
        static List<string> ipValues = new List<string>();
        static List<string> inputValues = new List<string>();
        static List<string> fileNames = new List<string>();
        static Dictionary<string, Doc> Dvector = new Dictionary<string, Doc>();
        static Dictionary<string,double> Lvector = new Dictionary<string,double> ();
        static Dictionary<string, double> Qvector = new Dictionary<string, double>();

        static bool containPage(List<InnerNode> l, string page, ref int ind)
        {
            bool retVal=false;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].pageName == page)
                {
                    retVal = true;
                    ind = i;
                }
            }

            return retVal;
        }
         
        static Dictionary<string, double> idf = new Dictionary<string, double>();
        
        static void computeIDF()
        {
            foreach (var val in ipValues)
            {
                double tempIdf = 0.0;
                double d = (double)Defs.filesCount / (double)lexicon[val].pageList.Count;
                
                tempIdf =Math.Log10(d);
                idf.Add(val, tempIdf);
                Lvector.Add(val, tempIdf);
                Qvector.Add(val, 0);
            }
        }
        static double computeDotProduct(Dictionary<string, double> v1, Dictionary<string, double> v2)
        {
            double dotPro = 0.0;
            foreach (var val in v1)
            {
                dotPro = dotPro + (val.Value * v2[val.Key]);
            }

            return dotPro;

        }
        static double computeVecMagnitude(Dictionary<string, double> v)
        {
            double sum = 0.0;
            foreach (var val in v)
            {
                double temp = val.Value;
                sum = sum + (temp * temp);
            }

            return Math.Sqrt(sum);

        }
        static void populateDvector()
        {
            foreach (var val in fileNames)
            {
                Doc d = new Doc();

                foreach (KeyValuePair<string, double> kvp in Lvector)
                {
                    HelperNode obj = new HelperNode();
                    int ind = -1;
                    if (containPage(lexicon[kvp.Key].pageList, val, ref ind))
                    {
                        obj.tf = lexicon[kvp.Key].pageList[ind].positions.Count;
                    }
                    obj.tfIdf = (double)Lvector[kvp.Key] * (double)obj.tf;
                    d.values.Add(kvp.Key, obj.tfIdf);
                }
                

                Dvector.Add(val, d);
           }
        }
        
        
       
        
        static void userEnd()
        {
           
            computeIDF();
            
            populateDvector();
           
            Console.WriteLine("clustering...");
            makeDocClusters();

        }
        static void populateLexicon(string page)
        {
            data = obj.read(page);



            for (int i = 0; i < data.Count; i++)
            {
                if (lexicon.ContainsKey(data[i].ToLower()))
                {
                    Node temp = lexicon[data[i].ToLower()];
                    temp.totalCount++;
                    int ind = -1;
                    if (containPage(temp.pageList, page, ref ind))
                    {
                        if (!temp.pageList[ind].positions.Contains(i))
                            temp.pageList[ind].positions.Add(i);
                    }
                    else
                    {
                        InnerNode t = new InnerNode();
                        t.pageName = page;
                        t.positions.Add(i);
                        temp.pageList.Add(t);
                    }
                    lexicon[data[i].ToLower()] = temp;
                }
                else
                {
                    Node temp = new Node();
                    temp.totalCount++;
                    InnerNode t = new InnerNode();
                    t.pageName = page;
                    t.positions.Add(i);
                    temp.pageList.Add(t);
                    lexicon.Add(data[i].ToLower(), temp);
                    ipValues.Add(data[i].ToLower());
                }

            }
        }
        static void Main(string[] args)
        {

            try
            {


                for (int i = 1; i <= Defs.filesCount; i++)
                {
                    try
                    {
                        string page = "File" + i.ToString() + ".txt";
                        fileNames.Add(page);
                        populateLexicon(page);
                    }
                    catch { }
                }


                userEnd();

                Console.WriteLine("\n******  ");
            }
            catch (KeyNotFoundException e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
