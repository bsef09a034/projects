Noodles & Company nears completion at Miller Hill Mall | Duluth News Tribune | Duluth, MinnesotaSubscription Services 
The Northland's No. 1 news website — 6,194,570 pageviews in June 2012.
 
Site 
jobsHQ 
carsHQ 
apartmentsHQ 
homesHQ 
Web 
        
Top Searches:  property transactions   matters of record   bankruptcies   obituaries   games   horoscope   dear abby   
Classifieds Marketplace JobsHQ CarsHQ HomesHQ ApartmentsHQ NorthlandOutdoors Agweek 
Home 
Calendar 
Contact 
Email Alerts 
Mobile Alerts 
Newspaper Prints 
RSS Feeds 
Subscribe 
Weather 
News 
carlton county 
Community 
crime 
education 
elections 
health 
investigations 
Iron Range 
Lake County 
politics 
superior 
Wisconsin 
Business 
cirrus 
dta 
Duluth Chamber 
minnesota power 
money 
Sports 
baseball 
basketball 
Brewers 
football 
hockey 
Huskies 
olympics 
preps 
racers calendar 
running 
twins 
Opinion 
columnists 
Local view 
Our view 
readers views 
Life 
Arts and Entertainment 
faces and names 
family 
Home and Garden 
Travel 
Wave 
Arts and Entertainment 
Best bets 
books 
food 
Movies 
restaurants 
Reviews 
Outdoors 
DNR 
fishing 
hunting 
Milestones 
anniversaries 
Births 
celebrations 
engagements 
Special Occasions 
Weddings 
Scrapbook 
Ask an Expert 
books 
Community 
family 
volunteer groups 
Obituaries 
memorials 
Blogs 
Budgeteer 
Budge Business 
Budge Community 
Budge News 
Budge Opinion 
Budge Sports 

 Print      E-mail 
Published July 26, 2012, 12:00 AM 
Noodles & Company nears completion at Miller Hill Mall
The Noodles & Company restaurant planned for Miller Hill Mall looks boarded up and abandoned from the outside. But the work is back on track. 
By: Candace Renalls, Duluth News Tribune 

 Talk about it 
The Noodles & Company restaurant planned for Miller Hill Mall looks boarded up and abandoned from the outside.
Indeed, construction did stop for a time when plans needed revision. But the work is back on track with the eatery specializing in pasta dishes scheduled to open in mid-August, according to Machelle Kendrick, the mallâ€™s marketing director.
â€œThe interior work has been in full swing for the last few weeks again,â€ said Mall Manager Katie Altrichter.
The fast-growing chain based in Broomfield, Colo., serves noodles, pastas, salad, soups and sandwiches from Asia, the Mediterranean and America, using fresh, healthy ingredients. In fast-casual style, diners order at a counter and are served at tables.
Founded in 1995, the chain recently opened its 300th store, with most company-owned. The Duluth restaurant will be its 30th location in Minnesota and its first for Northern Minnesota. Most of its Minnesota locations are in the Twin Cities area.
The eatery will fill 2,700 square feet of space facing Miller Trunk Highway, formerly occupied by the Great American Bar & Grill, which closed last year. The pasta chain was already seeking job applicants online in January when news of its plans for a Duluth store broke.
OTHER MALL NEWS
Mall officials announced Wednesday that a new womenâ€™s clothing store will open at the mall next month.
Torrid, which offers trendy clothing for plus-sized young women, will open on Aug. 29 next to Hot Topic and near the Caribou Coffee kiosk. The 2,230-square-foot store will offer stylish clothing for women sizes 12 to 32.
The chain, owned by Hot Topic, has more than 170 stores in the United States. 
Meanwhile, plans for a tea shop are also moving forward at the mall. Teavana, which sells high quality teas and serving accessories, plans to open a 500-square-foot store by the Center Court in late 2012. The tea chain, with 150 stores in the United States and Mexico, donates 1 percent of its annual profits to CARE, a humanitarian group fighting poverty.
Tags: business, news, entertainment, restaurants, food 
TOP ADS
RON'S HANDYMAN SERVICE
WWW.OAKLAKERV.COM
SEASONED Oak full loggers cord $180. 729-5627
CARLSON LAWN & LANDSCAPING
QWIC ELECTRIC: For all your electric needs
Showing 5 of 29 » Show All 
Most Read 
Most E-mailed 
Koochiching County grand jury indicts Muggli in wife's totem pole death 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Manager: Utility company van stolen in Superior, stops in Duluth 
Our view: Bayfield traffic tragedy offers chilling reminder 
Minnesota man charged with burrito assault 
Minnesota man charged with burrito assault 
Duluth housing market on the rebound 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Hibbing natives see Olympic dream working as volunteers 
Hot trend hits Duluth streets as entrepreneurs feed customers on the go 
  
Sections 
Home News Business Sports Opinion Life Wave Outdoors Milestones Scrapbook Obituaries Blogs Budgeteer 
About 
Advertising Information Contact Us FCC Careers Privacy Statement Terms & Conditions 
Tools & Features 
Calendar Mobile Deals Email Alerts  RSS Feeds Archive Video Photo Galleries Mobile Site 
FCC Digital Network 
Jobshq.com Carshq.com Apartmentshq.com Homeshq.com Northlandoutdoors.com Agweek.com Areavoices.com 
Duluth News Tribune 424 W. First St., Duluth, Minnesota 55802 | Phone: (218) 723-5281
© 2012 Forum Communications Co. — All rights reserved
Noodles & Company nears completion at Miller Hill Mall | Duluth News Tribune | Duluth, Minnesota
Noodles & Company nears completion at Miller Hill Mall | Duluth News Tribune | Duluth, Minnesota
Subscription Services 
The Northland's No. 1 news website — 6,194,570 pageviews in June 2012.
 
Site 
jobsHQ 
carsHQ 
apartmentsHQ 
homesHQ 
Web 
        
Top Searches:  property transactions   matters of record   bankruptcies   obituaries   games   horoscope   dear abby   
Classifieds Marketplace JobsHQ CarsHQ HomesHQ ApartmentsHQ NorthlandOutdoors Agweek 
Home 
Calendar 
Contact 
Email Alerts 
Mobile Alerts 
Newspaper Prints 
RSS Feeds 
Subscribe 
Weather 
News 
carlton county 
Community 
crime 
education 
elections 
health 
investigations 
Iron Range 
Lake County 
politics 
superior 
Wisconsin 
Business 
cirrus 
dta 
Duluth Chamber 
minnesota power 
money 
Sports 
baseball 
basketball 
Brewers 
football 
hockey 
Huskies 
olympics 
preps 
racers calendar 
running 
twins 
Opinion 
columnists 
Local view 
Our view 
readers views 
Life 
Arts and Entertainment 
faces and names 
family 
Home and Garden 
Travel 
Wave 
Arts and Entertainment 
Best bets 
books 
food 
Movies 
restaurants 
Reviews 
Outdoors 
DNR 
fishing 
hunting 
Milestones 
anniversaries 
Births 
celebrations 
engagements 
Special Occasions 
Weddings 
Scrapbook 
Ask an Expert 
books 
Community 
family 
volunteer groups 
Obituaries 
memorials 
Blogs 
Budgeteer 
Budge Business 
Budge Community 
Budge News 
Budge Opinion 
Budge Sports 

 Print      E-mail 
Published July 26, 2012, 12:00 AM 
Noodles & Company nears completion at Miller Hill Mall
The Noodles & Company restaurant planned for Miller Hill Mall looks boarded up and abandoned from the outside. But the work is back on track. 
By: Candace Renalls, Duluth News Tribune 

 Talk about it 
The Noodles & Company restaurant planned for Miller Hill Mall looks boarded up and abandoned from the outside.
Indeed, construction did stop for a time when plans needed revision. But the work is back on track with the eatery specializing in pasta dishes scheduled to open in mid-August, according to Machelle Kendrick, the mallâ€™s marketing director.
â€œThe interior work has been in full swing for the last few weeks again,â€ said Mall Manager Katie Altrichter.
The fast-growing chain based in Broomfield, Colo., serves noodles, pastas, salad, soups and sandwiches from Asia, the Mediterranean and America, using fresh, healthy ingredients. In fast-casual style, diners order at a counter and are served at tables.
Founded in 1995, the chain recently opened its 300th store, with most company-owned. The Duluth restaurant will be its 30th location in Minnesota and its first for Northern Minnesota. Most of its Minnesota locations are in the Twin Cities area.
The eatery will fill 2,700 square feet of space facing Miller Trunk Highway, formerly occupied by the Great American Bar & Grill, which closed last year. The pasta chain was already seeking job applicants online in January when news of its plans for a Duluth store broke.
OTHER MALL NEWS
Mall officials announced Wednesday that a new womenâ€™s clothing store will open at the mall next month.
Torrid, which offers trendy clothing for plus-sized young women, will open on Aug. 29 next to Hot Topic and near the Caribou Coffee kiosk. The 2,230-square-foot store will offer stylish clothing for women sizes 12 to 32.
The chain, owned by Hot Topic, has more than 170 stores in the United States. 
Meanwhile, plans for a tea shop are also moving forward at the mall. Teavana, which sells high quality teas and serving accessories, plans to open a 500-square-foot store by the Center Court in late 2012. The tea chain, with 150 stores in the United States and Mexico, donates 1 percent of its annual profits to CARE, a humanitarian group fighting poverty.
Tags: business, news, entertainment, restaurants, food 
TOP ADS
RON'S HANDYMAN SERVICE
WWW.OAKLAKERV.COM
SEASONED Oak full loggers cord $180. 729-5627
CARLSON LAWN & LANDSCAPING
QWIC ELECTRIC: For all your electric needs
Showing 5 of 29 » Show All 
Most Read 
Most E-mailed 
Koochiching County grand jury indicts Muggli in wife's totem pole death 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Manager: Utility company van stolen in Superior, stops in Duluth 
Our view: Bayfield traffic tragedy offers chilling reminder 
Minnesota man charged with burrito assault 
Minnesota man charged with burrito assault 
Duluth housing market on the rebound 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Hibbing natives see Olympic dream working as volunteers 
Hot trend hits Duluth streets as entrepreneurs feed customers on the go 
  
Sections 
Home News Business Sports Opinion Life Wave Outdoors Milestones Scrapbook Obituaries Blogs Budgeteer 
About 
Advertising Information Contact Us FCC Careers Privacy Statement Terms & Conditions 
Tools & Features 
Calendar Mobile Deals Email Alerts  RSS Feeds Archive Video Photo Galleries Mobile Site 
FCC Digital Network 
Jobshq.com Carshq.com Apartmentshq.com Homeshq.com Northlandoutdoors.com Agweek.com Areavoices.com 
Duluth News Tribune 424 W. First St., Duluth, Minnesota 55802 | Phone: (218) 723-5281
© 2012 Forum Communications Co. — All rights reserved
Subscription Services 
The Northland's No. 1 news website — 6,194,570 pageviews in June 2012.
 
Site 
jobsHQ 
carsHQ 
apartmentsHQ 
homesHQ 
Web 
        
Top Searches:  property transactions   matters of record   bankruptcies   obituaries   games   horoscope   dear abby   
Classifieds Marketplace JobsHQ CarsHQ HomesHQ ApartmentsHQ NorthlandOutdoors Agweek 
Home 
Calendar 
Contact 
Email Alerts 
Mobile Alerts 
Newspaper Prints 
RSS Feeds 
Subscribe 
Weather 
News 
carlton county 
Community 
crime 
education 
elections 
health 
investigations 
Iron Range 
Lake County 
politics 
superior 
Wisconsin 
Business 
cirrus 
dta 
Duluth Chamber 
minnesota power 
money 
Sports 
baseball 
basketball 
Brewers 
football 
hockey 
Huskies 
olympics 
preps 
racers calendar 
running 
twins 
Opinion 
columnists 
Local view 
Our view 
readers views 
Life 
Arts and Entertainment 
faces and names 
family 
Home and Garden 
Travel 
Wave 
Arts and Entertainment 
Best bets 
books 
food 
Movies 
restaurants 
Reviews 
Outdoors 
DNR 
fishing 
hunting 
Milestones 
anniversaries 
Births 
celebrations 
engagements 
Special Occasions 
Weddings 
Scrapbook 
Ask an Expert 
books 
Community 
family 
volunteer groups 
Obituaries 
memorials 
Blogs 
Budgeteer 
Budge Business 
Budge Community 
Budge News 
Budge Opinion 
Budge Sports 
Subscription Services 
The Northland's No. 1 news website — 6,194,570 pageviews in June 2012.
Subscription Services
The Northland's No. 1 news website — 6,194,570 pageviews in June 2012.
The Northland's No. 1 news website — 6,194,570 pageviews in June 2012.
The Northland's No. 1 news website — 6,194,570 pageviews in June 2012.
 
Site 
jobsHQ 
carsHQ 
apartmentsHQ 
homesHQ 
Web 
        
Top Searches:  property transactions   matters of record   bankruptcies   obituaries   games   horoscope   dear abby   
Classifieds Marketplace JobsHQ CarsHQ HomesHQ ApartmentsHQ NorthlandOutdoors Agweek 
Site 
jobsHQ 
carsHQ 
apartmentsHQ 
homesHQ 
Web 
        
Top Searches:  property transactions   matters of record   bankruptcies   obituaries   games   horoscope   dear abby   
Site 
jobsHQ 
carsHQ 
apartmentsHQ 
homesHQ 
Web 
Site 
jobsHQ 
carsHQ 
apartmentsHQ 
homesHQ 
Web 
Site 
Site
jobsHQ 
jobsHQ
carsHQ 
carsHQ
apartmentsHQ 
apartmentsHQ
homesHQ 
homesHQ
Web 
Web
        
        
Top Searches:  property transactions   matters of record   bankruptcies   obituaries   games   horoscope   dear abby   
Top Searches: 
property transactions
matters of record
bankruptcies
obituaries
games
horoscope
dear abby
Classifieds Marketplace JobsHQ CarsHQ HomesHQ ApartmentsHQ NorthlandOutdoors Agweek 
Classifieds
Marketplace
JobsHQ
CarsHQ
HomesHQ
ApartmentsHQ
NorthlandOutdoors
Agweek
Home 
Calendar 
Contact 
Email Alerts 
Mobile Alerts 
Newspaper Prints 
RSS Feeds 
Subscribe 
Weather 
News 
carlton county 
Community 
crime 
education 
elections 
health 
investigations 
Iron Range 
Lake County 
politics 
superior 
Wisconsin 
Business 
cirrus 
dta 
Duluth Chamber 
minnesota power 
money 
Sports 
baseball 
basketball 
Brewers 
football 
hockey 
Huskies 
olympics 
preps 
racers calendar 
running 
twins 
Opinion 
columnists 
Local view 
Our view 
readers views 
Life 
Arts and Entertainment 
faces and names 
family 
Home and Garden 
Travel 
Wave 
Arts and Entertainment 
Best bets 
books 
food 
Movies 
restaurants 
Reviews 
Outdoors 
DNR 
fishing 
hunting 
Milestones 
anniversaries 
Births 
celebrations 
engagements 
Special Occasions 
Weddings 
Scrapbook 
Ask an Expert 
books 
Community 
family 
volunteer groups 
Obituaries 
memorials 
Blogs 
Budgeteer 
Budge Business 
Budge Community 
Budge News 
Budge Opinion 
Budge Sports 
Home 
Calendar 
Contact 
Email Alerts 
Mobile Alerts 
Newspaper Prints 
RSS Feeds 
Subscribe 
Weather 
News 
carlton county 
Community 
crime 
education 
elections 
health 
investigations 
Iron Range 
Lake County 
politics 
superior 
Wisconsin 
Business 
cirrus 
dta 
Duluth Chamber 
minnesota power 
money 
Sports 
baseball 
basketball 
Brewers 
football 
hockey 
Huskies 
olympics 
preps 
racers calendar 
running 
twins 
Opinion 
columnists 
Local view 
Our view 
readers views 
Life 
Arts and Entertainment 
faces and names 
family 
Home and Garden 
Travel 
Wave 
Arts and Entertainment 
Best bets 
books 
food 
Movies 
restaurants 
Reviews 
Outdoors 
DNR 
fishing 
hunting 
Milestones 
anniversaries 
Births 
celebrations 
engagements 
Special Occasions 
Weddings 
Scrapbook 
Ask an Expert 
books 
Community 
family 
volunteer groups 
Obituaries 
memorials 
Blogs 
Budgeteer 
Budge Business 
Budge Community 
Budge News 
Budge Opinion 
Budge Sports 
Home 
Calendar 
Contact 
Email Alerts 
Mobile Alerts 
Newspaper Prints 
RSS Feeds 
Subscribe 
Weather 
Home
Calendar 
Contact 
Email Alerts 
Mobile Alerts 
Newspaper Prints 
RSS Feeds 
Subscribe 
Weather 
Calendar 
Contact 
Email Alerts 
Mobile Alerts 
Newspaper Prints 
RSS Feeds 
Subscribe 
Weather 
Calendar 
Calendar
Contact 
Contact
Email Alerts 
Email Alerts
Mobile Alerts 
Mobile Alerts
Newspaper Prints 
Newspaper Prints
RSS Feeds 
RSS Feeds
Subscribe 
Subscribe
Weather 
Weather
News 
carlton county 
Community 
crime 
education 
elections 
health 
investigations 
Iron Range 
Lake County 
politics 
superior 
Wisconsin 
News
carlton county 
Community 
crime 
education 
elections 
health 
investigations 
Iron Range 
Lake County 
politics 
superior 
Wisconsin 
carlton county 
Community 
crime 
education 
elections 
health 
investigations 
Iron Range 
carlton county 
carlton county
Community 
Community
crime 
crime
education 
education
elections 
elections
health 
health
investigations 
investigations
Iron Range 
Iron Range
Lake County 
politics 
superior 
Wisconsin 
Lake County 
Lake County
politics 
politics
superior 
superior
Wisconsin 
Wisconsin
Business 
cirrus 
dta 
Duluth Chamber 
minnesota power 
money 
Business
cirrus 
dta 
Duluth Chamber 
minnesota power 
money 
cirrus 
dta 
Duluth Chamber 
minnesota power 
money 
cirrus 
cirrus
dta 
dta
Duluth Chamber 
Duluth Chamber
minnesota power 
minnesota power
money 
money
Sports 
baseball 
basketball 
Brewers 
football 
hockey 
Huskies 
olympics 
preps 
racers calendar 
running 
twins 
Sports
baseball 
basketball 
Brewers 
football 
hockey 
Huskies 
olympics 
preps 
racers calendar 
running 
twins 
baseball 
basketball 
Brewers 
football 
hockey 
Huskies 
olympics 
preps 
baseball 
baseball
basketball 
basketball
Brewers 
Brewers
football 
football
hockey 
hockey
Huskies 
Huskies
olympics 
olympics
preps 
preps
racers calendar 
running 
twins 
racers calendar 
racers calendar
running 
running
twins 
twins
Opinion 
columnists 
Local view 
Our view 
readers views 
Opinion
columnists 
Local view 
Our view 
readers views 
columnists 
Local view 
Our view 
readers views 
columnists 
columnists
Local view 
Local view
Our view 
Our view
readers views 
readers views
Life 
Arts and Entertainment 
faces and names 
family 
Home and Garden 
Travel 
Life
Arts and Entertainment 
faces and names 
family 
Home and Garden 
Travel 
Arts and Entertainment 
faces and names 
family 
Home and Garden 
Travel 
Arts and Entertainment 
Arts and Entertainment
faces and names 
faces and names
family 
family
Home and Garden 
Home and Garden
Travel 
Travel
Wave 
Arts and Entertainment 
Best bets 
books 
food 
Movies 
restaurants 
Reviews 
Wave
Arts and Entertainment 
Best bets 
books 
food 
Movies 
restaurants 
Reviews 
Arts and Entertainment 
Best bets 
books 
food 
Movies 
restaurants 
Reviews 
Arts and Entertainment 
Arts and Entertainment
Best bets 
Best bets
books 
books
food 
food
Movies 
Movies
restaurants 
restaurants
Reviews 
Reviews
Outdoors 
DNR 
fishing 
hunting 
Outdoors
DNR 
fishing 
hunting 
DNR 
fishing 
hunting 
DNR 
DNR
fishing 
fishing
hunting 
hunting
Milestones 
anniversaries 
Births 
celebrations 
engagements 
Special Occasions 
Weddings 
Milestones
anniversaries 
Births 
celebrations 
engagements 
Special Occasions 
Weddings 
anniversaries 
Births 
celebrations 
engagements 
Special Occasions 
Weddings 
anniversaries 
anniversaries
Births 
Births
celebrations 
celebrations
engagements 
engagements
Special Occasions 
Special Occasions
Weddings 
Weddings
Scrapbook 
Ask an Expert 
books 
Community 
family 
volunteer groups 
Scrapbook
Ask an Expert 
books 
Community 
family 
volunteer groups 
Ask an Expert 
books 
Community 
family 
volunteer groups 
Ask an Expert 
Ask an Expert
books 
books
Community 
Community
family 
family
volunteer groups 
volunteer groups
Obituaries 
memorials 
Obituaries
memorials 
memorials 
memorials 
memorials
Blogs 
Blogs
Budgeteer 
Budge Business 
Budge Community 
Budge News 
Budge Opinion 
Budge Sports 
Budgeteer
Budge Business 
Budge Community 
Budge News 
Budge Opinion 
Budge Sports 
Budge Business 
Budge Community 
Budge News 
Budge Opinion 
Budge Sports 
Budge Business 
Budge Business
Budge Community 
Budge Community
Budge News 
Budge News
Budge Opinion 
Budge Opinion
Budge Sports 
Budge Sports
 Print      E-mail 
Published July 26, 2012, 12:00 AM 
Noodles & Company nears completion at Miller Hill Mall
The Noodles & Company restaurant planned for Miller Hill Mall looks boarded up and abandoned from the outside. But the work is back on track. 
By: Candace Renalls, Duluth News Tribune 

 Talk about it 
The Noodles & Company restaurant planned for Miller Hill Mall looks boarded up and abandoned from the outside.
Indeed, construction did stop for a time when plans needed revision. But the work is back on track with the eatery specializing in pasta dishes scheduled to open in mid-August, according to Machelle Kendrick, the mallâ€™s marketing director.
â€œThe interior work has been in full swing for the last few weeks again,â€ said Mall Manager Katie Altrichter.
The fast-growing chain based in Broomfield, Colo., serves noodles, pastas, salad, soups and sandwiches from Asia, the Mediterranean and America, using fresh, healthy ingredients. In fast-casual style, diners order at a counter and are served at tables.
Founded in 1995, the chain recently opened its 300th store, with most company-owned. The Duluth restaurant will be its 30th location in Minnesota and its first for Northern Minnesota. Most of its Minnesota locations are in the Twin Cities area.
The eatery will fill 2,700 square feet of space facing Miller Trunk Highway, formerly occupied by the Great American Bar & Grill, which closed last year. The pasta chain was already seeking job applicants online in January when news of its plans for a Duluth store broke.
OTHER MALL NEWS
Mall officials announced Wednesday that a new womenâ€™s clothing store will open at the mall next month.
Torrid, which offers trendy clothing for plus-sized young women, will open on Aug. 29 next to Hot Topic and near the Caribou Coffee kiosk. The 2,230-square-foot store will offer stylish clothing for women sizes 12 to 32.
The chain, owned by Hot Topic, has more than 170 stores in the United States. 
Meanwhile, plans for a tea shop are also moving forward at the mall. Teavana, which sells high quality teas and serving accessories, plans to open a 500-square-foot store by the Center Court in late 2012. The tea chain, with 150 stores in the United States and Mexico, donates 1 percent of its annual profits to CARE, a humanitarian group fighting poverty.
Tags: business, news, entertainment, restaurants, food 
TOP ADS
RON'S HANDYMAN SERVICE
WWW.OAKLAKERV.COM
SEASONED Oak full loggers cord $180. 729-5627
CARLSON LAWN & LANDSCAPING
QWIC ELECTRIC: For all your electric needs
Showing 5 of 29 » Show All 
Most Read 
Most E-mailed 
Koochiching County grand jury indicts Muggli in wife's totem pole death 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Manager: Utility company van stolen in Superior, stops in Duluth 
Our view: Bayfield traffic tragedy offers chilling reminder 
Minnesota man charged with burrito assault 
Minnesota man charged with burrito assault 
Duluth housing market on the rebound 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Hibbing natives see Olympic dream working as volunteers 
Hot trend hits Duluth streets as entrepreneurs feed customers on the go 
 Print      E-mail 
Published July 26, 2012, 12:00 AM 
Noodles & Company nears completion at Miller Hill Mall
The Noodles & Company restaurant planned for Miller Hill Mall looks boarded up and abandoned from the outside. But the work is back on track. 
By: Candace Renalls, Duluth News Tribune 

 Talk about it 
The Noodles & Company restaurant planned for Miller Hill Mall looks boarded up and abandoned from the outside.
Indeed, construction did stop for a time when plans needed revision. But the work is back on track with the eatery specializing in pasta dishes scheduled to open in mid-August, according to Machelle Kendrick, the mallâ€™s marketing director.
â€œThe interior work has been in full swing for the last few weeks again,â€ said Mall Manager Katie Altrichter.
The fast-growing chain based in Broomfield, Colo., serves noodles, pastas, salad, soups and sandwiches from Asia, the Mediterranean and America, using fresh, healthy ingredients. In fast-casual style, diners order at a counter and are served at tables.
Founded in 1995, the chain recently opened its 300th store, with most company-owned. The Duluth restaurant will be its 30th location in Minnesota and its first for Northern Minnesota. Most of its Minnesota locations are in the Twin Cities area.
The eatery will fill 2,700 square feet of space facing Miller Trunk Highway, formerly occupied by the Great American Bar & Grill, which closed last year. The pasta chain was already seeking job applicants online in January when news of its plans for a Duluth store broke.
OTHER MALL NEWS
Mall officials announced Wednesday that a new womenâ€™s clothing store will open at the mall next month.
Torrid, which offers trendy clothing for plus-sized young women, will open on Aug. 29 next to Hot Topic and near the Caribou Coffee kiosk. The 2,230-square-foot store will offer stylish clothing for women sizes 12 to 32.
The chain, owned by Hot Topic, has more than 170 stores in the United States. 
Meanwhile, plans for a tea shop are also moving forward at the mall. Teavana, which sells high quality teas and serving accessories, plans to open a 500-square-foot store by the Center Court in late 2012. The tea chain, with 150 stores in the United States and Mexico, donates 1 percent of its annual profits to CARE, a humanitarian group fighting poverty.
Tags: business, news, entertainment, restaurants, food 
 Print      E-mail 
Published July 26, 2012, 12:00 AM 
Noodles & Company nears completion at Miller Hill Mall
The Noodles & Company restaurant planned for Miller Hill Mall looks boarded up and abandoned from the outside. But the work is back on track. 
By: Candace Renalls, Duluth News Tribune 
 Print      E-mail 
Print
E-mail
Noodles & Company nears completion at Miller Hill Mall
By: Candace Renalls, Duluth News Tribune 
Candace Renalls
Candace Renalls
 Talk about it 
The Noodles & Company restaurant planned for Miller Hill Mall looks boarded up and abandoned from the outside.
Indeed, construction did stop for a time when plans needed revision. But the work is back on track with the eatery specializing in pasta dishes scheduled to open in mid-August, according to Machelle Kendrick, the mallâ€™s marketing director.
â€œThe interior work has been in full swing for the last few weeks again,â€ said Mall Manager Katie Altrichter.
The fast-growing chain based in Broomfield, Colo., serves noodles, pastas, salad, soups and sandwiches from Asia, the Mediterranean and America, using fresh, healthy ingredients. In fast-casual style, diners order at a counter and are served at tables.
Founded in 1995, the chain recently opened its 300th store, with most company-owned. The Duluth restaurant will be its 30th location in Minnesota and its first for Northern Minnesota. Most of its Minnesota locations are in the Twin Cities area.
The eatery will fill 2,700 square feet of space facing Miller Trunk Highway, formerly occupied by the Great American Bar & Grill, which closed last year. The pasta chain was already seeking job applicants online in January when news of its plans for a Duluth store broke.
OTHER MALL NEWS
Mall officials announced Wednesday that a new womenâ€™s clothing store will open at the mall next month.
Torrid, which offers trendy clothing for plus-sized young women, will open on Aug. 29 next to Hot Topic and near the Caribou Coffee kiosk. The 2,230-square-foot store will offer stylish clothing for women sizes 12 to 32.
The chain, owned by Hot Topic, has more than 170 stores in the United States. 
Meanwhile, plans for a tea shop are also moving forward at the mall. Teavana, which sells high quality teas and serving accessories, plans to open a 500-square-foot store by the Center Court in late 2012. The tea chain, with 150 stores in the United States and Mexico, donates 1 percent of its annual profits to CARE, a humanitarian group fighting poverty.
Tags: business, news, entertainment, restaurants, food 
 Talk about it 
 Talk about it 
 Talk about it 
 Talk about it
Talk about it
Talk about it
The Noodles & Company restaurant planned for Miller Hill Mall looks boarded up and abandoned from the outside.
Indeed, construction did stop for a time when plans needed revision. But the work is back on track with the eatery specializing in pasta dishes scheduled to open in mid-August, according to Machelle Kendrick, the mallâ€™s marketing director.
â€œThe interior work has been in full swing for the last few weeks again,â€ said Mall Manager Katie Altrichter.
The fast-growing chain based in Broomfield, Colo., serves noodles, pastas, salad, soups and sandwiches from Asia, the Mediterranean and America, using fresh, healthy ingredients. In fast-casual style, diners order at a counter and are served at tables.
Founded in 1995, the chain recently opened its 300th store, with most company-owned. The Duluth restaurant will be its 30th location in Minnesota and its first for Northern Minnesota. Most of its Minnesota locations are in the Twin Cities area.
The eatery will fill 2,700 square feet of space facing Miller Trunk Highway, formerly occupied by the Great American Bar & Grill, which closed last year. The pasta chain was already seeking job applicants online in January when news of its plans for a Duluth store broke.
OTHER MALL NEWS
OTHER MALL NEWS
Mall officials announced Wednesday that a new womenâ€™s clothing store will open at the mall next month.
Torrid, which offers trendy clothing for plus-sized young women, will open on Aug. 29 next to Hot Topic and near the Caribou Coffee kiosk. The 2,230-square-foot store will offer stylish clothing for women sizes 12 to 32.
The chain, owned by Hot Topic, has more than 170 stores in the United States. 
Meanwhile, plans for a tea shop are also moving forward at the mall. Teavana, which sells high quality teas and serving accessories, plans to open a 500-square-foot store by the Center Court in late 2012. The tea chain, with 150 stores in the United States and Mexico, donates 1 percent of its annual profits to CARE, a humanitarian group fighting poverty.
Tags: business, news, entertainment, restaurants, food 
Tags:
business
news
entertainment
restaurants
food
TOP ADS
RON'S HANDYMAN SERVICE
WWW.OAKLAKERV.COM
SEASONED Oak full loggers cord $180. 729-5627
CARLSON LAWN & LANDSCAPING
QWIC ELECTRIC: For all your electric needs
Showing 5 of 29 » Show All 
TOP ADS
RON'S HANDYMAN SERVICE
WWW.OAKLAKERV.COM
SEASONED Oak full loggers cord $180. 729-5627
CARLSON LAWN & LANDSCAPING
QWIC ELECTRIC: For all your electric needs
Showing 5 of 29 » Show All 
TOP ADS
RON'S HANDYMAN SERVICE
WWW.OAKLAKERV.COM
SEASONED Oak full loggers cord $180. 729-5627
CARLSON LAWN & LANDSCAPING
QWIC ELECTRIC: For all your electric needs
Showing 5 of 29 » Show All 
RON'S HANDYMAN SERVICE
RON'S HANDYMAN SERVICE
WWW.OAKLAKERV.COM
WWW.OAKLAKERV.COM
SEASONED Oak full loggers cord $180. 729-5627
SEASONED Oak full loggers cord $180. 729-5627
CARLSON LAWN & LANDSCAPING
CARLSON LAWN & LANDSCAPING
QWIC ELECTRIC: For all your electric needs
QWIC ELECTRIC: For all your electric needs
Showing 5 of 29 » Show All 
» Show All
Most Read 
Most E-mailed 
Koochiching County grand jury indicts Muggli in wife's totem pole death 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Manager: Utility company van stolen in Superior, stops in Duluth 
Our view: Bayfield traffic tragedy offers chilling reminder 
Minnesota man charged with burrito assault 
Minnesota man charged with burrito assault 
Duluth housing market on the rebound 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Hibbing natives see Olympic dream working as volunteers 
Hot trend hits Duluth streets as entrepreneurs feed customers on the go 
Most Read 
Most E-mailed 
Koochiching County grand jury indicts Muggli in wife's totem pole death 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Manager: Utility company van stolen in Superior, stops in Duluth 
Our view: Bayfield traffic tragedy offers chilling reminder 
Minnesota man charged with burrito assault 
Minnesota man charged with burrito assault 
Duluth housing market on the rebound 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Hibbing natives see Olympic dream working as volunteers 
Hot trend hits Duluth streets as entrepreneurs feed customers on the go 
Most Read 
Most E-mailed 
Koochiching County grand jury indicts Muggli in wife's totem pole death 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Manager: Utility company van stolen in Superior, stops in Duluth 
Our view: Bayfield traffic tragedy offers chilling reminder 
Minnesota man charged with burrito assault 
Minnesota man charged with burrito assault 
Duluth housing market on the rebound 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Hibbing natives see Olympic dream working as volunteers 
Hot trend hits Duluth streets as entrepreneurs feed customers on the go 
Most Read 
Most E-mailed 
Most Read 
Most E-mailed 
Most Read 
Most Read
Most E-mailed 
Most E-mailed
Koochiching County grand jury indicts Muggli in wife's totem pole death 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Manager: Utility company van stolen in Superior, stops in Duluth 
Our view: Bayfield traffic tragedy offers chilling reminder 
Minnesota man charged with burrito assault 
Koochiching County grand jury indicts Muggli in wife's totem pole death 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Manager: Utility company van stolen in Superior, stops in Duluth 
Our view: Bayfield traffic tragedy offers chilling reminder 
Minnesota man charged with burrito assault 
Koochiching County grand jury indicts Muggli in wife's totem pole death 
Koochiching County grand jury indicts Muggli in wife's totem pole death
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Fast friends: Olympic teammates Goucher, Flanagan push each other
Manager: Utility company van stolen in Superior, stops in Duluth 
Manager: Utility company van stolen in Superior, stops in Duluth
Our view: Bayfield traffic tragedy offers chilling reminder 
Our view: Bayfield traffic tragedy offers chilling reminder
Minnesota man charged with burrito assault 
Minnesota man charged with burrito assault 
Minnesota man charged with burrito assault 
Duluth housing market on the rebound 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Hibbing natives see Olympic dream working as volunteers 
Hot trend hits Duluth streets as entrepreneurs feed customers on the go 
Minnesota man charged with burrito assault 
Duluth housing market on the rebound 
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Hibbing natives see Olympic dream working as volunteers 
Hot trend hits Duluth streets as entrepreneurs feed customers on the go 
Minnesota man charged with burrito assault 
Minnesota man charged with burrito assault 
Duluth housing market on the rebound 
Duluth housing market on the rebound
Fast friends: Olympic teammates Goucher, Flanagan push each other 
Fast friends: Olympic teammates Goucher, Flanagan push each other
Hibbing natives see Olympic dream working as volunteers 
Hibbing natives see Olympic dream working as volunteers
Hot trend hits Duluth streets as entrepreneurs feed customers on the go 
Hot trend hits Duluth streets as entrepreneurs feed customers on the go
  
Sections 
Home News Business Sports Opinion Life Wave Outdoors Milestones Scrapbook Obituaries Blogs Budgeteer 
About 
Advertising Information Contact Us FCC Careers Privacy Statement Terms & Conditions 
Tools & Features 
Calendar Mobile Deals Email Alerts  RSS Feeds Archive Video Photo Galleries Mobile Site 
FCC Digital Network 
Jobshq.com Carshq.com Apartmentshq.com Homeshq.com Northlandoutdoors.com Agweek.com Areavoices.com 
Duluth News Tribune 424 W. First St., Duluth, Minnesota 55802 | Phone: (218) 723-5281
© 2012 Forum Communications Co. — All rights reserved
  
  
Sections 
Home News Business Sports Opinion Life Wave Outdoors Milestones Scrapbook Obituaries Blogs Budgeteer 
About 
Advertising Information Contact Us FCC Careers Privacy Statement Terms & Conditions 
Tools & Features 
Calendar Mobile Deals Email Alerts  RSS Feeds Archive Video Photo Galleries Mobile Site 
FCC Digital Network 
Jobshq.com Carshq.com Apartmentshq.com Homeshq.com Northlandoutdoors.com Agweek.com Areavoices.com 
Sections 
Home News Business Sports Opinion Life Wave Outdoors Milestones Scrapbook Obituaries Blogs Budgeteer 
Home
News
Business
Sports
Opinion
Life
Wave
Outdoors
Milestones
Scrapbook
Obituaries
Blogs
Budgeteer
About 
Advertising Information Contact Us FCC Careers Privacy Statement Terms & Conditions 
Advertising Information
Contact Us
FCC Careers
Privacy Statement
Terms & Conditions
Tools & Features 
Calendar Mobile Deals Email Alerts  RSS Feeds Archive Video Photo Galleries Mobile Site 
Calendar
Mobile Deals
Email Alerts
 RSS Feeds
Archive
Video
Photo Galleries
Mobile Site
FCC Digital Network 
Jobshq.com Carshq.com Apartmentshq.com Homeshq.com Northlandoutdoors.com Agweek.com Areavoices.com 
Jobshq.com
Carshq.com
Apartmentshq.com
Homeshq.com
Northlandoutdoors.com
Agweek.com
Areavoices.com
Duluth News Tribune 424 W. First St., Duluth, Minnesota 55802 | Phone: (218) 723-5281
© 2012 Forum Communications Co. — All rights reserved
Duluth News Tribune 424 W. First St., Duluth, Minnesota 55802 | Phone: (218) 723-5281
© 2012 Forum Communications Co. — All rights reserved
Duluth News Tribune
Forum Communications Co.
