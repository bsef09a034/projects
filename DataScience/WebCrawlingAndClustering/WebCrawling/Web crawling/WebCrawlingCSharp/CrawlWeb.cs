﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using mshtml;

namespace WebCrawlingCSharp
{
    class CrawlWeb
    {
        static StreamReader sr;
        static StreamWriter sw;
        public static int fileNumber;
        public static List<string> recList = new List<string>();
        public static List<string> pageRecList = new List<string>();

        public static List<String> getWebLinksAndWriteOutputToFile(String strWebPageContents, String strSourceurl,String outputFolderPath)
        {
            sr = new StreamReader("count.txt");
            fileNumber=Convert.ToInt32(sr.ReadLine());
            sr.Close();


            IHTMLDocument2 htmlDocument = (IHTMLDocument2)new mshtml.HTMLDocument();
            // Construct the document
            htmlDocument.write(strWebPageContents);
            // Extract all elements
            IHTMLElementCollection allElements = htmlDocument.all;
            List<String> webLinks = new List<string>();


            String FilePath = outputFolderPath + "\\File" + fileNumber + ".txt";
            fileNumber++;
            sw = new StreamWriter("count.txt");
            sw.Write(fileNumber);
            sw.Close();


            if (fileNumber > Defs.maxFilesCount)
                Environment.Exit(0);

            TextWriter tw = new StreamWriter(FilePath);


            foreach (IHTMLElement element in allElements)
            {
                String strelement = element.tagName;
                if (element.innerText != null)
                    tw.WriteLine(element.innerText.ToString());
                if (strelement.ToLower().CompareTo("a") == 0)
                {
                    String href = element.getAttribute("href");
                    if (href != null)
                    {
                        if (href.StartsWith("about:"))
                        {
                            String sublink=href.Substring(6);
                            if(sublink.StartsWith("/") )
                            {
                                String substr;

                                try
                                {
                                    substr = sublink.Substring(0, sublink.IndexOf("/", 1));
                                }
                                catch(ArgumentOutOfRangeException)
                                {
                                    substr=sublink.Substring(1,sublink.Length-1);
                                }
                                                       
                                if (strSourceurl.Contains(substr))
                                {
                                    int index = strSourceurl.IndexOf(substr);
                                    String baseURL = strSourceurl.Substring(0, index);
                                    String strFullyQualifiedLink = baseURL + sublink;
                                    if (!webLinks.Contains(strFullyQualifiedLink))
                                    webLinks.Add(strFullyQualifiedLink);
                                }
                                else
                                {
                                    String strFullyQualifiedLink = strSourceurl + sublink;
                                    if(!webLinks.Contains(strFullyQualifiedLink))
                                    webLinks.Add(strFullyQualifiedLink);
                                }
                            }
                        }
                        else if (href.StartsWith("http") || href.StartsWith("www"))
                            if (!webLinks.Contains(href))
                            webLinks.Add(href);
                    }
                }
            }
            tw.Close();


            return webLinks;
        }
        public static String getWebPageContents(String url)
        {
            WebClient client = new WebClient();
            string pageContents="";
            try
            {
                pageContents = client.DownloadString(url);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return pageContents;
        }
        public static void writeResultsToFile(List<String> links,String strOutputFolderPath)
        {
            for (int i = 0; i < links.Count; i++)
            {
                String Contents=getWebPageContents(links[i]);
                if (Contents.CompareTo("") == 0)
                    continue;
                List<String> resLinks = new List<string>();
                List<String> temp=getWebLinksAndWriteOutputToFile(Contents, links[i], strOutputFolderPath);
                foreach(var val in temp)
                {
                    if (!recList.Contains(val))
                    {
                        Console.WriteLine(val);
                        recList.Add(val);
                        resLinks.Add(val);
                    }
                }
                
                writeResultsToFile(resLinks, strOutputFolderPath);
            }
        }
        
        static void Main(string[] args)
        {
            List<string> links = new List<string>();
            string link = "https://www.google.com.pk/";
            links.Add(link);
            string pageContents=getWebPageContents(link);
           
            String outputFolderPath = "temp";

            writeResultsToFile(links, outputFolderPath);
          
        }
    }
}
