﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ARM
{
    class File
    {
        public StreamReader sr = null;
        public List<string> list = new List<string>();
        int ind = 0;

        public List<string> read(string fname)
        {
            try
            {
                list.Clear();
                sr = new StreamReader(fname);
                
                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split(' ','\n',',','.','\t');
                   
                    for (int i = 0; i < words.Length; i++)
                        list.Add(words[i]);
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();

            return list;

        }
       
    }
}
