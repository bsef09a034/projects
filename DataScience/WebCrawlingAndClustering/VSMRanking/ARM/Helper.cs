﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    partial class Program
    {
      



        /* Given:  ch   A character.
           Task:   To determine whether ch represents an operand (here understood
                   to be a single letter or digit).
           Return: In the function name: true, if ch is an operand, false otherwise.
        */
        static bool IsOperand(string  ch)
           {
               if (ch != "AND" && ch != "OR" && ch != "NOT" && ch != "+" && ch != "*" && ch != "-" &ch!="/")
              return true;
           else
              return false;
           }


        /* Given:  OperatorA    A character representing an operator or parenthesis.
                   OperatorB    A character representing an operator or parenthesis.
           Task:   To determine whether OperatorA takes precedence over OperatorB.
           Return: In the function name: true, if OperatorA takes precedence over
                   OperatorB.
        */
        static bool TakesPrecedence(string OperatorA, string OperatorB)
           {

           if (OperatorA == "(")
              return false;
           else if (OperatorB == "(")
              return false;
           else if (OperatorB == ")")
              return true;
           else if ((OperatorA == "^") && (OperatorB == "^"))
              return false;
           else if (OperatorA == "^")
              return true;
           else if (OperatorB == "^")
              return false;
           else if ((OperatorA == "*") || (OperatorA == "/"))
              return true;
           else if ((OperatorB == "*") || (OperatorB == "/"))
              return false;
           else
              return true;
      
           }


        /* Given:  Infix    A string representing an infix exp<b></b>ression (no spaces).
           Task:   To find the postfix equivalent of this exp<b></b>ression.
           Return: Postfix  A string holding this postfix equivalent.
        */
        static void Convert(ref string  Infi, ref string Postfix)
           {
           Stack<string> OperatorStack=new Stack<string> ();
           string[] Infix = Infi.Split(' ');
           string TopSymbol, Symbol;
           int k;

           for (k = 0; k < Infix.Length; k++)
              {
              Symbol = Infix[k];
              if (IsOperand(Symbol))
                  Postfix = Postfix + " " + Symbol;
              else
                 {
                 while ((! (OperatorStack.Count==0)) &&
                    (TakesPrecedence(OperatorStack.Peek(), Symbol)))
                    {
                    TopSymbol = OperatorStack.Peek();
                    OperatorStack.Pop();
                    Postfix = Postfix + " " + TopSymbol;
                    }
                 if ((! (OperatorStack.Count==0)) && (Symbol == ")"))
                    OperatorStack.Peek();  
                 else
                    OperatorStack.Push(Symbol);
                 }
              }

           while (! (OperatorStack.Count==0))
              {
              TopSymbol = OperatorStack.Peek();
              OperatorStack.Pop();
              Postfix = Postfix + " "+ TopSymbol;
              }
           }
        static void result(RetObj obj)
        {
            Console.WriteLine("Result!");

            for (int i = 0; i < obj.list.Count; i++)
            {
                for (int j = 0; j < obj.list[i].Count; j++)
                {
                    Console.WriteLine("\n"+obj.list[i][j].pageName);
                    for (int k = 0; k < obj.list[i][j].positions.Count; k++)
                    {
                        Console.Write(obj.list[i][j].positions[k]+ " ,");
                    }
                }
            }
        }
        static RetObj Helper(string Infix)
        {
            RetObj ret=new RetObj ();
           
                string Postfic="";  

                Convert(ref Infix,ref Postfic);
                string[] Postfix = Postfic.Split(' ');
               

                Stack<RetObj> S=new Stack<RetObj> ();
	            string[] a=(string[])Postfix.Clone();


                try
                        {

                for(int i=1; i<a.Length; i++)
	            {
                    
                    if (a[i] == "+")
                    {
                        RetObj temp= processOr(S.Pop(), S.Pop());
                        S.Push(temp);
                      
                    }
                   
                    else if (a[i] == "*")
                    {
                        
                        RetObj temp = processAnd(S.Pop(), S.Pop());
                        S.Push(temp);
                    }
                    else if (a[i] == "/")
                    {
                        RetObj d1 = S.Pop();

                        RetObj d2 = S.Pop();
                        
                        RetObj temp = processNot(d2, d1);
                        S.Push(temp);
                    }
                    else
                    {
                        
                            List<InnerNode> temp = lexicon[a[i]].pageList;
                            RetObj ro = new RetObj();
                            ro.list.Add(temp);
                            S.Push(ro);
                       
                    }


	            }
                ret=S.Pop();
                result(ret);

                        }
                catch (KeyNotFoundException)
                {
                    Console.WriteLine("Key Not found.");
                }

                return ret; 
          
        }
            
    }
}
