﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    partial class Program
    {
        static File obj = new File();
        static List<string> data;
        static Dictionary<string, Node> lexicon=new Dictionary<string,Node> ();
        static List<string> ipValues = new List<string>();
        static List<string> inputValues = new List<string>();
        static List<string> fileNames = new List<string>();
        static Dictionary<string, Doc> Dvector = new Dictionary<string, Doc>();
        static Dictionary<string,double> Lvector = new Dictionary<string,double> ();
        static Dictionary<string, double> Qvector = new Dictionary<string, double>();

        static bool containPage(List<InnerNode> l, string page, ref int ind)
        {
            bool retVal=false;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].pageName == page)
                {
                    retVal = true;
                    ind = i;
                }
            }

            return retVal;
        }
        static RetObj minus(List<InnerNode> l1, List<InnerNode> l2)
        {
            RetObj retObj = new RetObj();
            List<InnerNode> result = new List<InnerNode>();
            List<string> temp1 = new List<string>(), temp2 = new List<string>(), final = new List<string>();

            for (int i = 0; i < l1.Count; i++)
            {
                temp1.Add(l1[i].pageName);
            }
            for (int i = 0; i < l2.Count; i++)
            {
                temp2.Add(l2[i].pageName);
            }
            for (int i = 0; i < temp1.Count; i++)
            {
                if (!temp2.Contains(temp1[i])) { final.Add(temp1[i]); }
            }
            for (int i = 0; i < l1.Count; i++)
            {
                if (final.Contains(l1[i].pageName)) result.Add(l1[i]);
            }
            
            retObj.list.Add(result);
            return retObj;
        }
        static RetObj intersaction(List<InnerNode> l1, List<InnerNode> l2)
        {
            RetObj retObj = new RetObj();
            List<InnerNode> result1 = new List<InnerNode>(), result2 = new List<InnerNode>();
            List<string> temp1 = new List<string>(), temp2 = new List<string>(), final = new List<string>();
            for (int i = 0; i < l1.Count; i++)
            {
                temp1.Add(l1[i].pageName);
            }
            for (int i = 0; i < l2.Count; i++)
            {
                temp2.Add(l2[i].pageName);
            }
            for (int i = 0; i < temp1.Count; i++)
            {
                if (temp2.Contains(temp1[i])) { final.Add(temp1[i]); }
            }
            for (int i = 0; i < l1.Count; i++)
            {
                if (final.Contains(l1[i].pageName)) result1.Add(l1[i]);
            }
            for (int i = 0; i < l2.Count; i++)
            {
                if (final.Contains(l2[i].pageName)) result2.Add(l2[i]);
            }
            retObj.list.Add(result1);
            retObj.list.Add(result2);

            return retObj;
        }
        static RetObj processOr(RetObj a, RetObj b)
        {
            RetObj result = new RetObj(); 
            try
            {
                
                
                if (a.list.Count > 0||b.list.Count > 0)
                {
                    result.list.AddRange(a.list);
                    result.list.AddRange(b.list);
                 
                }
                else Console.WriteLine("\n No match!");

            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("\nKey has no match!");
            }
            return result;
        }
        static Dictionary<string, double> idf = new Dictionary<string, double>();
        static RetObj processAnd(RetObj a, RetObj b)
        {
            RetObj retObj = intersaction(a.list[0], b.list[0]);
            try
            {
                retObj = intersaction(a.list[0], b.list[0]);
                List<InnerNode> l=retObj.list[0];
                for (int i = 1; i < a.list.Count; i++)
                {
                    List<InnerNode> temp = new List<InnerNode>();
                    for (int j = 0; j < a.list[i].Count; j++)
                    {
                        int ind=-1;
                        InnerNode iN = new InnerNode();
                        if(containPage( l,a.list[i][j].pageName,ref ind))
                        {
                            iN.pageName = a.list[i][j].pageName;
                            iN.positions = a.list[i][j].positions;
                        }
                        temp.Add(iN);
                    }
                    retObj.list.Add(temp);
                }

            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("\nKey has no match!");
            }
            return retObj;

        }
        static RetObj processNot(RetObj a, RetObj b)
        {
            RetObj ret = null;
            try
            {
                ret= minus(a.list[0], b.list[0]);
                
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("\nKey has no match!");
            }
            return ret;

        }
        static void computeIDF()
        {
            foreach (var val in ipValues)
            {
                double tempIdf = 0.0;
                double d = (double)Defs.filesCount / (double)lexicon[val].pageList.Count;
                
                tempIdf =Math.Log10(d);
                idf.Add(val, tempIdf);
                Lvector.Add(val, tempIdf);
                Qvector.Add(val, 0);
            }
        }
        static double computeDotProduct(Dictionary<string, double> v1, Dictionary<string, double> v2)
        {
            double dotPro = 0.0;
            foreach (var val in v1)
            {
                dotPro = dotPro + (val.Value * v2[val.Key]);
            }

            return dotPro;

        }
        static double computeVecMagnitude(Dictionary<string, double> v)
        {
            double sum = 0.0;
            foreach (var val in v)
            {
                double temp = val.Value;
                sum = sum + (temp * temp);
            }

            return Math.Sqrt(sum);

        }
        static void populateDvector()
        {
            foreach (var val in fileNames)
            {
                Doc d = new Doc();

                foreach (KeyValuePair<string, double> kvp in Lvector)
                {
                    HelperNode obj = new HelperNode();
                    int ind = -1;
                    if (containPage(lexicon[kvp.Key].pageList, val, ref ind))
                    {
                        obj.tf = lexicon[kvp.Key].pageList[ind].positions.Count;
                    }
                    obj.tfIdf = (double)Lvector[kvp.Key] * (double)obj.tf;
                    d.values.Add(kvp.Key, obj.tfIdf);
                }
                
                Dvector.Add(val, d);
           }
        }
        static void computeRank()
        {
            double v2Mag = computeVecMagnitude(Qvector);

            Dictionary<string, double> rank = new Dictionary<string, double>();
            foreach(var val in Dvector)
            {
                double dotPro = computeDotProduct(val.Value.values , Qvector);
                double v1Mag = computeVecMagnitude(val.Value.values);

                double cos = dotPro / (v1Mag * v2Mag);
                rank.Add(val.Key, cos);
                Console.WriteLine("\nFile is=" + val.Key + " rank is=" + cos);
            }

        }
        static void populateQvector()
        {
            for (int i = 0; i < inputValues.Count; i++)
            {
                Qvector[inputValues[i]] = Lvector[inputValues[i]];
            }
        }
        static void computeTF_TF_IDF(RetObj o)
        {
            Dictionary<string, HelperNode> d1=new Dictionary<string,HelperNode> ();
            double []prevSum=new double[Defs.filesCount];
            for (int i = 0; i < ipValues.Count; i++)
            {
                d1 = new Dictionary<string, HelperNode>();
                foreach (var val in fileNames)
                {
                    HelperNode obj = new HelperNode();
                    int ind = -1;
                    if (containPage(lexicon[ipValues[i]].pageList, val, ref ind))
                    {
                        obj.tf = lexicon[ipValues[i]].pageList[ind].positions.Count;
                    }
                    obj.tfIdf = (double)idf[ipValues[i]] * (double)obj.tf;

                    d1.Add(val, obj);
                }
                
                for (int j = 0; j < prevSum.Length; j++)
                {
                    prevSum[j] = prevSum[j] + d1[fileNames[j]].tfIdf;
                }
                

            }
            List<TfIdfNode> l = new List<TfIdfNode>();
            for (int j = 0; j < prevSum.Length; j++)
            {
                 l.Add(new TfIdfNode(fileNames[j], prevSum[j]));
            }
            l.Sort();
            l.Reverse();
            Console.WriteLine("\n \n TF-IDF is...");
            for (int j = 0; j < prevSum.Length; j++)
            {
                Console.WriteLine("File is: " + l[j].fileName + "Tf-Idf is= " + l[j].tfIdf);
            }
            
            Console.WriteLine("\n\n the order of the result should be...");
            int index = -1;
            List<string> s = new List<string>();
            for(int i=0;i<o.list.Count;i++)
            {
                for(int j=0;j<l.Count;j++)
                {
                    if(containPage(o.list[i],l[j].fileName,ref index))
                    {
                        if(!s.Contains(l[j].fileName))
                        {
                            s.Add(l[j].fileName);
                        }
                    }
                }
            }
            foreach (var val in s)
                Console.WriteLine(val);

        }
        static void userEnd()
        {
            Console.WriteLine("Enter query here:");
            string s = Console.ReadLine();
            s = s.ToLower();
            string[] words = s.Split(' ');
            string temp = "";
            bool chk = true;
            for (int i = 0; i < words.Length; i++)
            {
                if (words[i] == "not") words[i] = "/";
                else if (words[i] == "and") words[i] = "*";
                else if (words[i] == "or") words[i] = "+";
                else
                {
                    inputValues.Add(words[i]);
                }
                
                if (chk)
                {
                    temp =  words[i];
                    chk = false;
                }
                else temp = temp + " " + words[i];
            }
        
            Console.WriteLine(temp);
            RetObj o= Helper(temp);

            computeIDF();
            populateDvector();
            populateQvector();
            Console.WriteLine("\n\nAlgo 2 results");
            computeRank();
            
            
           
        }
        static void populateLexicon(string page)
        {
            data = obj.read(page);



            for (int i = 0; i < data.Count; i++)
            {
                if (lexicon.ContainsKey(data[i].ToLower()))
                {
                    Node temp = lexicon[data[i].ToLower()];
                    temp.totalCount++;
                    int ind = -1;
                    if (containPage(temp.pageList, page, ref ind))
                    {
                        if (!temp.pageList[ind].positions.Contains(i))
                            temp.pageList[ind].positions.Add(i);
                    }
                    else
                    {
                        InnerNode t = new InnerNode();
                        t.pageName = page;
                        t.positions.Add(i);
                        temp.pageList.Add(t);
                    }
                    lexicon[data[i].ToLower()] = temp;
                }
                else
                {
                    Node temp = new Node();
                    temp.totalCount++;
                    InnerNode t = new InnerNode();
                    t.pageName = page;
                    t.positions.Add(i);
                    temp.pageList.Add(t);
                    lexicon.Add(data[i].ToLower(), temp);
                    ipValues.Add(data[i].ToLower());
                }

            }
        }
        static void Main(string[] args)
        {

            try
            {


                for (int i = 1; i <= Defs.filesCount; i++)
                {
                    string page = "chapter " + i.ToString() + ".txt";
                    fileNames.Add(page);
                    populateLexicon(page);
                }


                userEnd();

                Console.WriteLine("\n******  ");
            }
            catch (KeyNotFoundException e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
