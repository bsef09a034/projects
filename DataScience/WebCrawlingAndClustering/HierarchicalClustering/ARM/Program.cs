﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    class Program
    {
        static int count = 0;
        static File obj = new File();
        static public List<List<string>> data=new List<List<string>> ();
        static List<string>[] clusters =new List<string>[1];
        static public double[,] distances;
        static public Dictionary<string, Node> met = new Dictionary<string, Node>();
        static double distanceHelper(int a, int b)
        {
            
            double mismatch = 0;
            for (int i = 0; i < data[a].Count; i++)
            {
                if (data[a][i] != data[b][i]) mismatch++;

            }

            return mismatch/(double)data[a].Count;

        }

       

        static void computeDistances()
        {
            for (int i = 0; i < data.Count; i++)
            {
                for (int j = i; j < data.Count; j++)
                {
                    distances[i, j] =Math.Round(distanceHelper(i, j),1);
                    distances[j, i] = distances[i, j];
                }
            }
        }
        static double computeMinDis(int n, List<int> l)
        {
            double min = double.MaxValue;
            double dis=0;
            for(int i=0;i<l.Count;i++)
            {
                dis=distances[n,l[i]];
                if (min > dis) min = dis;
            }
            return min;



        }
        
        static bool compare(double[] a, double[] b)
        {
            bool ret = true;
            if (a.Length == b.Length)
            {
                for (int i = 0; i < a.Length && ret; i++)
                {
                    if (a[i] != b[i]) ret = false;
                }
            }
            else ret = false;

            return ret;
        }
        static void remove(string s)
        {
            met.Remove(s);
            foreach (var val in met)
            {
                val.Value.d.Remove(s);
            }
        }
        static string minDist()
        {
            string retStr = "";
            double min = double.MaxValue;

            foreach (var val in met)
            {
                foreach (var v in val.Value.d)
                {
                    if (v.Value < min && /*v.Value!=0*/ v.Key!=val.Key)
                    {
                        min = v.Value;
                        retStr = val.Key +";"+ v.Key;
                    }
                }
            }

            return retStr;
        }
        static public double minDist(string a, string b)
        {
            double minDis = double.MaxValue;

            string []words=a.Split(',');
            int[] aa = new int[words.Length];
            for (int i = 0; i < words.Length; i++)
            {
                aa[i] = Convert.ToInt32( words[i]);
            }
                
            words=b.Split(',');
            int[] bb = new int[words.Length];
            for (int i = 0; i < words.Length; i++)
            {
                bb[i] = Convert.ToInt32(words[i]);
            }

            for (int i = 0; i < aa.Length; i++)
            {
                for (int j = 0; j < bb.Length; j++)
                {
                    if (distances[aa[i],bb[j]] < minDis) minDis = distances[aa[i],bb[j]];
                }
            }
            return minDis;
        }
        static void output()
        {
            string[] words= new string [1];
            foreach (var val in met)
            {
                Console.Write(val.Key+"\t");
                foreach (var v in val.Value.d)
                {
               
                    Console.Write(v.Value + "\t");
                }
                Console.WriteLine();
            }
        }

        static void userEnd()
        {
           
            try
            {
                Console.WriteLine("Enter the cluster size from 2 to " + data.Count + " where cluster size is referred as the number of points per cluster.");
                int n = Convert.ToInt32(Console.ReadLine());

                for (int i = 0; i < clusters[n].Count; i++)
                {
                    Console.WriteLine(clusters[n][i]+"\n");
                }

            }
            catch(NullReferenceException )
            {
                Console.WriteLine("No cluster of this size.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        static void Main(string[] args)
        {
            obj.read();
            data=obj.train();
            clusters = new List<string>[data.Count+1];
            distances = new double[data.Count, data.Count];

            computeDistances();

            for (int i = 0; i < data.Count; i++)
            {
                string k = i.ToString();
                Node temp = new Node(i);

                met.Add(k, temp);
            }

            while (met.Count > 1)
            {
                string s = minDist();
                if (s != "")
                {
                    string[] words = s.Split(';');
                    for (int i = 0; i < words.Length; i++)
                    {
                        remove(words[i]);
                    }
                    string k = s.Replace(';', ',');
                    Node temp = new Node(k);
                    met.Add(k, temp);
                    foreach (var val in met)
                    {
                        try
                        {
                            val.Value.d.Add(k, met[k].d[val.Key]);
                        }
                        catch { }
                    }
                    try
                    {
                        clusters[k.Split(',').Length].Add(k);
                    }
                    catch (NullReferenceException)
                    {
                        clusters[k.Split(',').Length] = new List<string>();
                        clusters[k.Split(',').Length].Add(k);
                    }
                }
                
            }

            userEnd();
            
        }
    }
}
