﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    class Node
    {
        public Dictionary<string, double> d = new Dictionary<string, double>();
        public Node() { }
        public Node(int n)
        {
            for (int i = 0; i < Program.data.Count; i++)
            {
                string k = i.ToString();
                double v = Program.distances[i, n];
                d.Add(k, v);
            }
        }
        public Node(string s)
        {
            d.Add(s, 0);
            foreach (var val in Program.met)
            {
                string k = val.Key;
                double v = Program.minDist(k,s);
                d.Add(k, v);
            }
        }
    }
}
