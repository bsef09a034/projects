﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    static class Defs
    {
        public static int filesCount =16;
        public static int kThresh = 3, min = 0, max = 9;
        public static double objThresh = 0.998;
    }
}
