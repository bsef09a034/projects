﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    class TfIdfNode:IComparable
    {
        public string fileName = "";
        public double tfIdf = 0.0;
        public TfIdfNode(string f="", double t=0)
        {
            fileName = f;
            tfIdf = t;
        }
        public int CompareTo(object obj)

        {

            if (obj is TfIdfNode)

            {

                TfIdfNode p2 = (TfIdfNode)obj;

                return tfIdf.CompareTo(p2.tfIdf);

            }

            else

                throw new ArgumentException("Object is not a TfIdfNode.");

        }
        
    }
}
