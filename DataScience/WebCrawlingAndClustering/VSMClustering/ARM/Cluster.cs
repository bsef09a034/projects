﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    partial class Program
    {
        
        static List<ClusterNode> clusters = new  List<ClusterNode> ();

        static double[] previousCluster;
        static double[] newCluster;

        static double[] similarity ;
        
        static double similarityHelper(string a, string b)
        {

            double cos = 0;
            double dotPro = computeDotProduct(Dvector[a].values, Dvector[b].values);
            double v1 = computeVecMagnitude(Dvector[a].values);
            double v2 = computeVecMagnitude(Dvector[b].values);
            
            cos = dotPro / (v1 * v2);
            return cos;

        }

       

        
        static double computeMaxSimilarity(string n,List<string> l)
        {
            double max = double.MinValue;
            
            double sim = 0;
            for (int i = 0; i < l.Count; i++)
            {
                sim = similarityHelper(n,l[i]);
                if (sim > max) max = sim;
            }
            return max;



        }
        static int assignCluster(string n)
        {

            int maxInd = -1;
            for (int i = 0; i < clusters.Count; i++)
            {
                if (clusters[i].list.Contains(n)) clusters[i].list.Remove(n);
            }
            for (int i = 0; i < clusters.Count; i++)
            {
                similarity[i] = computeMaxSimilarity(n, clusters[i].list);
            }
          

            double max = similarity.Max();
            maxInd = Array.IndexOf(similarity, max);
            clusters[maxInd].list.Add(n);
            return maxInd;
            
        }
        static bool compare(double[] a, double[] b)
        {
            bool ret = true;
            if (a.Length == b.Length)
            {
                for (int i = 0; i < a.Length && ret; i++)
                {
                    if (a[i] != b[i]) ret = false;
                }
            }
            else ret = false;

            return ret;
        }
       
        static void makeDocClusters()
        {
           
            Console.WriteLine("\n*******\n");
            previousCluster=new double[Dvector.Count];
            newCluster = new double[Dvector.Count];

          
            {
                List<ClusterNode> tempList = new List<ClusterNode>();
                int ii=0;
                foreach (var val in Dvector)
                {
                    if (ii < Defs.kThresh)
                    {
                        ClusterNode temp = new ClusterNode();
                        temp.list.Add(val.Key);
                        clusters.Add(temp);
                        ii++;
                    }
                    else break;
                }
               
                for (int i = 0; i < previousCluster.Length; i++)
                {
                    previousCluster[i] = -1;
                }

                bool chk = true;
                while (chk)
                {
                    similarity = new double[clusters.Count];
                    
                    ii = 0;
                    foreach (var val in Dvector)
                    {
                        if (ii >= Defs.kThresh)
                        {
                            newCluster[ii] = assignCluster(val.Key);
                        }
                        else
                        {
                            newCluster[ii] = ii;

                        }
                        ii++;
                    }
                    
                    if (!compare(newCluster, previousCluster))
                    {
                        newCluster.CopyTo(previousCluster, 0);
                    }
                    else
                    {
                        chk = false;
                    }
                    
                }
                for (int i = 0; i < clusters.Count; i++)
                {
                    Console.WriteLine("Cluster number= " + i + "  Files are: ");
                    for (int j = 0; j < clusters[i].list.Count; j++)
                        Console.Write(clusters[i].list[j] + "\t");
                    Console.WriteLine("\n\n");
                }
            }
            

            Console.WriteLine("\n\n\nThis is the solution for k = "+(Defs.kThresh));
        }
    }
}
