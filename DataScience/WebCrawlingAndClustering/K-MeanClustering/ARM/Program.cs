﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    class Program
    {
        static File obj = new File();
        static public List<List<string>> data=new List<List<string>> ();
        static List<Node> clusters = new  List<Node> ();

        static double[] previousMinDis;
        static double[] newMinDis;

        static double[] minDis ;
        static double[,] distances;
        static double objSum = double.MaxValue;
        static double distanceHelper(int a, int b)
        {
            
            double mismatch = 0;
            for (int i = 0; i < data[a].Count; i++)
            {
                if (data[a][i] != data[b][i]) mismatch++;

            }

            return mismatch/data[a].Count;

        }

       

        static void computeDistances()
        {
            for (int i = 0; i < data.Count; i++)
            {
                for (int j = i; j < data.Count; j++)
                {
                    distances[i, j] =Math.Round(distanceHelper(i, j),1);
                    distances[j, i] = distances[i, j];
                }
            }
        }
        static double computeMinDis(int n, List<int> l)
        {
            double min = double.MaxValue;
            double dis=0;
            for(int i=0;i<l.Count;i++)
            {
                dis=distances[n,l[i]];
                if (min > dis) min = dis;
            }
            return min;



        }
        static int assignCluster(int n)
        {

            int minInd = -1;
            for (int i = 0; i < clusters.Count; i++)
            {
                if (clusters[i].list.Contains(n)) clusters[i].list.Remove(n);
            }
            for (int i = 0; i < clusters.Count; i++)
            {
                minDis[i] = computeMinDis(n, clusters[i].list);
            }
            

            double min = minDis.Min();
            minInd = Array.IndexOf(minDis, min);
            clusters[minInd].list.Add(n);
            return minInd;
            
        }
        static bool compare(double[] a, double[] b)
        {
            bool ret = true;
            if (a.Length == b.Length)
            {
                for (int i = 0; i < a.Length && ret; i++)
                {
                    if (a[i] != b[i]) ret = false;
                }
            }
            else ret = false;

            return ret;
        }
        static double sumFromCentroid(List<int> l)
        {
            double sum=double.MaxValue ,tempSum= 0;
            for (int i = 0; i < l.Count; i++)
            {
                tempSum = 0;
                for (int j = 0; j < l.Count; j++)
                {
                    tempSum = tempSum + distances[i, j];
                }
                if (tempSum < sum) sum = tempSum;
            }
            return sum;

        }
        static bool objFunc()
        {
            double sum = 0;
            for (int i = 0; i < clusters.Count; i++)
            {
                sum = sum + sumFromCentroid(clusters[i].list);
            }
            if ((objSum / sum) > Defs.objThresh)
            {
                objSum = sum;
                return true;
            }
            else return false;
        }
        static void Main(string[] args)
        {
            obj.read();
            data=obj.train();
            distances = new double[data.Count, data.Count];

            computeDistances();


            previousMinDis=new double[data.Count];
            newMinDis = new double[data.Count];

            
            Defs.kThresh = 1;
            bool kCheck=true;

            while(kCheck)
            {
                for (int j = 0; j < Defs.kThresh; j++)
                {
                    Node temp = new Node();
                    temp.list.Add(j);

                    clusters.Add(temp);
                }
                for (int i = 0; i < previousMinDis.Length; i++)
                {
                    previousMinDis[i] = -1;
                }
                bool chk = true;
                while (chk)
                {
                    
                    minDis = new double[clusters.Count];
                    for (int i = 0; i < data.Count; i++)
                    {
                        if (i >= Defs.kThresh)
                            newMinDis[i] = assignCluster(i);
                        else newMinDis[i] = 0;
                    }
                    if (!compare(newMinDis, previousMinDis))
                    {
                        Console.WriteLine("not equal");
                        newMinDis.CopyTo(previousMinDis, 0);
                    }
                    else
                    {
                        Console.WriteLine("equal");
                        chk = false;
                    }
                    
                }
                for (int i = 0; i < clusters.Count; i++)
                {
                    Console.WriteLine("Cluster number= " + i + "  Data record number are: ");
                    for (int j = 0; j < clusters[i].list.Count; j++)
                        Console.Write(clusters[i].list[j] + "\t");
                    Console.WriteLine("*");
                }
                kCheck = objFunc();
                clusters.Clear();
                Defs.kThresh++;
            }
            

            Console.WriteLine("\n\n\nThis is the optimal solution for k = "+(Defs.kThresh-2)+" with objective threshhold of "+Defs.objThresh+"\n\n\n ******  ");
        }
    }
}
