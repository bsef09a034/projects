﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ARM
{
    class File
    {
        public StreamReader sr = null;
        public List<string> list = new List<string>();
        public List<string> read(string fname)
        {
            try
            {
                list=new List<string> ();
                sr = new StreamReader(fname);
                


               
                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split(' ','\n',',','.','\t');
                   
                    for (int i = 0; i < words.Length; i++)
                        list.Add(words[i]);
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();

            return list;

        }

        public List<string> read2(string fname)
        {
            try
            {
                list = new List<string>();
                sr = new StreamReader(fname);

                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split(' ', '\n', '\t', '+', '/', '=', '^', '*');



                    for (int i = 0; i < words.Length; i++)
                    {
                        string tempW = words[i].Trim();
                        if (tempW.Length > 0)
                        {
                            if (tempW.Last() == '.')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf('.'));
                            }
                            else if (tempW.Last() == ',')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf(','));
                            }
                            else if (tempW.Last() == ';')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf(';'));
                            }
                            else if (tempW.Last() == ':')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf(':'));
                            }

                            if (tempW.Contains(')')) tempW = tempW.Replace(")", "");
                            if (tempW.Contains('(')) tempW = tempW.Replace("(", "");
                            if (tempW.Contains('-')) tempW = tempW.Replace("-", "");
                            if (tempW.Contains('?')) tempW = tempW.Replace("?", "");
                            if (tempW.Contains('[')) tempW = tempW.Replace("[", "");
                            if (tempW.Contains(']')) tempW = tempW.Replace("]", "");
                            if (tempW.Contains('_')) tempW = tempW.Replace("_", "");
                            if (tempW.Contains('\'')) tempW = tempW.Replace("\'", "");
                            if (tempW.Contains('`')) tempW = tempW.Replace("`", "");
                            try
                            {


                                if (tempW.All(char.IsLetter))
                                    list.Add(tempW);
                                else
                                {
                                    Convert.ToDouble(tempW);
                                    list.Add(tempW);
                                }

                            }
                            catch
                            {
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();

            return list;


        }

        public static void WriteResults(string fname, string query, List<string> resultingRankedFiles,int size=-1)
        {
            StreamWriter sw = new StreamWriter(fname,true);
            if (size != -1)
            {
                sw.WriteLine("Size of lexicon is: " + size);
                sw.WriteLine();
            }

            sw.WriteLine("query is: "+query);
            sw.WriteLine("Result is: ");
            foreach (var v in resultingRankedFiles)
                sw.WriteLine(v);

            sw.WriteLine();

            sw.Close();

        }
        public List<string> readQueries(string fname)
        {
            try
            {
                list=new List<string> ();
                sr = new StreamReader(fname);
                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    list.Add(s);
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();

            return list;

        }
        
    }
}
