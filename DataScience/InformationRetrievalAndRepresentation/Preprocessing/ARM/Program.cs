﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PorterStemmer;
namespace ARM
{
    partial class Program
    {
        static int phase = 1,queryCount=1;
        static string query = "";
        public static File obj = new File();
        public static List<string> data;
        static Dictionary<string, double> idf = new Dictionary<string, double>();
        public static Dictionary<string, Node> lexicon=new Dictionary<string,Node> ();
        static List<string> ipValues = new List<string>();
        static List<string> fileNames = new List<string>();
        static void addPageList(List<InnerNode> a, List<InnerNode> b)
        {
            for (int i = 0; i < b.Count; i++)
            {
                int ind = -1;
                if (containPage(a, b[i].pageName, ref ind))
                {
                    if (!a[ind].positions.Contains(i))
                        a[ind].positions.Add(i);
                }
                else
                {
                    a.Add(b[i]);
                }
            }
        }
        static bool containPage(List<InnerNode> l, string page, ref int ind)
        {
            bool retVal=false;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].pageName == page)
                {
                    retVal = true;
                    ind = i;
                }
            }

            return retVal;
        }
        static RetObj minus(List<InnerNode> l1, List<InnerNode> l2)
        {
            RetObj retObj = new RetObj();
            List<InnerNode> result = new List<InnerNode>();
            List<string> temp1 = new List<string>(), temp2 = new List<string>(), final = new List<string>();

            for (int i = 0; i < l1.Count; i++)
            {
                temp1.Add(l1[i].pageName);
            }
            for (int i = 0; i < l2.Count; i++)
            {
                temp2.Add(l2[i].pageName);
            }
            for (int i = 0; i < temp1.Count; i++)
            {
                if (!temp2.Contains(temp1[i])) { final.Add(temp1[i]); }
            }
            for (int i = 0; i < l1.Count; i++)
            {
                if (final.Contains(l1[i].pageName)) result.Add(l1[i]);
            }
            
            retObj.list.Add(result);
            return retObj;
        }
        static RetObj intersaction(List<InnerNode> l1, List<InnerNode> l2)
        {
            RetObj retObj = new RetObj();
            List<InnerNode> result1 = new List<InnerNode>(), result2 = new List<InnerNode>();
            List<string> temp1 = new List<string>(), temp2 = new List<string>(), final = new List<string>();
            for (int i = 0; i < l1.Count; i++)
            {
                temp1.Add(l1[i].pageName);
            }
            for (int i = 0; i < l2.Count; i++)
            {
                temp2.Add(l2[i].pageName);
            }
            for (int i = 0; i < temp1.Count; i++)
            {
                if (temp2.Contains(temp1[i])) { final.Add(temp1[i]); }
            }
            for (int i = 0; i < l1.Count; i++)
            {
                if (final.Contains(l1[i].pageName)) result1.Add(l1[i]);
            }
            for (int i = 0; i < l2.Count; i++)
            {
                if (final.Contains(l2[i].pageName)) result2.Add(l2[i]);
            }
            retObj.list.Add(result1);
            retObj.list.Add(result2);

            return retObj;
        }
        static RetObj processOr(RetObj a, RetObj b)
        {
            RetObj result = new RetObj(); 
            try
            {
                
                
                if (a.list.Count > 0||b.list.Count > 0)
                {
                    result.list.AddRange(a.list);
                    result.list.AddRange(b.list);
                 
                }
                else Console.WriteLine("\n No match!");

            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("\nKey has no match!");
            }
            return result;
        }
        
        static RetObj processAnd(RetObj a, RetObj b)
        {
            RetObj retObj = intersaction(a.list[0], b.list[0]);
            try
            {
                retObj = intersaction(a.list[0], b.list[0]);
                List<InnerNode> l=retObj.list[0];
                for (int i = 1; i < a.list.Count; i++)
                {
                    List<InnerNode> temp = new List<InnerNode>();
                    for (int j = 0; j < a.list[i].Count; j++)
                    {
                        int ind=-1;
                        InnerNode iN = new InnerNode();
                        if(containPage( l,a.list[i][j].pageName,ref ind))
                        {
                            iN.pageName = a.list[i][j].pageName;
                            iN.positions = a.list[i][j].positions;
                        }
                        temp.Add(iN);
                    }
                    retObj.list.Add(temp);
                }

            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("\nKey has no match!");
            }
            return retObj;

        }
        static RetObj processNot(RetObj a, RetObj b)
        {
            RetObj ret = null;
            try
            {
                ret= minus(a.list[0], b.list[0]);
                
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("\nKey has no match!");
            }
            return ret;

        }
        static void computeIDF()
        {
            idf.Clear();
            foreach (var val in ipValues)
            {
                double tempIdf = 0.0;
                try
                {
                    tempIdf = Math.Log10((double)Defs.filesCount / (double)lexicon[val].pageList.Count);
                }
                catch { }
                idf.Add(val, tempIdf);
            }
        }
        static List<string> computeTF_TF_IDF(RetObj o)
        {
            Dictionary<string, HelperNode> d1=new Dictionary<string,HelperNode> ();
            double []prevSum=new double[Defs.filesCount];
            for (int i = 0; i < ipValues.Count; i++)
            {
                
                    d1 = new Dictionary<string, HelperNode>();
                    foreach (var val in fileNames)
                    {
                        try
                        {
                        HelperNode obj = new HelperNode();
                        int ind = -1;
                        
                            if (containPage(lexicon[ipValues[i]].pageList, val, ref ind))
                            {
                                obj.tf = lexicon[ipValues[i]].pageList[ind].positions.Count;
                            }
                        
                        obj.tfIdf = (double)idf[ipValues[i]] * (double)obj.tf;

                        d1.Add(val, obj);
                        }
                        catch { }
                    }

                    for (int j = 0; j < prevSum.Length; j++)
                    {
                        try{
                        prevSum[j] = prevSum[j] + d1[fileNames[j]].tfIdf;
                        }
                        catch { }

                    }
                
                

            }
            List<TfIdfNode> l = new List<TfIdfNode>();
            for (int j = 0; j < prevSum.Length; j++)
            {
                 l.Add(new TfIdfNode(fileNames[j], prevSum[j]));
            }
            l.Sort();
            l.Reverse();
           
            int index = -1;
            List<string> resultingRankedFiles = new List<string>();
            for(int i=0;i<o.list.Count;i++)
            {
                for(int j=0;j<l.Count;j++)
                {
                    if(containPage(o.list[i],l[j].fileName,ref index))
                    {
                        if (!resultingRankedFiles.Contains(l[j].fileName))
                        {
                            resultingRankedFiles.Add(l[j].fileName);
                        }
                    }
                }
            }
            string fname="phase"+phase+".txt";
            if (queryCount == 1)
                File.WriteResults(fname, query, resultingRankedFiles, PreprocessingHelper.SizeofLexicon());
            else
                File.WriteResults(fname, query, resultingRankedFiles);

            queryCount++;

            return resultingRankedFiles;
        }
        static List<string> userEnd(string s="")
        {
            
            ipValues.Clear();
            if (s == "")
            {
                Console.WriteLine("Enter query here:");
                s = Console.ReadLine();
            }
            query = s;
            s = s.ToLower();
            string[] words = s.Split(' ');
            string temp = "";
            bool chk = true;
            for (int i = 0; i < words.Length; i++)
            {
                if (words[i] == "not") words[i] = "/";
                else if (words[i] == "and") words[i] = "*";
                else if (words[i] == "or") words[i] = "+";
                else
                {
                    ipValues.Add(words[i]);
                }
                
                if (chk)
                {
                    temp =  words[i];
                    chk = false;
                }
                else temp = temp + " " + words[i];
            }
        
            RetObj o= Helper(temp);

            computeIDF();
            return (computeTF_TF_IDF(o));
 
           
        }
        static void populateLexicon(string page)
        {
            
            if (phase == 1)
                data = obj.read(page);
            else if (phase == 2)
                data = obj.read2(page);
            else
                data = obj.read(page);


            for (int i = 0; i < data.Count; i++)
            {
                if (lexicon.ContainsKey(data[i].ToLower()))
                {
                    Node temp = lexicon[data[i].ToLower()];
                    temp.totalCount++;
                    int ind = -1;
                    if (containPage(temp.pageList, page, ref ind))
                    {
                        if (!temp.pageList[ind].positions.Contains(i))
                            temp.pageList[ind].positions.Add(i);
                    }
                    else
                    {
                        InnerNode t = new InnerNode();
                        t.pageName = page;
                        t.positions.Add(i);
                        temp.pageList.Add(t);
                    }
                    lexicon[data[i].ToLower()] = temp;
                }
                else
                {
                    Node temp = new Node();
                    temp.totalCount++;
                    InnerNode t = new InnerNode();
                    t.pageName = page;
                    t.positions.Add(i);
                    temp.pageList.Add(t);
                    lexicon.Add(data[i].ToLower(), temp);
                }

            }
        }
        static void clearAll()
        {
            queryCount=1;
            query = "" ;
            File obj = new File();
        
            idf = new Dictionary<string, double>();
            lexicon=new Dictionary<string,Node> ();
            ipValues = new List<string>();
            fileNames = new List<string>();
        }
        static int actPred(List<string> act, List<string> pred)
        {
            int count = 0;
            for (int i = 0; i < pred.Count; i++)
            {
                if (act.Contains(pred[i])) count++;
            }
            return count;

        }
        static int actNotPred(List<string>act, List<string> pred)
        {
            int count = 0;
            for (int i = 0; i < act.Count; i++)
            {
                if (!pred.Contains(act[i])) count++;
            }
            return count;

        }
        static int notActPred(List<string>act, List<string>pred)
        {
            int count = 0;
            for (int i = 0; i < pred.Count; i++)
            {
                if (!act.Contains(pred[i])) count++;
            }
            return count;

        }
        static int notActNotPred(List<string>act, List<string> pred)
        {
            int count = act.Count();
            for (int i = 0; i < pred.Count; i++)
            {
                if (!act.Contains(pred[i])) count++;
            }
            return (Defs.filesCount- count);

        }
        static void computeMatrix(List<string> originalArr, List<string> tempArr)
        {
           
            int[,] matrix = new int[2, 2];
            matrix[0,0] = actPred(originalArr, tempArr);
            matrix[0, 1] = actNotPred(originalArr, tempArr);
            matrix[1, 0] = notActPred(originalArr, tempArr);
            matrix[1, 1] = notActNotPred(originalArr, tempArr);

            Finalmatrix[0, 0] = Finalmatrix[0, 0] + matrix[0, 0];
            Finalmatrix[0, 1] = Finalmatrix[0, 1] + matrix[0, 1];
            Finalmatrix[1, 0] = Finalmatrix[1, 0] + matrix[1, 0];
            Finalmatrix[1, 1] = Finalmatrix[1, 1] + matrix[1, 1];

        }

        static int[,] Finalmatrix = new int[2, 2];
        static void Main(string[] args)
        {

            try
            {
                
                //phase 1
                List<string> queries = obj.readQueries("Queries.txt");
                List<string>[] originalArr = new List<string>[queries.Count];
                List<string>[] tempArr = new List<string>[queries.Count];


                for (int i = 1; i <= Defs.filesCount; i++)
                {
                    string page = "chapter " + i.ToString() + ".txt";
                    fileNames.Add(page);
                    
                    populateLexicon(page);
                    
                }

                int ii = 0;
                foreach (var vv in queries)
                {
                    originalArr[ii]= userEnd(vv);
                    ii++;
                }
                
                Console.WriteLine("Acuracy phase 1:");
                for (int i = 0; i < queries.Count;i++ )
                {
                    computeMatrix(originalArr[i], originalArr[i]);
                }
                Console.WriteLine((double)(Finalmatrix[0, 0] + Finalmatrix[1, 1]) / Defs.filesCount/queries.Count);
                //phase 2
                Finalmatrix = new int[2, 2];
                phase++;
                clearAll();
                for (int i = 1; i <= Defs.filesCount; i++)
                {
                    string page = "chapter " + i.ToString() + ".txt";
                    fileNames.Add(page);
                    populateLexicon(page);
                }

                ii = 0;
                foreach (var v in queries)
                {
                    tempArr[ii]= userEnd(v);
                    ii++;
                }
                Console.WriteLine("Acuracy phase 2:");
                for (int i = 0; i < queries.Count; i++)
                {
                    computeMatrix(originalArr[i], tempArr[i]);
                }
                Console.WriteLine( (double)(Finalmatrix[0, 0] + Finalmatrix[1, 1]) / Defs.filesCount/queries.Count);
                
                //phase 3

                Finalmatrix = new int[2, 2];
                queryCount = 1;
                phase++;
                
                var temp = from element in lexicon
                           orderby element.Value.totalCount descending
                           select element;


                lexicon = temp.ToDictionary<KeyValuePair<string, Node>, string, Node>(pair => pair.Key, pair => pair.Value);



                for (int i = 0; i < 50 && i<lexicon.Count; i++)
                {
                    lexicon.Remove(lexicon.ElementAt(i).Key);
                }

                tempArr=new List<string>[queries.Count];
                ii = 0;
                foreach (var v in queries)
                {
                    tempArr[ii] = userEnd(v);
                    ii++;
                }
                Console.WriteLine("Acuracy phase 3:");
                for (int i = 0; i < queries.Count;i++ )
                {
                    computeMatrix(originalArr[i], tempArr[i]);
                }
                Console.WriteLine( (double)(Finalmatrix[0, 0] + Finalmatrix[1, 1]) / Defs.filesCount/queries.Count);
                
                //phase 4
                phase++;
                queryCount = 1;
                Finalmatrix = new int[2, 2];
                PorterStemmer.PorterStemmer ps = new PorterStemmer.PorterStemmer();
                for (int i = 0; i < lexicon.Count; i++)
                {
                    string s = ps.stemTerm(lexicon.ElementAt(i).Key);
                    if (!lexicon.ContainsKey(s))
                        lexicon.Add(s, lexicon.ElementAt(i).Value);
                    else
                    {
                        lexicon[s].totalCount = lexicon[s].totalCount + lexicon[lexicon.ElementAt(i).Key].totalCount;

                        addPageList(lexicon[s].pageList, lexicon[lexicon.ElementAt(i).Key].pageList);
                    }

                    lexicon.Remove(lexicon.ElementAt(i).Key);
                }
                
                tempArr=new List<string>[queries.Count];
                ii = 0;
                foreach (var v in queries)
                {
                    tempArr[ii] = userEnd(v);
                    ii++;
                }
                Console.WriteLine("Acuracy phase 4:");
                for (int i = 0; i < queries.Count;i++ )
                {
                    computeMatrix(originalArr[i], tempArr[i]);
                }
                Console.WriteLine(  (double)(Finalmatrix[0, 0] + Finalmatrix[1, 1]) / Defs.filesCount/queries.Count);
                
                Console.WriteLine("\n\nSee files for details:");
                Console.WriteLine("Queries.txt for input queries");
                Console.WriteLine("Phase[1 to 4].txt for results of each query against eash phase");
                Console.WriteLine("These files are in append mode, so please delete them before executing the program. Otherwise there will be duplication in files (but the code will work correctly as these files are only to output results).");

                Console.WriteLine("\n******  ");


            }
            catch (KeyNotFoundException e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
