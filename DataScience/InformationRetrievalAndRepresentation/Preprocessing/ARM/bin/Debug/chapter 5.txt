1.	Introduction
For a human performing certain task, an important aspect is the trust in various information sources that can potentially aid in performing the task. For instance, the person may have experienced in the past that the manual for the task contains a lot of flaws, whereas it was experienced that a knowledgeable colleague can immediately aid in an adequate fashion. As a result the trust level for the manual will be much lower than the trust level for the colleague, and a support system offering the manual will most likely be ignored. This illustrates two main functional properties of trust states as cognitive states that are often considered (Jonker and Treur, 2003):
(1)	A trust state accumulates experiences over time
(2)	Trust states are used in decision making by choosing more trusted options above less trusted options
In trust research, a variety of computational models have been proposed for human trust, see e.g., (Jonker and Treur, 2003; Jonker and Treur, 1999; Falcone and Castelfranchi, 2004; Hoogendoorn, Jaffry, and Treur, 2008). Such models often assume that trust values over time depend on the experiences of the human with the specific object of trust. In (Hoogendoorn, Jaffry, and Treur, 2008) an additional factor is taken into account, namely how the object of trust performs relative to its competitors. Most of such models consider experiences and trust as cognitive concepts and exclude affective factors. This contrasts with how persons in general experience trust and trust-affecting experiences, which, for example, may go hand in hand with strong feelings of disappointment or insecurity. Much work reports interactions between cognitive and affective aspects for a variety of cases (Eich, Kihlstrom, Bower, Forgas and Niedenthal, 2000; Forgas, Laham and Vargas, 2005; Forgas, Goldenberg and Unkelbach, 2009; Niedenthal, 2007; Schooler and Eich, 2000; Winkielman, Niedenthal and Oberman, 2009), without relating this explicitly to neurological findings or theories. In the current paper, neurological theories on emotion and feeling are adopted as a basis of inspiration for a computational model for trust dynamics that models such interactions. The computational model, which is inspired by neurological theories on the embodiement of emotions as described, for example, in (Winkielman, Niedenthal and Oberman, 2009; Damasio, 1994; Damasio, 1996; Damasio, 1999; Damasio, 2004), describes how trust dynamics relates to experiences with (external) sources, both from a cognitive and affective perspective. More specifically, in accordance with, for example (Damasio, 1999; Damasio, 2004), for feeling the emotion associated to a mental state, a converging recursive body loop is assumed. In addition, based on Hebbian learning (cf. Hebb, 1949; Bi and Poo, 2001; Gerstner  and Kistler,  2002) for the strength of the connections to the emotional responses an adaptation process is introduced, inspired by the Somatic Marker Hypothesis (Damasio, 1994; Damasio, 1996). Compared to more detailed neural models, the model presented here abstracts from more fine-grained descriptions of neurons, and, for example, their biochemistry and spiking patterns. States in the presented model may be viewed as abstract descriptions of states of neurons or rather of groups of neurons. Such a more abstract representation provides a less complex and more manageable model, while the assumption is that it can still show the essential dynamics from a higher level perspective. 
In this paper, first in Section 2 Damasio�s theory on the generation of feelings is briefly introduced. In Sections 3 and 4 the model is described in detail, including the adaptation based on Hebbian learning. Section 5 presents some simulation results. In Section 6 it is discussed how functional properties of trust states as cognitive states were formulated and automatically verified. Finally, Section 7 is a discussion.
2.	On the Interaction Between Affective and Cognitive Aspects
Cognitive states of a person, such as sensory or other representations often induce emotions felt within this person, as described by neurologist Damasio (Damasio, 1999; Damasio, 2004); for example: �Through either innate design or by learning, we react to most, perhaps all, objects with emotions, however weak, and subsequent feelings, however feeble.� (Damasio, 2004, p. 93). In some more detail, emotion generation via a body loop roughly proceeds according to the following causal chain; see (Damasio, 1999; Damasio, 2004):
cognitive state  ?  preparation for bodily response  ?   bodily response  ?  
sensing the bodily response  ?  sensory representation of the bodily response  ?  feeling
The body loop (or as if body loop) is extended to a recursive body loop (or recursive as if body loop) by assuming that the preparation of the bodily response is also affected by the state of feeling the emotion as an additional causal relation: feeling  ?  preparation for the bodily response. Such recursiveness is also assumed by Damasio (Damasio, 2004), as he notices that what is felt by sensing is actually a body state which is an internal object, under control of the person:
�The brain has a direct means to respond to the object as feelings unfold because the object at the origin is inside the body, rather than external to it. (�) The object at the origin on the one hand, and the brain map of that object on the other, can influence each other in a sort of reverberative process that is not to be found, for example, in the perception of an external object.� (Damasio, 2004, p. 91)
Within the model presented in this paper, both the bodily response and the feeling are assigned a level or gradation, expressed by a number. The causal cycle is modelled as a positive feedback loop, triggered by a mental state and converging to a certain level of feeling and body state. 
Another neurological theory addressing the interaction between cognitive and affective aspects can be found in Damasio�s Somatic Marker Hypothesis; cf. (Damasio, 1994; Damasio, 1996; Damasio, 2004; Bechara,  and Damasio, 2004). This is a theory on decision making which provides a central role to emotions felt. Within a given context, each represented decision option induces (via an emotional response) a feeling which is used to mark the option. For example, a strongly negative somatic marker linked to a particular option occurs as a strongly negative feeling for that option. Similarly, a positive somatic marker occurs as a positive feeling for that option. Damasio describes the use of somatic markers in the following way: 
�the somatic marker (..) forces attention on the negative outcome to which a given action may lead, and functions as an automated alarm signal which says: beware of danger ahead if you choose the option which leads to this outcome. The signal may lead you to reject, immediately, the negative course of action and thus make you choose among other alternatives. (�)  When a positive somatic marker is juxtaposed instead, it becomes a beacon of incentive..� (Damasio, 1994, pp. 173-174)
Somatic markers may be innate, but may also by adaptive, related to experiences:
�Somatic markers are thus acquired through experience, under the control of an internal preference system and under the influence of an external set of circumstances ...� (Damasio, 1994, p. 179)
In the model introduced below, this adaptive aspect will be modelled as Hebbian learning; cf. (Hebb, 1949; Bi and Poo, 2001; Gerstner  and Kistler,  2002). Viewed informally, in the first place it results in a dynamical connection strength obtained as an accumulation of experiences over time (1). Secondly, in decision making this connection plays a crucial role as it determines the emotion felt for this option, which is used as a main decision criterion (2). As discussed in the introduction, these two properties (1) and (2) are considered two main functional, cognitive properties of a trust state. Therefore they give support to the assumption that the strength of this connection can be interpreted as a representation of the trust in the option considered.
3.	Incorporating Affective Aspects in a Trust Model 
Informally described theories in scientific disciplines, for example, in biological or neurological contexts, often are formulated in terms of causal relationships or in terms of dynamical systems. To adequately formalise such a theory the hybrid dynamic modelling language LEADSTO has been developed that subsumes qualitative and quantitative causal relationships, and dynamical systems; cf. (Bosse, Jonker, Meij and Treur, 2007). This language has been proven successful in a number of contexts, varying from biochemical processes that make up the dynamics of cell behaviour (Jonker, Snoep, Treur, Westerhoff and Wijngaards, 2008) to neurological and cognitive processes (Bosse, Jonker, Los, Torre  and Treur, 2007; Bosse, Jonker and Treur,  2007; Bosse, Jonker and Treur, 2008). Within LEADSTO a temporal relation a ?? b denotes that when a state property a occurs, then after a certain time delay (which for each relation instance can be specified as any positive real number), state property b will occur. In LEADSTO both logical and numerical calculations can be specified in an integrated manner; a dedicated software environment is available to support specification and simulation. 
An overview of the model for how trust dynamics emerges from the experiences is depicted in Figure 1. 
  
Figure 1.  Overview of the model for trust dynamics
How decisions are made, given these trust states is depicted in Figure 2. These pictures also show representations from the detailed specifications explained below. However, note that the precise numerical relations between the indicated variables V shown are not expressed in this picture, but in the detailed specifications of properties below, which are labeled by LP1 to LP11 as also shown in the pictures. 
 

Figure 2.  Overview of the model for trust-based decision making
The detailed specification (both informally and formally) of the model is presented below. Here capitals are used for (assumed universally quantified) variables. First the part is presented that describes the basic mechanisms to generate a belief state and the associated feeling. The first dynamic property addresses how properties of the world state can be sensed. 
LP1  Sensing a world state
If 	world state property W occurs of strength V
then 	a sensor state for W of strength V will occur.
world_state(W, V) ??  sensor_state(W, V)

Note that this generic dynamic property is used for a specific world state, for experiences with the different options and for body states; to this end the variable W is instantiated respectively by w, exp1 and exp2, b1 and b2. From the sensor states, sensory representations are generated according to the dynamic property LP2. Note that also here for the example the variable P is instantiated as indicated.
LP2  Generating a sensory representation for a sensed world or body state
If 	a sensor state for world state or body state property P with level V occurs, 
then 	a sensory representation for P with level V will occur.
sensor_state(P, V)  ??  srs(P, V)

For a given world state representations for a number of options are activated:
LP3  Generating an option for a sensory representation of a world state
If 	a sensory representation for w with level V occurs 
then 	a representation for optinon o with level V will occur
srs(w, V) ??  rep(o, V)  

Dynamic property LP4 describes the emotional response to the person�s mental state in the form of the preparation for a specific bodily reaction. Here the mental state comprises a number of cognitive and affective aspects: options activated, experienced results of options and feelings. This specifies part of the loop between feeling and body state. This property uses a combination model based on a function g(?1, ?2, V1, V2, V3 ,?1, ?2, ?3)  including a threshold function. For example,
g(?1, ?2, V1, V2, V3,?1, ?2, ?3)  = th(?1, ?2,?1V1 + ?2V2 + ?3V3)
with V1, V2, V3 activation levels and ?1, ?2, ?3  weights of the connections, and  threshold function th(?1, ?2,V)  = 1/(1+e-?2(V-?1) ) with threshold ?1 and steepness ?2.
LP4a  From option activation and experience to preparation of a body state (non-competitive case)
If 	option o with level V1 occurs 
   and	feeling the associated body state b has level V2
   and 	an experience for o occurs with level V3
   and 	the preparation state for b has level V4
then 	a preparation state for body state b will occur with level V4+? (g(?1, ?2, V1, V2, V3,?1, ?2, ?3)-V4) ?t.
rep(o, V1)  &  feeling(b, V2)  &  srs(exp, V3) &  preparation_state(b, V4) 
??  preparation_state(b, V4+ ? (g(?1, ?2, V1, V2, V3,?1, ?2, ?3)-V4) ?t)

For the competitive case also the inhibiting cross connections from one represented option to the body state induced by another represented option are used. A function involving these cross connections was defined, for example 
h(?1, ?2, V1, V2, V3, V21,?1, ?2, ?3, ?21) = th(?1, ?2,?1V1 + ?2V2 + ?3V3 - ?21V21)

for two considered options, with ?21  the weight of the suppressing connection from represented option 2 to the preparation state induced by option 1. 

LP4b  From option activation and experience to preparation of a body state (competitive case)
If 	option o1 with level V1 occurs 
   and	option o2 with level V21  occurs 
   and	feeling the associated body state b1 has level V2
   and 	an experience for o1 occurs with level V3
   and 	the preparation state for b1 has level V4
then 	a preparation state for body state b1 will occur with 
	level V4+ ? (h(?1, ?2, V1, V2, V3, V21,?1, ?2, ?3, ?21)-V4) ?t.
rep(o1, V1)  &  rep(o2, V21)  & feeling(b1, V2)  &  srs(exp1, V3) &  preparation_state(b1, V4) 
??  preparation_state(b1, V4+ ? (h(?1, ?2, V1, V2, V3, V21,?1, ?2, ?3, ?21)-V4) ?t)

Dynamic properties LP5, LP6, and LP7 together with LP2 describe the body loop.
LP5  From preparation to effector state for body modification
If 	preparation state for body state B occurs with level V,
then 	the effector state for body state B with level V will occur.
preparation_state(B, V)  ??  effector_state(B, V)

LP6  From effector state to modified body state
If 	the effector state for body state B with level V occurs,
then 	the body state B with level V will occur.
effector_state(B, V)  ??  body_state(B, V)

LP7  Sensing a body state
If 	body state B with level V occurs,
then 	this body state B with level V will be sensed.
body_state(B, V)   ??   sensor_state(B, V)

LP8  From sensory representation of body state to feeling
If 	a sensory representation for body state B with level V occurs,
then 	B is felt with level V.
srs(B, V)  ??   feeling(B, V)


Alternatively, dynamic properties LP5 to LP7 can also be replaced by one dynamic property LP9 describing an as if body loop as follows.

LP9  From preparation to sensed body state
If 	preparation state for body state B occurs with level V,
then 	the effector state for body state B with level V will occur.
preparation_state(B, V)  ??  srs(B, V)

For the decision process on which option Oi  to choose, represented by action Ai, a winner-takes-it-all model is used based on the feeling levels associated to the options; for an overview, see Fig. 2. This has been realised by combining the option representations Oi with their related emotional responses Bi in such a way that for each i the level of the emotional response Bi has a strongly positive effect on preparation of the action Ai  related to option Oi  itself, but a strongly suppressing effect on the preparations for actions Aj  related to the other options Oj for j ? i. As before, this is described by a function 
h(?1, ?2, V1, � ,Vm, U1, � ,Um,?11, �,?mm)

with Vi  levels for representations of options Oi and Ui levels of preparation states for body state Bi related to options Oi and ?ij  the strength of the connection between  preparation states for body state Bi and preparation states for action Aj.
LP10  Decisions based on felt emotions induced by the options
If 	options Oi with levels Vi occur,
   and	preparation states for body state Bi related to options Oi occur with level Ui,
   and	the preparation state for action Ai  for option Oi has level Wi
then 	the preparation state for action Ai  for option Oi will occur 
	with level Wi  + ? (h(?1, ?2, V1, � ,Vm, U1, � ,Um,?11, .. ?mm) - Wi) ?t 
rep(O1, V1)  &  � & rep(Om, Vm)  &  
preparation_state(B1, U1) &  � & preparation_state(Bm, Um) &
preparation_state(Ai, Wi)
 ??  preparation_state(Ai, Wi  + ? (h(?1, ?2, V1, � ,Vm, U1, � ,Um,?11, .. ?mm) - Wi) ?t)

LP11  From preparation to effector state for an action
If 	preparation state for action A occurs with level V,
then 	the effector state for action A with level V will occur.
preparation_state(A, V)  ??  effector_state(A, V)

4.	The Hebbian Adaptation Process
From a neurological perspective the strength of a connection from an option to an emotional response may depend on how experiences are felt emotionally, as neurons involved in the option, the preparation for the body state, and in the associated feeling will often be activated simultaneously. Therefore such a connection from option to emotional response may be strengthened based on a general Hebbian learning mechanism (Hebb, 1949; Bi and Poo, 2001; Gerstner  and Kistler,  2002) that states that connections between neurons that are activated simultaneously are strengthened, similar to what has been proposed for the emergence of mirror neurons; e.g., (Keysers and Perrett,  2004; Keysers  and Gazzola,  2009). This principle is applied to the strength ?1  of the connection from option 1 to the emotional response expressed by body state b1. The following learning rule takes into account a maximal connection strength 1, a learning rate ?, and an extinction rate ?.
LP12 Hebbian learning rule for the connection from option to preparation
If 	the connection from option o1 to preparation of b1 has strength ?1
  and	the option o1 has strength V1 
  and 	the preparation of b1 has strength V2 
  and 	the learning rate from option o1 to preparation of b1 is ?
  and 	the extinction rate from option o1 to preparation of b1 is ?
then 	after ?t  the connection from option o1 to preparation state b1 will have 
	strength ?1 + (?V1V2(1 - ?1) - ??1) ?t.
has_connection_strength(rep(o1), preparation(b1), ?1) &  rep(o1, V1)  &  preparation(b1, V2)  &  
has_learning_rate(rep(o1), preparation(b1), ?)  &   has_extinction_rate(rep(o1), preparation(b1), ?)    
??   has_connection_strength(rep(o1), preparation(b1), ?1 + (?V1V2 (1 - ?1) - ??1) ?t)

By this rule through their affective aspects, the experiences are accumulated in the connection strength from option o1 to preparation of body state b1, and thus serves as a representation of trust in this option o1. A similar Hebbian learning rule can be found in (Gerstner  and Kistler,  2002, p. 406). 
5.	Example Simulation Results
The model described in Section 3 has been used to generate a number of simulation experiments for non-competitive and competitive cases (see Figure. 3 for some example results). To ease the comparison between these cases the same model parameter values were used for these examples (see Table 1). In Figure. 3a) example simulation results are shown for the non-competitive case. Here the subject is exposed to an information source that provides experience values 0 respectively 1 alternating periodically in a period of 200 time steps each. In this figure it can be observed that change in experience leads to changes in the connection strengths (representing trust) as well as the action effector states. Furthermore, the decrease in the connection strengths representing trust due to a bad experience (0) takes longer than the increase due to a good experience (1), which can be explained by the higher value of the learning rate than of the extinction rate.
Table 1. Parameter values used in the example simulations

Parameter	Value	Meaning
�1	0.95	threshold value for preparation state and action effector state
�2	10, 100	steepness value for preparation state, action effector state 
?	0.90	activation change rate 
?	0.80	learning rate of connection from option representation to preparation
?	0.10	extinction rate of connection from option representation to preparation
?t	0.90	time step
?s, ?a
	0.50
	suppressing weight from option representation to preparation state and from preparation state to the action state (competitive case)

In Figures 3b), c) and d), the simulation results are shown for the competitive case with two competitive options having suppression weight 0.5 from option representation to preparation state and from preparation state to the action state. In this case  the subject is exposed to two information sources that provides experience values 0 respectively 1 alternating periodically in a period of 200 time steps each, in a reverse cycle with respect to each other (see Figure. 3b)). Here change in experience changes the connections representing trust as well as the action effector states. Moreover, in comparison to the non-competitive case, the learning is slow while decay is fast, which is due to the presence of competition. Finally Figure. 3 shows that the connection strengths in the presented model exhibit the two fundamental functional properties of trust discussed in Section 1, namely that trust is based on accumulation of experiences over time (see Figure. 3c)) and that trust states are used in decision making by choosing more trusted options above less trusted ones (see Figure. 3d)).
 a)  b)
 c)  d)

Figure 3.  Simulation Results: experience, connection representing trust, and action effector state for a non-competitive case in a) and a competitive case in b), c) and d) respectively
6.	Verification of Functional Properties of Trust 
The two functional properties of trust states formulated in the introduction are: (1) A trust state accumulates experiences over time, and (2) Trust states are used in decision making by choosing more trusted above less trusted options. These properties characterise trust states from a functional, cognitive perspective. Therefore any model or computational or physical realisation claimed to describe trust dynamics has to (at least) satisfy these properties. Such properties can be formalized in a temporal logical language, and can be automatically verified for the traces that have been generated using the proposed model. In this section this verification of properties is discussed. First, the language used to verify these properties is explained. Thereafter the properties and the results of the verification are discussed.
The verification of properties has been performed using a language called TTL (Temporal Trace Language), that features a dedicated editor and an automated checker; cf. (Bosse, Jonker, Meij, Sharpanskykh and Treur, 2009). This predicate logical temporal language supports formal specification and analysis of dynamic properties, covering both qualitative and quantitative aspects. TTL is built on atoms referring to states of the world, time points and traces, i.e. trajectories of states over time. In addition, dynamic properties are temporal statements that can be formulated with respect to traces based on the state ontology Ont in the following manner. Given a trace ? over state ontology Ont, the state in ? at time point t is denoted by state(?, t). These states can be related to state properties via the infix predicate |=, where state(?, t) |= p denotes that state property p holds in trace ? at time t. Based on these statements, dynamic properties can be formulated using quantifiers over time and traces and the usual first-order logical connectives such as ?, ?, ?, ?, ?, ?. For more details, see (Bosse, Jonker, Meij, Sharpanskykh and Treur, 2009).
In order to be able to automatically verify the properties upon the simulation traces, they have been formalised. From the computational verification process it was found that indeed they are satisfied by the simulation traces of the model for which they were verified. The first functional property (1), specifying that a trust state accumulates experiences over time, is split up into a number of properties. First, two properties are specified which express trust accumulation for the non-competitive case, whereby the connections for the respective trustees are not influenced by experiences with competitors.
P1.1  Connection strength increases with more positive experience (non-competitive case)
If a sensor state indicates a particular value V1 of an experience E, and E is an experience for trustee T, and the current strength of the connection for trustee T is V2, and V1 is higher than V2, then the connection strength will remain the same or increase.
Note that in the property, the effector state merely has one argument, namely the trustee with the highest effector state value.
7.	Discussion
In this paper a computational model for trust dynamics was introduced incorporating the reciprocal interaction between cognitive and affective aspects based on neurological theories that address the role of emotions and feelings. The introduced model describes more specifically how considered options and experiences generate an emotional response that is felt. For feeling the emotion, based on elements taken from (Damasio, 1999; Damasio, 2004; Bosse, Jonker and Treur, 2008), a converging recursive body loop is included in the model. An adaptation process based on Hebbian learning (Hebb, 1949; Bi and Poo, 2001; Gerstner  and Kistler,  2002), was incorporated, inspired by the Somatic Marker Hypothesis described in (Damasio, 1994; Damasio, 1996; Bechara,  and Damasio, 2004), and as also has been proposed for the functioning of mirror neurons; e.g., (Keysers and Perrett,  2004; Keysers  and Gazzola,  2009). The model was specified in the hybrid dynamic modelling language LEADSTO, and simulations were performed in its software environment; cf. (Bosse, Jonker, Meij and Treur, 2007). 
It has been shown that within the model the strength of the connection between a considered option and the emotional response induced by it, satisfies two properties that are considered as two central functional properties of a trust state as a cognitive state (Jonker and Treur, 2003): (1) it accumulates experiences, and (2) it is a crucial factor used in deciding for the option. This provides support for the assumption that the strength of this connection can be interpreted as a representation of the trust level in the considered option.
Models of neural processes can be specified at different levels of abstraction. The model presented here can be viewed as a model at a higher abstraction level, compared to more detailed models that take into account more fine-grained descriptions of neurons and their biochemical and/or spiking patterns. States in the presented model can be viewed as abstract descriptions of states of neurons or as representing states of groups of neurons. An advantage of a more abstract representation is that such a model is less complex and therefore may be less difficult to handle, while it still shows the essential dynamics. An interesting issue for further research is how such a more abstract model can be related to more detailed models, and in how far patterns observed in more specific models also are represented in such a more abstract model.
