Introduction
Within multi-agent systems, the concept of trust is often a key element in the relationship between agents. For example, an agent�s trust in other agents might influence the selection process for task delegation. As a result, a variety of models related to trust have been proposed, see e.g. (Falcone and Castelfranchi, 2004; Hoogendoorn, Jaffry and Treur, 2008; Jonker and Treur, J., 1999[3],[4],[7]). In most of these trust models, the trust an agent has in another agent is seen as the result of experiences the agent has with that other agent, and certain personality attributes (see e.g. Hoogendoorn, Jaffry and Treur, 2008; Jonker and Treur, J., 1999)[4], [7]), and is independent of trust in other agents. More complex trust models also address a more advanced interpretation of the experiences, for example using a cognitive attribution process (see e.g. Falcone and Castelfranchi, 2004[3]).
Merely considering experiences the agent has with a single other agent is however not always sufficient nor realistic. Sometimes experiences with an agent or the trust value in an agent can influence the trust in another agent. Imagine the following scenario: somebody wants to buy a new television. He currently has experience with two websites that sell televisions (referred to as w1 and w2). Given these experiences, he has a high trust in w1 and a low trust in w2. As a result, he decides to buy the television at w1. Unfortunately, he gets a negative experience as the television is delivered with a broken screen and they do not accept the liability. Of course, this negative experience brings down the trust the person has in w1, however it might also result in an update of the trust in w2. Essentially, there are three options for this dependency: (1) a positive trust dependency, whereby negative experiences with one trustee have a negative influence upon the trust level of another trustee as well, and a negative experience has a negative influence upon the trust level of other trustee; (2) a negative trust dependency, whereby a negative experience with one trustee has a positive influence upon the trust of another trustee and a positive experience a negative influence, and (3) no trust dependency, meaning that there is no influence because the agent does not perceive a relationship. For the example, a negative trust dependency between w1 and w2 could be the case if the human perceives them as substitutable competitors (given the experience with w1, w2 was actually not that bad) and a positive trust dependency in case the human sees the trustees as representatives from the same group (all Internet stores cannot be trusted).
In order to address these dependencies, a possibility is to incorporate such relationships in the trust model explicitly. This approach is for instance taken in [4](Hoogendoorn, Jaffry and Treur, 2008), and has been refined in this paper. A disadvantage of such an approach is however that this does not allow the reuse of the currently existing trust models, each having their own specific pros and cons. Another option which is therefore investigated in this paper is to model a process which transforms the objective experiences into subjective experiences for each of the agents, thereby considering the dependencies as expressed before. This does allow for the reuse of existing trust models. Hereby, first a model to translate objective into subjective experiences is created with a number of parameters that can be set to tune the precise relationship between the experiences with each of the trustees. Simulation runs have been performed using both approaches to investigate whether the models are able to generate the desired behavior. 
This paper is organized as follows. First, a trust model which incorporates interdependency directly is presented in Section 2, followed by the model to translate objective into subjective experiences and feed this into an existing trust model in Section 3. Section 4 presents compatible existing trust models and simulations results using all variants are presented in Section 5. Section 6 presents related work, and finally, Section 7 concludes the paper and gives directions for future work.
Relative Trust Model
This section describes a dedicated trust model of human trust on interdependent trustees incorporating the interdependence in the trust model itself. Note that this model has been inspired on [4](Hoogendoorn, Jaffry and Treur, 2008) but has been slightly simplified to obtain higher transparency. In this model trust values have some degree of interdependency (competition, neutral, cooperation) where the human trust on a trustee depends on the relative trust in the trustee in comparison to the trust on other trustees. This model includes human personality characteristics like trust decay, flexibility and the degree of interdependency among trustees. Figure 1 shows the dynamic relationships in the model. Here, the trust value of one trustee is directly dependent on the trust values on other trustees. 
 
Figure 1. Dynamic relationships in relative trust model
In the model described in Figure 1 it is assumed that trustees {S1, S2, . . . Sn} provide experiences (Ei(t)) to human at each time step continuously, these experiences have value from the continuous interval [-1, 1]. Here, -1 indicates the most negative experience whereas 1 is the most positive. The human updates the trust value on a trustee by keeping trust values on other trustees under consideration along with the interdependency relation of the trustees (?). Furthermore, human personality attributes like trust flexibility, expressing how much an experience counts (�) and autonomous trust decay (?), indicating how fast trust goes back to a neutral value when there are no new experiences also play a role in this process. On receiving an experience Ei(t) from a trustee Si at time point t, the human trust on Si at the next time point (t+1) is the sum of the human trust on Si (Ti(t)) and the experience Ei(t) minus the autonomous decay in trust, this is expressed as follows

T_i (t+1)=(1-�)*T_i (t)+�*E_i (t)-?*T_i (t)
  
In differential form
(dT_i)/dt=�*(E_i (t)-T_i (t))-?*T_i (t)
  
To introduce the notion of trustees interdependence in this model, the relative trust of the human is defined. The relative trust of the human on Si at time point t (ti(t)) is the difference between the human�s projected trust on Si (T�i(t)) and the average of the human�s projected trust on all trustees times the degree of trustees interdependency ?. The human projected trust on Si at time point t is the human trust on Si projected from the range [-1, 1] to [0, 1] as follows:
T_i^' (t)=((T(t)+1))/2
  
The human�s relative trust on trustee Si at time point t can be calculated as follows
t_i (t)=?*(T_i^' (t)-(?_(j=1)^n�?T_j^' (t) ?)/n)
  
Where ? is the degree of trustees interdependency in the range [-1, 1]. A negative value of ? denotes cooperation while a positive value represents competition among the trustees and n is the number of trustees. The human relative trust is designed to fulfill the requirements for different interdependencies among trustees as shown in Table 1.
Table 1. Value of Relative Trust with different interdependencies
Trust Value	Interdependency	Relative Trust Value
Above average	Cooperation	Negative
Above average	Competition	Positive
Equal to average	Cooperation	Zero
Equal to average	Competition	Zero
Below average	Cooperation	Positive
Below average	Competition	Negative


This relative trust is introduced as a bias in the above trust equation with experience and trust values as follows


 
Subjective Experience Based Trust Model
This section describes a model of human trust on interdependent trust values using experience transformation, thereby allowing for the reuse of existing trust models. In this model the trust values for different trustees have some degree of interdependency among themselves (competition, neutral, cooperation), similar to the model presented in Section 2. In this case, the human trust on a trustee depends on the relative experiences with the trustee in comparison to the experiences it has obtained from the other trustees. This model includes human personality characteristics like trust decay, flexibility, trust bias on experience, and degree of interdependency among trustees. Figure 2 shows the dynamic relationships in the model used. 
The model expressed in Figure 2 is composed of two models: one for transforming an objective experience with a trustee into a subjective experience (on the left), and another model for updating the human trust value based on this subjective experience (on the right). In this case, it is assumed that the trustee continuously provides the human with objective experiences (Ei(t)) which are transformed into subjective experiences (E�i(t)) also on the interval [-1, 1]. This transformation depends on the human trust bias on experience (a) and the degree of interdependency among trustees (?). Thereafter this experience is passed on to the trust model (which can in principle be any compatible existing trust model). The computational models for Figure 2 are described in the following sections.
 
Figure 2. Dynamic relationships in subjective experience based trust model
3.1 	Transforming Objective into Subjective Experience Using Trust Interdependency
This section explains the design of the mathematical model for transforming an objective experience provided by the trustee into a subjective experience using trust interdependency. The human�s subjective experience at any time point t is based on a combination of two parts: the trust biased part, and the trustee�s interdependency part. For the latter part an important indicator is the human�s relative experience of trustee Si (Ei(t)) at time point t (denoted by ti(t)): the difference of the human�s experience of Si to the average human�s experience from all trustees at time point t times the degree of trustees interdependency ?. This is calculated as follows:

  
Here it should be noted that E�i(t) is the objective experience from trustee Si at time point t projected from the interval [-1, 1] onto the interval [0, 1]. The trust interdependency parameter (?) can take values from the continuous interval [-1, 1], where negative and positive values of ? denote cooperation and competition respectively similar to the model presented in Section 2. The human�s relative experience is designed to fulfill the requirements as expressed in Table 2 for different interdependencies among trustees.
Table 2. Change in objective experience with interdependencies
Objective experience	Interdependency	Change in Objective Experience
Above average	Cooperation	Negative
Above average	Competition	Positive
Equal to average	Cooperation	Zero
Equal to average	Competition	Zero
Below average	Cooperation	Positive
Below average	Competition	Negative







To calculate the experience when taking the interdependency into account, the relative experience at time point t is added to the objective experience Ei(t) from trustee Si at time point t:

 
In addition, a trust bias is included when transforming the objective experience into a subjective experience at time point t. Hereby the factor a (trust bias on experiences) is used to take a percent of the value of the trust a human has in a trustee i at time point t (Ti(t)) and (1 � a) percent of the trustees experience in combination with the interdependency relation:  

  
For further smoothing of this transformation (following approaches often found in neurological modeling), a threshold function th(�1, �2, Vi) is used with threshold �1 and steepness �2, defined as follows:

  
As the value of this threshold function resides on the interval [0, 1], this value is projected onto the interval [-1,1] according to the following formula: 

 
3.2   	A Sample Trust Model
In order to illustrate how this experience can be used in a trust model, a very basic trust model is explained here. This model is an extension of a model present in the literature [7](Jonker and Treur, J., 1999), which accumulates experiences over time and updates trust accordingly. Other trust models that are also suitable and have been found in existing literature are expressed in Section 4. In the trust model presented below, the trust is based on experiences in combination with two personality characteristics trust flexibility � and autonomous trust decay ? (defined similarly as in Section 2). In this model, it is assumed that the human trust on a trustee Si at time point t is Ti(t) (a value from the interval [-1,1]). On receiving a subjective experience Esi(t) from trustee Si trust of human on the trustee at time point t + ?t adapts as follows:

  
in differential form:


 
Here it could be noted that trust would be in equilibrium if, 

 
4   Experience-Based Trust Models from Literature
This section describes two other experience based models taken from literature, and it will be shown that they also can be combined with the subjective experience model as proposed in Section 3. In [8](Jonker and Treur, J., 2003) a simple trust model is proposed which accumulates experiences over time without temporal discounting of experiences. The model is defined by the following equation:

 
In the equation, � is the trust flexibility (i.e., how much an experience counts) while Ei(t) is the experience with trustee i (in this case, the subjective experience if it is integrated with the proposed model). Similarly, another trust model described in (Jonker and Treur, J., 2003)[8] also accommodates experiences from the environment (but this time in discounted form), and the trust values are then updated as follows (with the same parameters): 

 
Here it can be noted that the exponential part in this model supports temporal discounting of experiences over time. Other, more complex trust models that are based upon experiences exist as well, see Section 6 for more details.
5   Simulation Results
The models described in Section 2, 3 and 4 have been used to conduct a number of simulation experiments to see what patterns emerge using the different models, and how the different models compare. Here, first the experimental configurations of the simulations are described briefly followed by some of the simulation results. 
The experimental configurations for the simulations are shown in Table 3. During the experiments, the initial trust value of the human on all trustees is considered neutral (zero) and the effect of the degree of trust interdependency has been observed using three different values [-1, 0, 1] of ? for cooperation, neutral and competition. The models were analyzed against several experience sequences. An experience sequence is the series of experience values received by human from different trustees over time. The experience sequences used in the graphs presented in this section are shown in Figure 3. Here, three trustees are assumed namely S1, S2 and S3, giving experiences E1, E2 and E3 respectively, where S2 and S3 give positive (1) and negative (-1) experience values respectively for the whole experiment (2000 time steps) while S1 gives negative (-1) and positive (l) alternating periodically in a period of 500 time steps each (see Figure 3a). Moreover, in Figure 3b, S1 gives positive (1) and neutral (0) experiences and S2 gives negative (-1) and positive (1) alternating periodically in a period of 500 time steps each, while S3 gives neutral (0) experiences for the whole experiment. This approach of using a shift in experience after a certain number of positive and negatives experiences is inspired on different empirical validations techniques of experience based trust models presented in literature (see e.g. [6]). The presented experience sequences in this paper are selected to show adaptation of different trust models over sudden shifts in the behavior of the trustees.
Table 3. Model configurations used for simulation experiments
Parameters	Symbols	Values
Threshold	�1	0
steepness		�2	1
degree of trustees interdependency	?	[-1, 0, 1]
Trust bias on experience	a	0.25
autonomous trust decay	?	0.10
trust flexibility		�	0.10
time step	?t	0.10
initial trust on trustees	T(0)	0, 0, 0



 a)  b)
Figure. 3. Experience Sequence used for Simulations
5.1 	Experiment 1: Relative trust model
In this experiment the experience sequence of Figure 3a is used for the relative trust model as explained in Section 2. The results are shown in Figure 4. In Figure 4a, 4b and 4c the trust values T1, T2 and T3 of the trustee S1, S2 and S3 are shown on the y-axis for competitive, neutral and cooperative trust interdependency, respectively, while time is on the x-axis. In Figure 4a the interdependency is competition, so it can be observed that when S1 gives positive experiences, the trust values of the other two trustees become lower and vice-versa. In Figure 4b, ? is set to zero that means the trust values of trustees are mutually independent which shows in the graphs as well. In Figure 4c cooperation is introduced by setting ? = -1. Hence in Figure 4c it can be seen that when S1 gives positive experiences it also effects the trust values of the other two trustees positively and vice-versa. Furthermore in the competitive case (Figure 4a) the maximum and minimum values of trust attained is higher than the neutral and cooperative cases. This is because competition gives an additional increase to the positive and negative trust values towards extremes while cooperation brings them closer to each other.

 a)  b)
 c)
Fig. 4. Dynamics of relative trust model a) ?=1, b) ?=0 c) ?=-1.
5.2 	Experiment 1: Subjective experience based in combination with simple trust model
In this experiment the experience sequence of Figure 3a was used for the subjective experience based model described in Section 3 in combination with the simple trust model shown in Section 3.2. In Figure 5a, 5c and 5e the values of the subjective experiences for trustee S1, S2 and S3 are shown on the y-axis for competitive, neutral and cooperative trust interdependency respectively and in Figure 5b, 5d and 5f the respective trust values are shown. The patterns shown resemble the ones shown for the model presented in Section 2. In Figure 5b it can be noted that when trustee S1 gives negative objective experiences, due to the competition values the subjective experiences of S2 and S3 become slightly higher and vice-versa. In Figure 5c as the interdependency value ? is 0, no mutual effect of the subjective experiences on each other could be observed. The curves shown in Figure 5c also show the effect of the threshold function used in the model. In Figure 5e where ? = -1, representing cooperation, it can be seen that an increase in objective experience of S1 gives a positive boost to the subjective values of S2 and S3 and vice-versa. Furthermore, in the competitive case (Figure 5a) the maximum and minimum values of the subjective experiences are higher than for the neutral and cooperative cases. This is due to that fact that competition gives an additional increase to the positive and negative subjective experience values towards extremes while cooperation brings them closer to each other. Similarly these subjective experiences have a direct effect on the trust values of the trustees in each case (see Figure 5b, 5d, 5e).

 a)  b)
 c)  d)
 e)  f)
Figure. 5. Dynamics of subjective experience based trust model a) subjective experience for ?=1 b) Trust for ?=1, c) subjective experience for ?=0, d) Trust for ?=0 e) subjective experience for ?=-1, f) Trust for ?=-1.
5.3 	Experiment 1: Subjective experience based in combination with trust model from literature
In this experiment the experience sequence of Figure 3a was used for the subjective experience for trust model from literature described in Section 4. One of the two models described in this literature is not accounting temporal discounting of experiences while the second does. In Figure 5a, 5c and 5e simulation results for the model without temporal discounting of experience are shown. In this case it can be observed that this simple model does not show the type of results as expected and generated by the other models. In Figures 6b, 6d, and 6f the simulation results for model incorporating temporal discounting of experiences are shown. In Figure 6b it can be noted that when trustee S1 gives negative objective experiences, due to the competition values the trust of S2 and S3 become slightly higher and vice-versa. In Figure 6d as the interdependency value ? is 0, no mutual effect of the trust values on each other could be observed. In Figure 6f where ? = -1, representing cooperation, it can be seen that an increase in objective experience of S1 gives a positive boost to the subjective values of S2 and S3 and vice-versa. Furthermore, in the competitive case (Figure 6a, 6b) the maximum and minimum values of the trust are higher than for the neutral and cooperative cases. This is due to that fact that competition gives an additional increase to the positive and negative trust values towards extremes while cooperation brings them closer to each other (see Figure 6c, d, e, f).

 a)  b)
 c)  d)
 e)  f)

Figure . 6. Dynamics of subjective experience based on trust models from literature a) without temporal discounting and ?=1 b) with temporal discounting and ?=1, c) without temporal discounting and ?=0, d) with temporal discounting and ?=0 e) without temporal discounting and ?=-1, f) with temporal discounting and ?=-1.
5.4 	Experiment 2: Relative trust and Subjective experience based trust models
In this experiment the second experience sequence was used where trustee S1 gives positive (1) and neutral (0) and S2 gives negative (-1) and positive (1) experience values alternating periodically every 500 time steps, while S3 gives neutral (0) experiences for the whole experiment (see figure 3b). 

 a)  b)
 c)  d)
 e)  f)

Figure. 7. Dynamics of relative and subjective experience based trust model for experiment 2, a) relative trust model for ?=1, b) subjective experience based model for ?=1, c) relative trust model for ?=0, d) subjective experience based model for ?=0, e) relative trust model for ?=-1, f) subjective experience based model for ?=-1.

In Figure 7a (relative trust model) and 7b (subjective experience based trust model) it is shown that when trustee S2 gives negative experiences, and there is a competitive interdependency the trust values of S1 become slightly higher than the trust values of S1 in Figure 7c and 7d (where there is a neutral interdependency). In Figure 7c and 7d as the interdependency value ? is 0, no mutual effect of trust values on each other can be observed. In Figure 7e and 7f where ? = -1 representing cooperation among trust values of trustees, it can be seen again that an increase in trust of S2 due to positive experience gives a positive boost to S1 and S3 and vice-versa. Furthermore, in the competitive case (Figure 7a and 7b), the maximum and minimum values of trust attained for the trustees is higher than for the neutral and cooperative cases. This is due to that fact that competition gives an additional increase to the positive and negative trust values towards extremes while cooperation brings them closer to each other. Also here as S3 is giving neutral experience in Figure 7c and 7d, there is no change in trust value of S3 because initial value of the trust is neutral as well. But the effect of competition and cooperation on S3 can clearly be seen in Figure 7a, 7b and 7e, 7f.
6   Related Work
A variety of computational models have been proposed for trust. Falcone and Castelfranchi [3](Falcone and Castelfranchi, 2004) for example, present an approach which includes a cognitive attribution process to update the trust of an agent. This process has been incorporated into their trust model because they claim that the simple idea that positive experiences increase the trust is not always valid, and therefore a more complex process is needed. They do however not directly take interdependencies of trust levels into account. Although in this paper simpler trust models have been used, the model as proposed by Flacone and Castelfranchi is certainly compatible with the subjective experience model as presented in this paper. Jonker and Treur [7](Jonker and Treur, J., 1999) present a formal framework for the analysis and specification of models for trust evolution and trust update. Here, both quantitative and qualitative examples are shown of trust functions. In this paper, the quantitative example model has been used to exemplify that the subjective experience approach is compatible with an existing trust function. The qualitative example could in principle also be used in combination with the proposed function for subjective experiences, however a mapping would be needed from the qualitative to the quantitative values and vice versa. [9](Kluwer and Waaler, 2006) incorporates the notion of relativeness as an extension of the trust model presented by [5](Jones, 2002). In [9](Kluwer and Waaler, 2006) for determining trust, as a basis trust values determined by the model presented in [5](Jones, 2002) are taken and then these values are used in order to make statements about different trust values for different agents which does not incorporate the experiences with other agents that can perform similar tasks. In [4](Hoogendoorn, Jaffry and Treur, 2008) a computational model of trust which does represent interdependencies between trust levels is presented. In the current paper refined version of this model has been used as a comparison with the presented alternative that allows the reuse of existing trust models, thereby showing that similar patterns can be generated. For a more extensive overview of existing computation trust models, see [12] and [11](Ramchurn, Huynh and Jennings, 2004; Sabater and Sierra, 2005).
Besides formalised computational models for trust, also informal trust models have been proposed in other fields, for example, in the field of management sciences. An interesting example is the model proposed in [10](Mokhtar, Wajid and Wang, 2007) which specifically addresses trust in collaborative environments. However, the focus is not so much on how trust values are influenced due to interdependencies, but more on the requirements of trust values different agents have in each other in order to make cooperation successful. 
Next to trust models, the notion of reputation is also frequently modeled. Hereby, reputation can be defined as the opinion or view of someone about something (cf. Sabater and Sierra, 2005[12]) and according to [11](Ramchurn, Huynh and Jennings, 2004) can mainly be derived from the aggregation of opinions of members of a community about one of them. In this paper, the main focus has been on the sole experiences of one agent, and the community view has not been addressed. Nevertheless, it would be an interesting part of future work to investigate how trust interdependencies affect the overall reputation of agents in a society. For an overview of computational models for reputation, see [12], and [11](Ramchurn, Huynh and Jennings, 2004; Sabater and Sierra, 2005).
7   Conclusions and Future Work
In this paper, the notion of interdependent trust has been addressed. Hereby, two approaches have been taken: (1) to use a dedicated trust model which incorporates interdependent trust (which is a refinement of the model presented in (Hoogendoorn, Jaffry and Treur, 2008)[4]), and (2) to develop a model that transforms objective experiences into subjective experiences that can be fed into an existing trust model. In order to evaluate the two approaches, simulation runs have been performed to investigate to which extent comparable patterns come out of the approaches. In the simulations of the subjective experience variant three trust models have been used to generate such patterns (of which two were taken from literature). The patterns that appeared in accordance with the expectations.
For future work, a validation of the model based upon experiments with humans is planned. Furthermore, the integration of the subjective experiences with more complex models such as for instance the model of Falcone and Castelfranchi [3](Falcone and Castelfranchi, 2004) is also planned.
