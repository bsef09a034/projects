﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    static class PreprocessingHelper
    {
        public static int SizeofLexicon()
        {
            int size = 0;
            foreach (var val in Program.lexicon)
            {
                size = size + val.Value.totalCount;
            }

            return size;

        }

        
    }
}
