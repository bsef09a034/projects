﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    partial class Program
    {
        static File obj = new File();
        static List<string> data;
        static Dictionary<string, Node> lexicon=new Dictionary<string,Node> ();

        static bool containPage(List<InnerNode> l, string page, ref int ind)
        {
            bool retVal=false;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].pageName == page)
                {
                    retVal = true;
                    ind = i;
                }
            }

            return retVal;
        }
        static RetObj minus(List<InnerNode> l1, List<InnerNode> l2)
        {
            RetObj retObj = new RetObj();
            List<InnerNode> result = new List<InnerNode>();
            List<string> temp1 = new List<string>(), temp2 = new List<string>(), final = new List<string>();
            for (int i = 0; i < l1.Count; i++)
            {
                temp1.Add(l1[i].pageName);
            }
            for (int i = 0; i < l2.Count; i++)
            {
                temp2.Add(l2[i].pageName);
            }
            for (int i = 0; i < temp1.Count; i++)
            {
                if (!temp2.Contains(temp1[i])) { final.Add(temp1[i]); }
            }
            for (int i = 0; i < l1.Count; i++)
            {
                if (final.Contains(l1[i].pageName)) result.Add(l1[i]);
            }
            

            retObj.list.Add(result);
            return retObj;
        }
        static RetObj intersaction(List<InnerNode> l1, List<InnerNode> l2)
        {
            RetObj retObj = new RetObj();
            List<InnerNode> result1 = new List<InnerNode>(), result2 = new List<InnerNode>();
            List<string> temp1 = new List<string>(), temp2 = new List<string>(), final = new List<string>();
            for (int i = 0; i < l1.Count; i++)
            {
                temp1.Add(l1[i].pageName);
            }
            for (int i = 0; i < l2.Count; i++)
            {
                temp2.Add(l2[i].pageName);
            }
            for (int i = 0; i < temp1.Count; i++)
            {
                if (temp2.Contains(temp1[i])) { final.Add(temp1[i]); }
            }
            for (int i = 0; i < l1.Count; i++)
            {
                if (final.Contains(l1[i].pageName)) result1.Add(l1[i]);
            }
            for (int i = 0; i < l2.Count; i++)
            {
                if (final.Contains(l2[i].pageName)) result2.Add(l2[i]);
            }
            retObj.list.Add(result1);
            retObj.list.Add(result2);

            return retObj;
        }
        static RetObj processOr(RetObj a, RetObj b)
        {
            RetObj result = new RetObj(); 
            try
            {
                
                
                if (a.list.Count > 0||b.list.Count > 0)
                {
                    result.list.AddRange(a.list);
                    result.list.AddRange(b.list);
                   
                }
                else Console.WriteLine("\n No match!");

            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("\nKey has no match!");
            }
            return result;
        }

        static RetObj processAnd(RetObj a, RetObj b)
        {
            RetObj retObj = intersaction(a.list[0], b.list[0]);
            try
            {
                retObj = intersaction(a.list[0], b.list[0]);
                List<InnerNode> l=retObj.list[0];
                for (int i = 1; i < a.list.Count; i++)
                {
                    List<InnerNode> temp = new List<InnerNode>();
                    for (int j = 0; j < a.list[i].Count; j++)
                    {
                        int ind=-1;
                        InnerNode iN = new InnerNode();
                        if(containPage( l,a.list[i][j].pageName,ref ind))
                        {
                            iN.pageName = a.list[i][j].pageName;
                            iN.positions = a.list[i][j].positions;
                        }
                        temp.Add(iN);
                    }
                    retObj.list.Add(temp);
                }

            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("\nKey has no match!");
            }
            return retObj;

        }
        static RetObj processNot(RetObj a, RetObj b)
        {
            RetObj ret = null;
            try
            {
                ret= minus(a.list[0], b.list[0]);
               
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("\nKey has no match!");
            }
            return ret;

        }
        static void userEnd()
        {
            Console.WriteLine("Enter query here:");
            string s = Console.ReadLine();
            s = s.ToLower();
            string[] words = s.Split(' ');
            string temp = "";
            bool chk = true;
            for (int i = 0; i < words.Length; i++)
            {
                if (words[i] == "not") words[i] = "/";
                else if (words[i] == "and") words[i] = "*";
                else if (words[i] == "or") words[i] = "+";
                if (chk)
                {
                    temp =  words[i];
                    chk = false;
                }
                else temp = temp + " " + words[i];
            }

            Console.WriteLine(temp);
            Helper(temp);
 
            
        }
        static void populateLexicon(string page)
        {
            data = obj.read(page);



            for (int i = 0; i < data.Count; i++)
            {
                
                if (lexicon.ContainsKey(data[i].ToLower()))
                {
                    Node temp = lexicon[data[i].ToLower()];
                    temp.totalCount++;
                    int ind = -1;
                    if (containPage(temp.pageList, page, ref ind))
                    {
                        if (!temp.pageList[ind].positions.Contains(i))
                            temp.pageList[ind].positions.Add(i);
                    }
                    else
                    {
                        InnerNode t = new InnerNode();
                        t.pageName = page;
                        t.positions.Add(i);
                        temp.pageList.Add(t);
                    }
                    lexicon[data[i].ToLower()] = temp;
                }
                else
                {
                    Node temp = new Node();
                    temp.totalCount++;
                    InnerNode t = new InnerNode();
                    t.pageName = page;
                    t.positions.Add(i);
                    temp.pageList.Add(t);
                    lexicon.Add(data[i].ToLower(), temp);
                }

            }
        }
        static void Main(string[] args)
        {

            for (int i = 1; i <= Defs.filesCount; i++)
            {
                string page = "chapter " + i.ToString()+".txt";
                populateLexicon(page);
            }

            userEnd();
            Console.WriteLine("\n******  ");
        }
    }
}
