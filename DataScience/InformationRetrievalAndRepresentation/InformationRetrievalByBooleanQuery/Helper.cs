﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ruf
{
    class Program
    {
        //#include <iostream>
        //#include <string>
        //#include <stack>
        //#include <ctype>   // to use the tolower function

        //using namespace std;


        //void Convert(ref string  Infix,ref string  Postfix);

        //bool IsOperand(char ch);

        //bool TakesPrecedence(char OperatorA, char OperatorB);



        /* Given:  ch   A character.
           Task:   To determine whether ch represents an operand (here understood
                   to be a single letter or digit).
           Return: In the function name: true, if ch is an operand, false otherwise.
        */
        static bool IsOperand(string  ch)
           {
               //if (((ch >= "a") && (ch <= 'z')) ||
               //   ((ch >= 'A') && (ch <= 'Z')) ||
               //   ((ch >= '0') && (ch <= '9')))
               if (ch != "AND" && ch != "OR" && ch != "NOT" && ch != "+" && ch != "*" && ch != "-" &ch!="/")
              return true;
           else
              return false;
           }


        /* Given:  OperatorA    A character representing an operator or parenthesis.
                   OperatorB    A character representing an operator or parenthesis.
           Task:   To determine whether OperatorA takes precedence over OperatorB.
           Return: In the function name: true, if OperatorA takes precedence over
                   OperatorB.
        */
        static bool TakesPrecedence(string OperatorA, string OperatorB)
           {
               //if (OperatorA == "OR") return false;
               //if (OperatorB == "NOT") return false;

           if (OperatorA == "(")
              return false;
           else if (OperatorB == "(")
              return false;
           else if (OperatorB == ")")
              return true;
           else if ((OperatorA == "^") && (OperatorB == "^"))
              return false;
           else if (OperatorA == "^")
              return true;
           else if (OperatorB == "^")
              return false;
           else if ((OperatorA == "*") || (OperatorA == "/"))
              return true;
           else if ((OperatorB == "*") || (OperatorB == "/"))
              return false;
           else
              return true;
      
           }


        /* Given:  Infix    A string representing an infix exp<b></b>ression (no spaces).
           Task:   To find the postfix equivalent of this exp<b></b>ression.
           Return: Postfix  A string holding this postfix equivalent.
        */
        static void Convert(ref string  Infi, ref string Postfix)
           {
           Stack<string> OperatorStack=new Stack<string> ();
           string[] Infix = Infi.Split(' ');
           string TopSymbol, Symbol;
           int k;

           for (k = 0; k < Infix.Length; k++)
              {
              Symbol = Infix[k];
              if (IsOperand(Symbol))
                  Postfix = Postfix + " " + Symbol;
              else
                 {
                 while ((! (OperatorStack.Count==0)) &&
                    (TakesPrecedence(OperatorStack.Peek(), Symbol)))
                    {
                    TopSymbol = OperatorStack.Peek();
                    OperatorStack.Pop();
                    Postfix = Postfix + " " + TopSymbol;
                    }
                 if ((! (OperatorStack.Count==0)) && (Symbol == ")"))
                    OperatorStack.Peek();   // discard matching (
                 else
                    OperatorStack.Push(Symbol);
                 }
              }

           while (! (OperatorStack.Count==0))
              {
              TopSymbol = OperatorStack.Peek();
              OperatorStack.Pop();
              Postfix = Postfix + " "+ TopSymbol;
              }
           }

        static void Main(string[] args)
        {
           // char Reply;

            //do
            {
                string Infix, Postfic="";   // local to this loop

                Console.WriteLine( "Enter an infix exp<b></b>ression (e.g. (a+B)/c^2, with no spaces):");

                Infix = Console.ReadLine(); ;

                Convert(ref Infix,ref Postfic);
                string[] Postfix = Postfic.Split(' ');
                Console.WriteLine("The equivalent postfix exp<b></b>ression is:\n" );
                for (int i = 0; i < Postfix.Length; i++)
                {
                    Console.Write(Postfix[i]);
                }
                //cout << endl << "Do another (y/n)? ";
                //cin >> Reply;

                Stack<string> S=new Stack<string> ();
	            string[] a=(string[])Postfix.Clone();
                //Console.WriteLine("*** "+a[1]);
                 
	            for(int i=1; i<a.Length; i++)
	            {
                    //Console.WriteLine(Postfix.Length);
                    double d1, d2, d;
                    if ((double.TryParse(a[i].ToString(), out d)))
                    {
                        S.Push(a[i]);
                        //Console.WriteLine("push");
                    }
                    else if (a[i] == "+")
                    {
                        Console.WriteLine("add");
                        //Console.WriteLine("pop");
                        d1 = double.Parse(S.Pop());

                        d2 = double.Parse(S.Pop());

                        S.Push((d1 + d2).ToString());
                        //Console.WriteLine("push");
                    }
                    else if (a[i] == "-")
                    {
                        Console.WriteLine("sub");
                        S.Push((-(double.Parse(S.Pop()) - double.Parse(S.Pop()))).ToString());
                    }
                    else if (a[i] == "*")
                    {
                        Console.WriteLine("mul");
                        S.Push((double.Parse(S.Pop()) * double.Parse(S.Pop())).ToString());
                    }
                    else if (a[i] == "/")
                    {
                        d1 = double.Parse(S.Pop());

                        d2 = double.Parse(S.Pop());

                        S.Push((d2 / d1).ToString());
                        Console.WriteLine("div");
                    }
                    else Console.WriteLine("res=" + S.Pop());


	            }
                Console.WriteLine("res=" + S.Pop());
                //double dd = S.Pop();
                //Console.WriteLine("res=" + dd);*/
            }
            //while (tolower(Reply) == 'y');

            //char a[]=”6523+8*+3+*”;
	        
            //DisposeStack(S);
          
        }
    }
}
