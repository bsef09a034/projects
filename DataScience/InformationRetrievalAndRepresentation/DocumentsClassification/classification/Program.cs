﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace classification
{
    class Program
    {
        static fileRead obj = new fileRead();
        static List<DataBO> list;
        static int p = 0, q = 0;


        static List<Dictionary<string, ProbBO>> probList = new List<Dictionary<string, ProbBO>>();
        static List<string> lexicon = new List<string>();
        static void test(List<DataBO> list)
        {
            
            double probP = (double)p / (double)(p + q);
            double probQ = (double)q / (double)(p + q);
            Console.WriteLine("ProbValid= "+probP+"ProbInvalid= "+probQ);
            Console.WriteLine("Actual \t Predicted \t\t probSPAM \t ProbVALID");
            foreach(var val in list)
            {
                //All VALID joint probabilities
               
                double probVALID, probSPAM;
                try
                {
                    double sum = 0;
                    for (int j = 0; j <probList.Count; j++)
                    {
                        try
                        {
                            sum = sum + Math.Log10((double)probList[j][val.list[j]].VALID);
                        }
                        catch (KeyNotFoundException)
                        {
                            sum = sum + 0;
                        }
                        
                    }
                    sum = sum +Math.Log10( probP);
                    probVALID = sum;
                }
                catch (KeyNotFoundException)
                {
                    Console.WriteLine("exVALID");
                    probVALID = 0;
                }
                try
                {
                    double sum = 0;
                    for (int j = 0; j < probList.Count; j++)
                    {
                        try
                        {
                            sum = sum + Math.Log10((double)probList[j][val.list[j]].SPAM);
                        }
                        catch (KeyNotFoundException)
                        {
                            sum = sum + 0;
                        }
                    }
                    sum = sum + Math.Log10(probQ);
                    probSPAM = sum;

                }
                catch (KeyNotFoundException)
                {
                    Console.WriteLine("exSpam");
                    probSPAM = 0;
                }
                string result="";
                if (probVALID > probSPAM) result = "It is VALID";
                else if(probVALID<probSPAM)result = "It is SPAM  ";
                else result = "No decision\t";

                Console.WriteLine(val.clas + "\t" + result + "\t\t" + probSPAM + "\t" +probVALID);
            }
            
        }
        static void train(List<DataBO> list)
        {
            foreach (var val in list)
            {
                if (val.clas == "VALID") p++;
                else if (val.clas == "SPAM") q++;
            }
            for(int i=0;i<Defs.lexSize;i++)
            {
                Dictionary<string, ProbBO> d=new Dictionary<string,ProbBO> ();
                probList.Add(d);
            }

            foreach (var val in list)
            {
                for (int j = 0; j < probList.Count; j++)
                {
                    if (probList[j].ContainsKey(val.list[j])) 
                    {
                        if (val.clas == "VALID")
                        {
                            probList[j][val.list[j]].VALID = probList[j][val.list[j]].VALID + (1.0 / (double)p);
                            probList[j][val.list[j]].total = probList[j][val.list[j]].total + ((double)1 / (double)(p + q));
                        }
                        else if (val.clas == "SPAM")
                        {
                            probList[j][val.list[j]].SPAM = probList[j][val.list[j]].SPAM + ((double)1 / (double)q);
                            probList[j][val.list[j]].total = probList[j][val.list[j]].total + ((double)1 / (double)(p + q));
                        }
                    }
                    else
                    {
                        ProbBO tempObj = new ProbBO();
                        if (val.clas == "VALID")
                        {
                            tempObj.VALID = (double)1 / (double)p;
                            tempObj.total = ((double)1 / (double)(p + q));
                        }
                        else if (val.clas == "SPAM")
                        {
                            tempObj.SPAM = (double)1 / (double)q;
                            tempObj.total = ((double)1 / (double)(p + q));
                        }
                        probList[j].Add(val.list[j], tempObj);

                    }
                }
               
            }
        }
        static void createInputFileHelper(string fname,string clas)
        {
            List<string> tempLex = obj.readLex(fname);
            string outputStrr = "";

            foreach (var val in lexicon)
            {
                if (tempLex.Contains(val))
                {
                    outputStrr = outputStrr + "T\t";
                }
                else
                {
                    outputStrr = outputStrr + "F\t";
                }
            }
            obj.appedToFile("input.txt", outputStrr+clas);

        }
        static void createInputFile()
        {
            //for training data

            // For SPAMS
            string s="SPAM",temp="";
            for (int i = 1; i <= Defs.trainSpamFilesCount; i++)
            {
                temp=s+i+".txt";
                createInputFileHelper(temp, "SPAM");
            }

            //For VALIDS

            s = "VALID"; temp = "";
            for (int i = 1; i <= Defs.trainValidFilesCount; i++)
            {
                temp=s+i+".txt";
                createInputFileHelper(temp, "VALID");
            }


            //for testing data
            s = "SPAM"; temp = "";
            for (int i = Defs.trainSpamFilesCount+1; i < Defs.trainSpamFilesCount+Defs.testSpamFilesCount; i++)
            {
                temp = s + i + ".txt";
                createInputFileHelper(temp, "SPAM");
            }

            //For VALIDS

            s = "VALID"; temp = "";
            for (int i = Defs.trainValidFilesCount+1; i < Defs.trainValidFilesCount+Defs.testValidFilesCount; i++)
            {
                temp = s + i + ".txt";
                createInputFileHelper(temp, "VALID");
            }



        }
       
        static void Main(string[] args)
        {
            lexicon= obj.readLex("lex.txt");
            Defs.lexSize = lexicon.Count;

            createInputFile();

            obj.read();
            list = obj.train();
            train(list);
            list = obj.test();
            test(list);
        }
    }
}
