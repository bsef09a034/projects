﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace classification
{
    class fileRead
    {
        public StreamReader sr = null;
        public List<DataBO> list=new List<DataBO>();

        public List<string> readLex(string fname)
        {
            List<string> l = new List<string>();
            try
            {
                
                sr = new StreamReader(fname);

                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split(' ', '\n', '\t', '+', '/', '=', '^', '*');



                    for (int i = 0; i < words.Length; i++)
                    {
                        string tempW = words[i].Trim();
                        if (tempW.Length > 0)
                        {
                            if (tempW.Last() == '.')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf('.'));
                            }
                            else if (tempW.Last() == ',')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf(','));
                            }
                            else if (tempW.Last() == ';')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf(';'));
                            }
                            else if (tempW.Last() == ':')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf(':'));
                            }

                            if (tempW.Contains(')')) tempW = tempW.Replace(")", "");
                            if (tempW.Contains('(')) tempW = tempW.Replace("(", "");
                            if (tempW.Contains('-')) tempW = tempW.Replace("-", "");
                            if (tempW.Contains('?')) tempW = tempW.Replace("?", "");
                            if (tempW.Contains('[')) tempW = tempW.Replace("[", "");
                            if (tempW.Contains(']')) tempW = tempW.Replace("]", "");
                            if (tempW.Contains('_')) tempW = tempW.Replace("_", "");
                            if (tempW.Contains('\'')) tempW = tempW.Replace("\'", "");
                            if (tempW.Contains('`')) tempW = tempW.Replace("`", "");
                            try
                            {


                                if (tempW.All(char.IsLetter))
                                {
                                    if(!l.Contains(tempW))
                                        l.Add(tempW);
                                }
                                else
                                {
                                    Convert.ToDouble(tempW);
                                    if (!l.Contains(tempW))
                                        l.Add(tempW);
                                }

                            }
                            catch
                            {
                                
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();

            return l;


        }
        public void read()
        {
            try
            {
                
                sr = new StreamReader("input.txt");
                while (!sr.EndOfStream)
                {
                    string s=sr.ReadLine();
                    string []words=s.Split('\t');
                    DataBO temp = new DataBO();
                    int i=0;
                    for ( i= 0; i < words.Length - 1;i++ )
                    {
                        temp.list.Add(words[i]);
                    }
                    temp.clas = words[i];
                    

                    list.Add(temp);
                   
                }

                
            }
            catch 
            {
                Console.WriteLine("Error!");
            }
            sr.Close();


            
        }
        public void appedToFile(string fname, string s)
        {
            StreamWriter sw =null;
            try
            {
                sw = new StreamWriter(fname, true);
                sw.WriteLine(s);
            }
            catch { }
            sw.Close();
            

        }
        public List<DataBO> train()
        {
            return list.GetRange(0, 10);
        }
        public List<DataBO> test()
        {
            return list.GetRange(9,9);
        }


    }
}
