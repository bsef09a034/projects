﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pro
{
    static class Logic
    {
        public static List<string> membership(List<Set> set1, string start1, List<string> finals1, List<string> testStrings, List<string> v)
        {
            int size = set1.First().size;

            List<string> retStr=new List<string> ();
            string start;
            int i;
            for (i = 0; i < testStrings.Count; i++)
            {
                start = start1;
                bool check = true;
                string ret = "";
                for (int j = 0; j < testStrings[i].Length && check; j++)
                {
                    int s = Helper.getIndex(set1, start);
                    string c="";
                    if (testStrings[i][j] != '+')
                    {
                        c = testStrings[i][j].ToString();
                    }
                    else
                    {
                        
                        int ind = testStrings[i].IndexOf('+', j+1);
                        int len = ind - j ;
                        try
                        {
                            c = testStrings[i].Substring(j , len);
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            len = testStrings[i].Length - j ;
                            c = testStrings[i].Substring(j , len);
                        }
                        j = j + len-1;
                    }
                    string tmp;
                    tmp = c;
                   
                    int letterIndex =Helper. getIndexOfLang(tmp, v);

                    if (letterIndex >= 0)
                    {
                        
                        start = set1[s].arr[letterIndex + 1];
                        ret = ret + set1[s].output[letterIndex + 1];

                    }

                    else
                    {
                        letterIndex = Helper.getIndexOfLang("*", v);
                        if (letterIndex >= 0)
                        {
                            
                            start = set1[s].arr[letterIndex + 1];
                            ret = ret + testStrings[i][j];
                            
                        }
                        else
                        {
                            check = false;
                        }
                    }
                    
                }
                if (Helper.searchString(finals1, start) && check)
                {
                    retStr.Add(ret);
                }
                else
                {
                    retStr.Add("I"); 
                }

            }

            return retStr;
        }

        public static List<string> membership2(List<Set> set1, string start1, List<string> finals1, List<string> testStrings, List<string> v)
        {
            int s = 0, j = 0; ;
            int size = set1.First().size;
            int letterIndex=0;
            List<string> retStr = new List<string>();
            string start,retTmp="";
            int i,jj=0,sTmp=0,letI=0;
            bool chk = false;
            Stack<string> Stk = new Stack<string>();
            for (i = 0; i < testStrings.Count; i++)
            {

                
                start = start1;
                bool check = true;
                string ret = "";
                

                for (j = 0; j < testStrings[i].Length && check; j++)
                {
                    if (chk == false)
                    {
                            s = Helper.getIndex(set1, start);

                        string c = "";
                        c = testStrings[i][j].ToString();

                        string tmp;
                        tmp = c;
                        letterIndex = Helper.getIndexOfLang(tmp, v);
                        int nullInd = Helper.getIndexOfLang("~", v);
                        if (letterIndex >= 0)
                        {
                            start = set1[s].arr[letterIndex + 1];
                            ret = ret + set1[s].output[letterIndex + 1];
                            
                            if (set1[Helper.getIndex(set1,start)].arr[nullInd + 1] != "D")
                            {
                                Stk.Push(Helper.getIndex(set1, start).ToString());
                                Stk.Push(nullInd.ToString());
                                Stk.Push(j.ToString());
                                
                                Stk.Push(ret);
                            }

                        }

                        else
                        {
                            letterIndex = Helper.getIndexOfLang("*", v);
                            if (letterIndex >= 0)
                            {
                                
                                start = set1[s].arr[letterIndex + 1];
                                ret = ret + testStrings[i][j];

                            }
                            else
                            {
                                check = false;
                            }
                        }
                    }
                    else
                    {
                        j=jj;
                        s = sTmp;
                        ret = retTmp;
                        letterIndex = letI;
                        start = set1[s].arr[letterIndex + 1];
                        
                        ret = ret + set1[s].output[letterIndex + 1];
                        chk = false;
                    }
                }
               
                if (Helper.searchString(finals1, start) && check)
                {
                    retStr.Add(ret);
                }
                else
                {
                    
                    try
                    {
                        
                        retTmp = Stk.Pop();
                        jj = Convert.ToInt32(Stk.Pop());
                        letI = Convert.ToInt32(Stk.Pop());
                        sTmp = Convert.ToInt32(Stk.Pop());
                        chk = true;
                        i--;


                    }
                    catch
                    {
                        
                        retStr.Add("I");
                    }

                }
                
                    
                }
          
            return retStr;
        }
    }
}
