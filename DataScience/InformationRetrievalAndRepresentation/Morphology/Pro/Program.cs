﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pro
{
    class Program
    {
        static void Main(string[] args)
        {
        
            try
            {
                List<Set> set1;
                List<string> finals1 = new List<string>(), testStrings;
                string start1 = "";
                List<string> arr;
                List<string> v = new List<string>();
                set1 = File.readFile("in1.txt", ref finals1, ref start1, ref v);


                testStrings = File.readStrings("InputStrings.txt");


                arr = Logic.membership(set1, start1, finals1, testStrings, v);
                

                File.writeResult("Intermediary.txt", arr);

                /* ******************************************** */

                set1.Clear();
                finals1.Clear();
                testStrings.Clear();
                start1 = "";
                arr.Clear();
                v.Clear();

                set1 = File.readFile("in2.txt", ref finals1, ref start1, ref v);


                testStrings = File.readStrings("Intermediary.txt");


                arr = Logic.membership2(set1, start1, finals1, testStrings, v);


                File.writeResult("Results.txt", arr);

                Console.WriteLine("See files:");
                Console.WriteLine("in1: Rules for generating intermediate state.");
                Console.WriteLine("InputStrings: Input strings to test.");
                Console.WriteLine("Intermediary: Intermediary strings of input strings.");
                Console.WriteLine("in2: Rules for generating results from intermediate state.");
                Console.WriteLine("Results: Results of test strings.");


            }
            catch (Exception e)
            {
                Console.WriteLine("Exception\n" + e.ToString());
            }
                }
            }
}
