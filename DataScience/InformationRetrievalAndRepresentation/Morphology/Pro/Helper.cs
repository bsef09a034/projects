﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pro
{
    static class Helper
    {
        public static string toString(Set s, bool start = false, bool final = false)
        {
            string retval = "";
            if (start == true) retval = "-" + s.arr[0] + s.arr[1];
            else if (final == true) retval = "+" + s.arr[0] + s.arr[1];
            else retval = s.arr[0] + s.arr[1];
            return retval;
        }
        public static bool searchString(List<string> v, string s)
        {
            bool retval = false;
            for (int i = 0; i < v.Count; i++)
            {
                if (v[i] == s)
                {
                    retval = true;
                    break;
                }
            }
            return retval;
        }
        public static int getIndex(List<Set> v, string s)
        {
            int retval = -1;
            for (int i = 0; i < v.Count; i++)
            {
                if (v[i].arr[0] == s)
                {
                    retval = i;
                    break;
                }
            }
            return retval;
        }
        public static string cutSign(string c)
        {
            string d = "";
            for(int i=1;i<c.Length;i++)
            {
                d = d + c[i];

            } 
            return d;
        }
        public static bool searchSet(List<Set> v, Set s)
        {
            bool retVal = false;
            for (int i = 0; i < v.Count; i++)
            {
                if (v[i] == s) retVal = true;
            }

            return retVal;
        }
        public static int sizeOf(string s)
        {
            int i = 0;
            while (s[i] != '\0')
            {
                i++;
            }
            return i;
        }
        public static int getIndexOfLang(string s, List<string> v)
        {
            int ind = -1;
            bool check = true;
            for (int i = 0; i < v.Count && check; i++)
            {
                if (v[i] == s)
                {
                    ind = i;
                    check = false;
                }

            }
            return ind;
        }

    }
}
