﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace Pro
{
    static class File
    {
        public static List<Set> readFile(string fname,ref List<string> finals,ref string start,ref List<string> v)
        {
	
	        string temp;
	        StreamReader iff=new StreamReader (fname);
	        List<Set> set=new List<Set> ();
	        try
	        {
             
                    temp=iff.ReadLine();
                    string []words=temp.Split(' ');
	                foreach(var val in words)
                    {
                        if(val!=";")
                        v.Add(val);
                    }

                    iff.ReadLine();
                    int size=v.Count+1;
                    do
                    {
                        Set s = new Set(size);
                        string t=iff.ReadLine();
                        words = t.Split(' ');
                        int i = 0;
                        foreach(var val in words)
                        {
                            
                                temp=val;
                                if( (temp!=";" ))
                                {
                                    if(temp[0]=='+')
                                    {
                                         temp=Helper. cutSign(temp);
                                         finals.Add(temp);           
                                    }
                                    else if(temp[0]=='-')
                                    {
                                         temp = Helper.cutSign(temp);
                                 
                                         if(temp[0]=='+')
                                         {
                                             temp = Helper.cutSign(temp);
                                             finals.Add(temp);           
                                         }
                                         start=temp;
                                    }
                                    string []w = temp.Split(':');
                                    s.arr[i]=w[0];
                                    try
                                    {
                                        s.output[i] = w[1];
                                    }
                                    catch { }
                                    i++;
                                    if (i >= size) break;
                                }
                        
                        }

                           set.Add(s);
                    }while(!iff.EndOfStream);
            
                    iff.Close();
                    
            
            
            }
            catch(FileNotFoundException)
	        {
		       
		        Console.WriteLine( "File could be not found.");
	        }
	        return set;
        }
        public static void writeFile(string fname,List<Set> set)
        {
	
	        StreamWriter off=new StreamWriter (fname);

	        try
	        {
             
	         
                    int size=set.First().size;
                    for(int i=0;i<set.Count();i++)
	                {
                            string s="";
                            for(int j=0;j<size;j++)
                            {
                                    s=set[i].arr[j]+" ";
                            }
                            s=s+";";
                            off.WriteLine(s);
                    }
            
           

            }
            catch(Exception e)
	        {
		        Console.WriteLine( "File could be created."+e.ToString());
	        }
            off.Close();
        }
        public static  List<string> readStrings(string fname)
        {
            List<string> retVec=new List<string> ();
            StreamReader iff=new StreamReader (fname);
	
	        try
	        {
             
                   string temp;
                   while(!iff.EndOfStream)
                   {
                       temp = iff.ReadLine();
                       temp = temp.Trim();
                       retVec.Add(temp);
                   }
            
                    iff.Close();
            
                                
            }
            catch(Exception e)
	        {
		        Console.WriteLine( "File could be not found."+e.ToString());
	        }
	        return retVec;
               
        }
        public static void writeResult(string fname,List<string> results)
        {
	
	        StreamWriter off=new StreamWriter (fname);

	        try
	        {
                    int size=results.Count();
                    for(int i=0;i<size;i++)
	                {
                        off.WriteLine(results[i]);
                    }
            
             }
             
            catch(Exception e)
	        {
		        Console.WriteLine( "File could be created."+e.ToString());
	        }
            off.Close();
        }

    }
}
