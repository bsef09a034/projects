﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ARM
{
    class File
    {
        public StreamReader sr = null;
        public List<string> list = new List<string>();
        
        public List<string> read(string fname)
        {
            try
            {
                list=new List<string> ();
                sr = new StreamReader(fname);
                


               
                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split(' ','\n',',','.','\t');
                   
                    for (int i = 0; i < words.Length; i++)
                        list.Add(words[i]);
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();

            return list;

        }

        public List<string> read2(string fname)
        {
            try
            {
                list = new List<string>();
                sr = new StreamReader(fname);

                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split(' ', '\n', '\t', '+', '/', '=', '^', '*');



                    for (int i = 0; i < words.Length; i++)
                    {
                        string tempW = words[i].Trim();
                        if (tempW.Length > 0)
                        {
                            if (tempW.Last() == '.')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf('.'));
                            }
                            else if (tempW.Last() == ',')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf(','));
                            }
                            else if (tempW.Last() == ';')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf(';'));
                            }
                            else if (tempW.Last() == ':')
                            {
                                tempW = tempW.Remove(tempW.LastIndexOf(':'));
                            }

                            if (tempW.Contains(')')) tempW = tempW.Replace(")", "");
                            if (tempW.Contains('(')) tempW = tempW.Replace("(", "");
                            if (tempW.Contains('-')) tempW = tempW.Replace("-", "");
                            if (tempW.Contains('?')) tempW = tempW.Replace("?", "");
                            if (tempW.Contains('[')) tempW = tempW.Replace("[", "");
                            if (tempW.Contains(']')) tempW = tempW.Replace("]", "");
                            if (tempW.Contains('_')) tempW = tempW.Replace("_", "");
                            if (tempW.Contains('\'')) tempW = tempW.Replace("\'", "");
                            if (tempW.Contains('`')) tempW = tempW.Replace("`", "");
                            try
                            {


                                if (tempW.All(char.IsLetter))
                                    list.Add(tempW);
                                else
                                {
                                    Convert.ToDouble(tempW);
                                    list.Add(tempW);
                                }

                            }
                            catch
                            {
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();

            return list;


        }
        public List<SentenceNode> readSentences(string fname)
        {
            List<SentenceNode> l = new List<SentenceNode>();
            try
            {
                int seqNo = 0;
                
                sr = new StreamReader(fname);

                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    s = s + " ";
                    string[] arr = { ". "};
                    string[] words = s.Split(arr, StringSplitOptions.RemoveEmptyEntries);

                    
                    for (int i = 0; i < words.Length; i++)
                    {
                        string tempW = words[i].Trim();
                        if (tempW.Length > 0 && !tempW.All(char.IsWhiteSpace))
                        {
                            SentenceNode sn = new SentenceNode(seqNo, tempW);
                            l.Add(sn);
                            seqNo++;
                        }
                    }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();

            return l;


        }
        public List<string> readCandidateKW(string fname)
        {
            try
            {
                list = new List<string>();
                sr = new StreamReader(fname);

                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();

                    string[] words = PreprocessingHelper.stopWordSplit(s);
                    
                        for (int i = 0; i < words.Length; i++)
                        {
                            string tempW = words[i].Trim();
                            if (tempW.Length > 0&& !tempW.All(char.IsWhiteSpace))
                            {
                                
                                list.Add(tempW);
                            }
                        }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();

            return list;


        }
        public static void WriteResults(string fname, string query, List<string> resultingRankedFiles,int size=-1)
        {
            StreamWriter sw = new StreamWriter(fname,true);
            if (size != -1)
            {
                sw.WriteLine("Size of lexicon is: " + size);
                sw.WriteLine();
            }

            sw.WriteLine("query is: "+query);
            sw.WriteLine("Result is: ");
            foreach (var v in resultingRankedFiles)
                sw.WriteLine(v);

            sw.WriteLine();

            sw.Close();

        }
        public static void WriteList(string fname, List<string> list)
        {
            StreamWriter sw = new StreamWriter(fname);
            
            foreach (var v in list)
                sw.WriteLine(v);

            sw.Close();

        }
        public List<string> readStringList(string fname)
        {
            try
            {
                list=new List<string> ();
                sr = new StreamReader(fname);
                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    list.Add(s);
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();

            return list;

        }
        
    }
}
