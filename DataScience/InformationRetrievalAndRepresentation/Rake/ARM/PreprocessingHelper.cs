﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    static class PreprocessingHelper
    {
        public static int SizeofLexicon()
        {
            int size = 0;
            foreach (var val in Program.lexicon)
            {
                size = size + val.Value.totalCount;
            }

            return size;

        }
        public static string[] stopWordSplit(string s)
        {
            string[] retArr;
            string[] words = s.Split(' ', '\n', '\t', '+', '/', '=', '^', '*'/*,',','.',';',':'*/);
            string temp = "";
            for (int i = 0; i < words.Length; i++)
            {
                if (words[i].Contains(')')) words[i] = words[i].Replace(")", "");
                if (words[i].Contains('(')) words[i] = words[i].Replace("(", "");
                if (words[i].Contains('-')) words[i] = words[i].Replace("-", "");
                if (words[i].Contains('?')) words[i] = words[i].Replace("?", "");
                if (words[i].Contains('[')) words[i] = words[i].Replace("[", "");
                if (words[i].Contains(']')) words[i] = words[i].Replace("]", "");
                if (words[i].Contains('_')) words[i] = words[i].Replace("_", "");
                if (words[i].Contains('\'')) words[i] = words[i].Replace("\'", "");
                if (words[i].Contains('`')) words[i] = words[i].Replace("`", "");
                
                temp = temp + "  " + words[i];
            }
            temp = temp + " ";

            words=new string[Program.stopWords.Count+4];
            int ii=0;
            for(ii=0;ii<Program.stopWords.Count;ii++)
            {
                words[ii]=" "+Program.stopWords[ii]+" ";
            }

            words[ii++] = ". ";
            words[ii++] =  ", ";
            words[ii++] = "; ";
            words[ii++] = ": " ;

            retArr = temp.Split(words, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < retArr.Length; i++)
            {
                retArr[i] = retArr[i].Replace("  ", " ");
                
                bool chk = false;
               
                string[] w=retArr[i].Split(' ');
                string t="";
                for(int j=0;j<w.Length;j++)
                {
                    chk=false;
                    if(w[j].All(char.IsLetter))
                    {
                        chk=true;
                    }
                    else
                    {
                        try
                        {
                        Convert.ToDouble(w[j]);
                        chk = true;
                        }
                        catch { chk = false; }
                    }
                    if (chk == true)
                        t = t + w[j] + " ";
                }
                retArr[i] = t;

            }
            return retArr;

        }
        static int count(string s, char c)
        {
            int retcount = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == c) retcount++;
            }
            return retcount;
        }
        
        public static double freqHelper(string a, string b)
        {
            double count = 0;
            if (a == b)
            {
                count = Program.Keywords[a];
            }
            else
            {
                foreach (var val in Program.candidateKeywords)
                {
                    string[] words = val.Key.Split(' ');
                    if (words.Contains(a) && words.Contains(b))
                    {
                        count = count + val.Value;
                    }
                }

            }
            return count;
        }
        
    }
}
