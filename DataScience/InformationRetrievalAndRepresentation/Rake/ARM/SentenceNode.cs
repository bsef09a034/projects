﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    class SentenceNode:IComparable
    {
        public int seqNo = 0;
        public double weight = 0;
        public string sentence = "";
        public SentenceNode() { }
        public SentenceNode(int seq, string s)
        {
            seqNo = seq;
            weight = 0;
            sentence = s;
        }
        public int CompareTo(object obj)

        {

            if (obj is SentenceNode)

            {

                SentenceNode p2 = (SentenceNode)obj;

                return weight .CompareTo(p2.weight);

            }

            else

                throw new ArgumentException("Object is not a SentenceNode.");

        }
    }
}
