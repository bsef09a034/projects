﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    class FreqMatNode
    {
        public int ind = -1;
        public double sum = 0;
        public double [] arr;
        public FreqMatNode() { }
        public FreqMatNode(int i, int size)
        {
            ind = i;
            arr = new double[size];
        }
    }
}
