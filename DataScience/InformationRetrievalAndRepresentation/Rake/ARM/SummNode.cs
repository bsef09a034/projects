﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    class SummNode
    {
        public double weight = 0;
        public int seqNo = 0;
        public SummNode(int seq = 0, double w = 0)
        {
            seqNo = seq;
            weight = w;
        }

    }
}
