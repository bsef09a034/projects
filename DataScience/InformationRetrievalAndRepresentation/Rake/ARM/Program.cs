﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    partial class Program
    {
        
        public static File obj = new File();
        public static List<string> data;
        public static Dictionary<string, Node> lexicon=new Dictionary<string,Node> ();
        static List<string> fileNames = new List<string>();
        public static List<string> stopWords = new List<string>();
        public static Dictionary<string, double> candidateKeywords = new Dictionary<string, double>();
        public static Dictionary<string, double> Keywords = new Dictionary<string, double>();
        public static Dictionary<string, FreqMatNode > freqMatrix=new  Dictionary<string,FreqMatNode> ();

        static bool containPage(List<InnerNode> l, string page, ref int ind)
        {
            bool retVal=false;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].pageName == page)
                {
                    retVal = true;
                    ind = i;
                }
            }

            return retVal;
        }
        static void populateLexicon(string page)
        {
            
            data = obj.read2(page);


            for (int i = 0; i < data.Count; i++)
            {
                if (lexicon.ContainsKey(data[i].ToLower()))
                {
                    Node temp = lexicon[data[i].ToLower()];
                    temp.totalCount++;
                    int ind = -1;
                    if (containPage(temp.pageList, page, ref ind))
                    {
                        if (!temp.pageList[ind].positions.Contains(i))
                            temp.pageList[ind].positions.Add(i);
                    }
                    else
                    {
                        InnerNode t = new InnerNode();
                        t.pageName = page;
                        t.positions.Add(i);
                        temp.pageList.Add(t);
                    }
                    lexicon[data[i].ToLower()] = temp;
                }
                else
                {
                    Node temp = new Node();
                    temp.totalCount++;
                    InnerNode t = new InnerNode();
                    t.pageName = page;
                    t.positions.Add(i);
                    temp.pageList.Add(t);
                    lexicon.Add(data[i].ToLower(), temp);
                }

            }
        }
        static void populateCandidateKW(string page)
        {
            data = obj.readCandidateKW(page);
            for (int i = 0; i < data.Count; i++)
            {
                if (candidateKeywords.ContainsKey(data[i].ToLower()))
                {
                    candidateKeywords[data[i].ToLower()]++;
                }
                else
                {
                    candidateKeywords.Add(data[i].ToLower(), 1);
                }
                string []words=data[i].ToLower().Split(' ');
                foreach (var v in words)
                {
                    if (Keywords.ContainsKey(v))
                    {
                        Keywords[v]++;
                    }
                    else
                    {
                        Keywords.Add(v, 1);
                    }
                }

            }
        }
     
        static void textSummerization()
        {
            Dictionary<string, SummNode> summery = new Dictionary<string, SummNode>();
            List<SentenceNode> l = obj.readSentences("chapter 1.txt");
            for (int i = 0; i < l.Count; i++)
            {
                double w = 0;
                string a = " " + l[i].sentence + " ";
                foreach (var val in candidateKeywords)
                {
                    string b = " " + val.Key + " ";
                    if (a.Contains(b)) 
                    {
                        w = w + val.Value;
                    }
                }
                l[i].weight = w;

                if (!summery.ContainsKey(l[i].sentence))
                    summery.Add(l[i].sentence,new SummNode(i, l[i].weight));
                else
                    summery[l[i].sentence].weight = summery[l[i].sentence].weight + l[i].weight;
            }

            
            // computing 20% summery

            int n =(int)( (20.0 / 100.0) * (double)summery.Count)+1;
           

            var temp = from element in summery
                       orderby element.Value.weight descending
                       select element;

            var t=temp.ToDictionary<KeyValuePair<string, SummNode>, string,SummNode>(pair => pair.Key, pair => pair.Value);
            summery=new Dictionary<string,SummNode> ();

            
            for (int i = 0; i < n; i++)
            {
                summery.Add(t.ElementAt(i).Key, t.ElementAt(i).Value);
            }
            temp = from element in summery
                       orderby element.Value.seqNo
                       select element;

            summery = temp.ToDictionary<KeyValuePair<string, SummNode>, string, SummNode>(pair => pair.Key, pair => pair.Value);

            //ordered in sequence as of file
            foreach (var val in summery)
            {
                Console.WriteLine(val.Key);
            }
            

        }
        static void Main(string[] args)
        {

            try
            {
                stopWords = obj.readStringList("StopWords.txt");
                if (stopWords.Count < 1)
                {

                    for (int i = 1; i <= Defs.filesCount; i++)
                    {
                        string page = "chapter " + i.ToString() + ".txt";
                        fileNames.Add(page);

                        populateLexicon(page);

                    }


                    var temp = from element in lexicon
                               orderby element.Value.totalCount descending
                               select element;


                    lexicon = temp.ToDictionary<KeyValuePair<string, Node>, string, Node>(pair => pair.Key, pair => pair.Value);


                    Console.WriteLine("Help in creating list of stop words. Enter y to include.");
                    for (int i = 0; i <= 150 && i < lexicon.Count; i++)
                    {
                        Console.WriteLine(lexicon.ElementAt(i).Key);
                        ConsoleKeyInfo cki = Console.ReadKey(true);
                        if (cki.Key == ConsoleKey.Y)
                        {
                            stopWords.Add(lexicon.ElementAt(i).Key);
                        }
                    }
                    File.WriteList("StopWords.txt", stopWords);

                }
                for (int i = 1; i <= Defs.filesCount; i++)
                {
                    string page = "chapter " + i.ToString() + ".txt";
                    fileNames.Add(page);
                    populateCandidateKW(page);

                }

                for (int i = 0; i < Keywords.Count; i++)
                {
                    FreqMatNode n=new FreqMatNode (i,Keywords.Count);
                    freqMatrix.Add(Keywords.ElementAt(i).Key, n);
                }

                
                for (int i = 0; i < freqMatrix.Count; i++)
                {
                    for (int j = i; j < Keywords.Count; j++)
                    {
                        freqMatrix[freqMatrix.ElementAt(i).Key].arr[j] = PreprocessingHelper.freqHelper(freqMatrix.ElementAt(i).Key, freqMatrix.ElementAt(j).Key);
                        
                        freqMatrix[freqMatrix.ElementAt(j).Key].arr[i] = freqMatrix[freqMatrix.ElementAt(i).Key].arr[j];
                        
                    }
                    freqMatrix[freqMatrix.ElementAt(i).Key].sum = freqMatrix[freqMatrix.ElementAt(i).Key].arr.Sum();
                }

                for(int i=0;i<Keywords.Count;i++)
                {
                    Keywords[Keywords.ElementAt(i).Key] = (double)freqMatrix[Keywords.ElementAt(i).Key].sum / (double)freqMatrix[Keywords.ElementAt(i).Key].arr[freqMatrix[Keywords.ElementAt(i).Key].ind];
                }

                for (int i = 0; i < candidateKeywords.Count;i++ )
                {

                    string[] words = candidateKeywords.ElementAt(i).Key.Split(' ');
                    double weight = 0.0;
                    foreach (var v in words)
                    {
                        weight = weight + Keywords[v];
                    }
                    candidateKeywords[candidateKeywords.ElementAt(i).Key] = weight;
                }

                Console.WriteLine("How many keywords do you want?");
                int inp =int.Parse (Console.ReadLine());

                var t = from element in candidateKeywords
                           orderby element.Value descending
                           select element;


                candidateKeywords = t.ToDictionary<KeyValuePair<string, double>, string, double>(pair => pair.Key, pair => pair.Value);

                Console.WriteLine("Keywords are:");
                for (int i = 0; i < inp&&i<candidateKeywords.Count; i++)
                {
                    Console.WriteLine(candidateKeywords.ElementAt(i).Key);
                }

                
                Console.WriteLine("\n******  \nSummery is:");
                textSummerization();


            }
            catch (KeyNotFoundException e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
