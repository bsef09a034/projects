﻿using java.io;
using opennlp.tools.namefind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
namespace NER
{
    class OpenNlp
    {
        static NameFinderME Model = null;
        
        public static void InitializeModel()
        {
            FileInputStream nameModelStream =
                new FileInputStream(@"Models\OpenNlp\en-ner-location.bin");
            opennlp.tools.namefind.TokenNameFinderModel model =
                new opennlp.tools.namefind.TokenNameFinderModel(nameModelStream);
            Model = new opennlp.tools.namefind.NameFinderME(model);
            Model.clearAdaptiveData();
        }

        public static void GetResults(string[] tokens, int sentenceId, ref List<Data.Input> data)
        {
            var result = Model.find(tokens);
            var results = opennlp.tools.util.Span.spansToStrings(result, tokens).AsEnumerable();
            data.Where(d => d.SentenceId == sentenceId && results.Contains(d.Word)).ToList().ForEach(d => d.OpenNlp = "LOCATION");
            Model.clearAdaptiveData();
        }
    }
}
