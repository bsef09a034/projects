﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NER
{
    class Data
    {
        public static List<Input> ReadData()
        {
            var records = new List<Input>();
            using (var reader = new StreamReader("Data/ner_dataset_test.csv"))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                int sentenceId = 0;

                csv.Read();
                csv.ReadHeader();
                while (csv.Read())
                {
                    if (!string.IsNullOrWhiteSpace(csv.GetField("Sentence #")))
                    {
                        sentenceId++;
                    }
                    var record = new Input
                    {
                        SentenceId = sentenceId,
                        Word = csv.GetField("Word"),
                        Pos = csv.GetField("POS"),
                        Tag = csv.GetField("Tag"),
                    };

                    records.Add(record);
                }
            }
            return records;
        }

        public static void WriteData(List<Input> data)
        {
            using (var writer = new StreamWriter("Data/results.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(data);
            }
        }

        public class Input
        {
            public int SentenceId { get; set; }
            public string Word { get; set; }
            public string Pos { get; set; }
            public string Tag { get; set; }
            public string OpenNlp { get; set; }
            public string CoreNlp { get; set; }

        }
    }
}
