﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
namespace NER
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initialize models
            OpenNlp.InitializeModel();

            //Read data
            var data = Data.ReadData();


            for(int i=1;i<=data.Last().SentenceId;i++)
            {
                var tokens = data.Where(d=>d.SentenceId == i).Select(d=>d.Word).ToArray();
                OpenNlp.GetResults(tokens, i, ref data);
                CoreNlp.GetResults(tokens, i, ref data);
            }

            Data.WriteData(data);
        }


    }
}
