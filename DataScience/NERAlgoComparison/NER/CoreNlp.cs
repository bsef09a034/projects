﻿using SimpleNetNlp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NER
{
    class CoreNlp
    {
        public static void InitializeModel()
        {
        }

        public static void GetResults(string[] tokens, int sentenceId, ref List<Data.Input> data)
        {
            Console.WriteLine(sentenceId);
            var errors = string.Empty;
            var modelSentence = new Sentence(string.Join(" ", tokens));
            var ner = modelSentence.NerTags;
            int length = tokens.Count();
            if (ner.Count() < tokens.Count())
            {
                length = ner.Count();
                errors += sentenceId + ",";
            }
            for(int i =0;i< length; i++)
            {
                data.Where(d => d.SentenceId == sentenceId).ElementAt(i).CoreNlp = ner.ElementAt(i);
            }
            File.AppendAllText("Issues.txt", errors);
        }
    }
}
