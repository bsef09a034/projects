﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace Pro
{
    static class File
    {
        public static void readData(string fname)
        {

            int lineNo = 0;
	        string temp;
	        StreamReader iff=new StreamReader (fname);
            List<InputNode> list = new  List<InputNode> ();
	        bool chk=true;
	        try
	        {

                do
                {
                    chk = true;
                    while (chk)
                    {
                        temp = iff.ReadLine();
                        if (!Helper.allEquals(temp))
                        {
                            string[] words = temp.Split('/');
                            string s = "";
                            for (int i = 0; i < words.Length - 1; i++)
                            {
                                s = s + words[i];
                            }
                            string s2 = words[words.Length - 1];
                            string[] w = s2.Split('|');
                            list.Add(new InputNode(s, w[0]));
                        }
                        else chk = false;
                    }
                    
                    int ind = -1;
                    for (int i = 0; i <= list.Count; i++)
                    {
                        lineNo++;
                        try
                        {
                            if (i == 0)
                            {
                                ind = Program.A[list[i].pos].ind;
                                Program.A["<s>"].arr[ind]++;
                                Program.A["<s>"].sum++;
                            }
                            else if (i == list.Count)
                            {
                                ind = Program.A.First().Value.arr.Length - 1;
                                Program.A[list[i - 1].pos].arr[ind]++;
                                Program.A[list[i - 1].pos].sum++;
                            }
                            else
                            {
                                ind = Program.A[list[i].pos].ind;
                                Program.A[list[i - 1].pos].arr[ind]++;
                                Program.A[list[i - 1].pos].sum++;
                            }

                            if (i != list.Count)
                            {
                                ind = Program.A[list[i].pos].ind;
                                if (Program.B.ContainsKey(list[i].word))
                                    Program.B[list[i].word].arr[ind]++;
                                else
                                {
                                    BNode tmp = new BNode();
                                    Program.B.Add(list[i].word, tmp);
                                    Program.B[list[i].word].arr[ind]++;
                                }
                            }
                        }
                        catch { }
                    }
                    list.Clear();
                    


                } while (!iff.EndOfStream);
                    
            
                iff.Close();
                    
            
            
            }
            catch(FileNotFoundException)
	        {
		       
		        Console.WriteLine( "File could be not found.");
	        }
	        
        }

        //testing...

        public static List<List<InputNode>> readTestData(string fname)
        {
            List<List<InputNode>> retList = new List<List<InputNode>>();
            string temp;
            StreamReader iff = new StreamReader(fname);
           
            bool chk = true;
            try
            {

                do
                { 
                    List<InputNode> list = new List<InputNode>();
                    chk = true;
                    while (chk)
                    {
                        temp = iff.ReadLine();
                        if (!Helper.allEquals(temp))
                        {
                            string[] words = temp.Split('/');
                            list.Add(new InputNode(words[0], words[1]));
                        }
                        else chk = false;
                    }

                    retList.Add(list);
                } while (!iff.EndOfStream);


                iff.Close();
                


            }
            catch (FileNotFoundException)
            {

                Console.WriteLine("File could be not found.");
            }
            return retList;

        }

        public static void writeFileA(string fname,Dictionary<string, ANode> A)
        {
	
            StreamWriter off=new StreamWriter (fname);

            try
            {
             
	         
                foreach (var val in A)
                {
                    off.Write(val.Value.ind + " " + val.Key + "\t");
                    foreach (var v in val.Value.arr)
                    {
                        off.Write(v + " ");
                    }
                    off.WriteLine("sum= " + val.Value.sum);
                }



            }
            catch (Exception e)
            {
                Console.WriteLine("File could be created." + e.ToString());
            }
            off.Close();
        }
        public static void writeFileB(string fname, Dictionary<string, BNode> B)
        {

            StreamWriter off = new StreamWriter(fname);

            try
            {


                foreach (var val in B)
                {
                    off.Write(val.Key + "\t");
                    foreach (var v in val.Value.arr)
                    {
                        off.Write(v + " ");
                    }
                    off.WriteLine();
                }



            }
            catch (Exception e)
            {
                Console.WriteLine("File could be created." + e.ToString());
            }
            off.Close();
        }
        public static void writeComputedResults(string fname, List<List<InputNode>> modified)
        {

            StreamWriter off = new StreamWriter(fname);

            try
            {


                foreach (var val in modified)
                {
                    foreach (var v in val)
                    {
                        off.WriteLine(v.word + "/" + v.pos);
                    }
                    off.WriteLine("=========");
                }



            }
            catch (Exception e)
            {
                Console.WriteLine("File could be created." + e.ToString());
            }
            off.Close();
        }
        public static  void readTags(string fname)
        {
           
            StreamReader iff=new StreamReader (fname);
	
	        try
	        {
             
                   string temp;
                   int i = -1;

                   Program.A.Add("<s>", new ANode(i));
                   i++;

                   while(!iff.EndOfStream)
                   {
                       temp = iff.ReadLine();
                       temp = temp.Trim();
                       if (!Program.A.ContainsKey(temp))
                       {
                           ANode node = new ANode(i);

                           Program.A.Add(temp, node);
                           i++;
                       }
                   }
            
                   iff.Close();
            
                                
            }
            catch(Exception e)
	        {
		        Console.WriteLine( "File could be not found."+e.ToString());
	        }
	       
               
        }
        public static void writeResult(string fname,List<string> results)
        {
	
	        StreamWriter off=new StreamWriter (fname);

	        try
	        {
                    int size=results.Count();
                    for(int i=0;i<size;i++)
	                {
                        off.WriteLine(results[i]);
                    }
            
             }
             
            catch(Exception e)
	        {
		        Console.WriteLine( "File could be created."+e.ToString());
	        }
            off.Close();
        }

    }
}
