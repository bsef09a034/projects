﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pro
{
    class Program
    {
        static public Dictionary<string, ANode> A = new Dictionary<string, ANode>();
        static public Dictionary<string, BNode> B=new Dictionary<string,BNode> ();
        static void Main(string[] args)
        {
            
        
            try
            {
                File.readTags("tags.txt");
                File.readData("input.txt");
                File.writeFileA("A.txt", A);
                File.writeFileB("B.txt", B);
                List<List<InputNode>> original= File.readTestData("testInput.txt");

                List<List<InputNode>> modified=Logic.test(Helper.copy(original), Helper.copy(original));
                File.writeComputedResults("results.txt", modified);

                Console.WriteLine("See the files: ");
                Console.WriteLine("input.txt=> contains training data. ");
                Console.WriteLine("tags.txt=> contains all possible tags. ");
                Console.WriteLine("testInput.txt=> contains testing data. ");
                Console.WriteLine("results.txt=> contains computed results against training data. ");
                
                
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception\n" + e.ToString());
            }
                }
            }
}
