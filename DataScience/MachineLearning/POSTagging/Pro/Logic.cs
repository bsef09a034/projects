﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pro
{
    static class Logic
    {
        static double computePtt(string prev, string now)
        {
            int ind=-1;
            if (now != "</s>")
            {
                ind = Program.A[now].ind;
            }
            else ind = Program.A.First().Value.arr.Length - 1;

            double x = Program.A[prev].arr[ind];
            double y = Program.A[prev].sum;
            return x / y;
        }

        static double computePwt(string w, string t)
        {
            int ind = Program.A[t].ind;
            double x = Program.B[w].arr[ind];
            double y = Program.A[t].sum;
            return x / y;
        }

        public static List<List<InputNode>> test(List<List<InputNode>> Actlist, List<List<InputNode>> Modlist)
        {
            double countOfCorrectResult = 0, totalWords = 0;
            for (int i = 0; i < Modlist.Count; i++)
            {
                bool Wordchk = false;
                for (int j = 0; j <Modlist[i].Count; j++)
                {
                    double max = double.MinValue, tmp = 0,x=0,y=0 ;
                    string result = "";
                    string tempWord="";
                   
                    int k = 0;
                    Wordchk = false;
                    try
                    {
                        int count = 0;
                        try
                        {
                            count = Program.B[Modlist[i][j].word].arr.Length;
                        }
                        catch (KeyNotFoundException)
                        {
                            tempWord = Modlist[i][j].word;
                            Modlist[i][j].word = Program.B.First().Key;
                            Wordchk = true;
                        }
                        
                        for (k=0; k < Program.B[Modlist[i][j].word].arr.Length; k++)
                        {
                            
                            if (Program.B[Modlist[i][j].word].arr[k] != 0)
                            {
                                if (j == 0)
                                {
                                    x = computePwt(Modlist[i][j].word, Program.A.Keys.ElementAt(k + 1));
                                    y = computePtt("<s>", Program.A.Keys.ElementAt(k + 1));
                                }
                                else if (j == Modlist[i].Count)
                                {
                                    x = computePwt(Modlist[i][j].word, Program.A.Keys.ElementAt(k + 1));
                                    y = computePtt(Modlist[i][j - 1].pos, "</s>");
                                }
                                else
                                {
                                    x = computePwt(Modlist[i][j].word, Program.A.Keys.ElementAt(k + 1));
                                    y = computePtt(Modlist[i][j - 1].pos, Program.A.Keys.ElementAt(k + 1));

                                }
                                tmp = x * y;
                                if (tmp > max)
                                {
                                    max = tmp;
                                    result = Program.A.Keys.ElementAt(k + 1);
                                }
                            }

                        }
                        if(Wordchk==true)
                        Modlist[i][j].word = tempWord;
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        if (j == Modlist[i].Count)
                        {
                            x = computePwt(Modlist[i][j].word, Program.A.Keys.ElementAt(k + 1));
                            y = computePtt(Modlist[i][j - 1].pos, "</s>");
                            tmp = x * y;
                            if (tmp > max)
                            {
                                max = tmp;
                                result = Program.A.Keys.ElementAt(k + 1);
                            }
                        }
                    }
                    Modlist[i][j].pos = result;
                    if (Actlist[i][j].pos == Modlist[i][j].pos)
                    {
                        countOfCorrectResult++;
                    }
                    totalWords++;
                    
                }

            }
            Console.WriteLine("Precision is: "+ countOfCorrectResult+"/"+totalWords+" = "+ (countOfCorrectResult/totalWords));
            return Modlist;

        }
    }
}
