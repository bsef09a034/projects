﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pro
{
    class ANode
    {
        public int ind;
        public double sum=0;
        public double []arr;
        public ANode() { }
        public ANode(int i)
        {
            ind = i;
            arr = new double[Defs.tagCount+1];
            for (int j = 0; j < arr.Length;j++ )
            {
                arr[j] = Defs.defaultVal;
            }
        }
    }
}
