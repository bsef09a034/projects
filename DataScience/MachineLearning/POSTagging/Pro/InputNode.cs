﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pro
{
    class InputNode
    {
        public string word;
        public string pos;
        public InputNode(string w="", string p="")
        {
            word = w;
            pos = p;
        }
    }
}
