﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pro
{
    static class Helper
    {
       
        public static bool allEquals(string s)
        {
            bool retval = true;
            for (int i = 0; i < s.Length && retval; i++)
            {
                if (s[i] != '=')
                {
                    retval = false;
                }
            }
            return retval;
        }
        public static List<List<InputNode>> copy(List<List<InputNode>> a)
        {
            List<List<InputNode>> b=new List<List<InputNode>> ();

            for (int i = 0; i < a.Count; i++)
            {
                List<InputNode> l = new List<InputNode>();
                for (int j = 0; j < a[i].Count; j++)
                {
                    InputNode temp = new InputNode(a[i][j].word, a[i][j].pos);
                    l.Add(temp);
                }
                b.Add(l);
            }
            return b;
        }

    }
}
