﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace classification
{
    class Program
    {
        static fileRead obj = new fileRead();
        static List<DataBO> list;
        static int p = 0, q = 0;

        static Dictionary<string, ProbBO> day = new Dictionary<string, ProbBO>();
        static Dictionary<string, ProbBO> season = new Dictionary<string, ProbBO>();
        static Dictionary<string, ProbBO> wind = new Dictionary<string, ProbBO>();
        static Dictionary<string, ProbBO> rain = new Dictionary<string, ProbBO>();
        
        static void test(List<DataBO> list)
        {
            double probP = (double)p / (double)(p + q);
            double probQ = (double)q / (double)(p + q);
            Console.WriteLine("ProbP= "+probP+"ProbQ= "+probQ);
            Console.WriteLine("Actual \t Predicted \t\t ProbLate \t ProbOntime");
            foreach(var val in list)
            {
                //All Ontime joint probabilities
                
                double probOntime, probLate;
                try
                {
                    probOntime = (double)day[val.day].onTime * (double)season[val.season].onTime * (double)wind[val.wind].onTime * (double)rain[val.rain].onTime * (double)probP;
                   
                }
                catch (KeyNotFoundException)
                {
                    //Console.WriteLine("exOnTime");
                    probOntime = 0;
                }
                try
                {
                    probLate = (double)day[val.day].late * (double)season[val.season].late * (double)wind[val.wind].late * (double)rain[val.rain].late * (double)probQ;
                    
                }
                catch (KeyNotFoundException)
                {
                   // Console.WriteLine("exLate");
                    probLate = 0;
                }
                string result="";
                if (probOntime > probLate) result = "It will be on time";
                else if(probOntime<probLate)result = "It will be late  ";
                else result = "No decision\t";

                Console.WriteLine(val.clas + "\t" + result + "\t\t" + probLate + "\t" +probOntime);
            }

        }
        static void train(List<DataBO> list)
        {
            foreach (var val in list)
            {
                if (val.clas == "ONTIME") p++;
                else if (val.clas == "LATE") q++;
            }
            
            foreach (var val in list)
            {
                if (day.ContainsKey(val.day))
                {
                    if (val.clas == "ONTIME")
                    {
                        day[val.day].onTime = day[val.day].onTime + (1.0 / (double)p);
                        day[val.day].total = day[val.day].total + ((double)1 / (double)(p + q));
                    }
                    else if (val.clas == "LATE")
                    {
                        day[val.day].late = day[val.day].late + ((double)1 / (double)q);
                        day[val.day].total = day[val.day].total + ((double)1 / (double)(p + q));
                    }

                }
                else
                {
                    ProbBO tempObj = new ProbBO();
                    if (val.clas == "ONTIME")
                    {
                        tempObj.onTime = (double)1 / (double)p;
                        tempObj.total = ((double)1 / (double)(p + q));
                    }
                    else if (val.clas == "LATE")
                    {
                        tempObj.late = (double)1 / (double)q;
                        tempObj.total = ((double)1 / (double)(p + q));
                    }
                    day.Add(val.day, tempObj);

                }
                
                // for season

                if (season.ContainsKey(val.season))
                {
                    if (val.clas == "ONTIME")
                    {
                        season[val.season].onTime = season[val.season].onTime + ((double)1 / (double)p);
                        season[val.season].total = season[val.season].total + ((double)1 / (double)(p + q));
                    }
                    else if (val.clas == "LATE")
                    {
                        season[val.season].late = season[val.season].late + ((double)1 / (double)q);
                        season[val.season].total = season[val.season].total + ((double)1 / (double)(p + q));
                    }

                }
                else
                {
                    ProbBO tempObj = new ProbBO();
                    if (val.clas == "ONTIME")
                    {
                        tempObj.onTime = (double)1 / (double)p;
                        tempObj.total = ((double)1 / (double)(p + q));
                    }
                    else if (val.clas == "LATE")
                    {
                        tempObj.late = (double)1 / (double)q;
                        tempObj.total = ((double)1 / (double)(p + q));
                    }
                    season.Add(val.season, tempObj);

                }

                // for wind

                if (wind.ContainsKey(val.wind))
                {
                    if (val.clas == "ONTIME")
                    {
                        wind[val.wind].onTime = wind[val.wind].onTime + ((double)1 / (double)p);
                        wind[val.wind].total = wind[val.wind].total + ((double)1 / (double)(p + q));
                    }
                    else if (val.clas == "LATE")
                    {
                        wind[val.wind].late = wind[val.wind].late + ((double)1 / (double)q);
                        wind[val.wind].total = wind[val.wind].total + ((double)1 / (double)(p + q));
                    }

                }
                else
                {
                    ProbBO tempObj = new ProbBO();
                    if (val.clas == "ONTIME")
                    {
                        tempObj.onTime = (double)1 / (double)p;
                        tempObj.total = ((double)1 / (double)(p + q));
                    }
                    else if (val.clas == "LATE")
                    {
                        tempObj.late = (double)1 / (double)q;
                        tempObj.total = ((double)1 / (double)(p + q));
                    }
                    wind.Add(val.wind, tempObj);

                }

                // for rain

                if (rain.ContainsKey(val.rain))
                {
                    if (val.clas == "ONTIME")
                    {
                        rain[val.rain].onTime = rain[val.rain].onTime + ((double)1 / (double)p);
                        rain[val.rain].total = rain[val.rain].total + ((double)1 / (double)(p + q));
                    }
                    else if (val.clas == "LATE")
                    {
                        rain[val.rain].late = rain[val.rain].late + ((double)1 / (double)q);
                        rain[val.rain].total = rain[val.rain].total + ((double)1 / (double)(p + q));
                    }

                }
                else
                {
                    ProbBO tempObj = new ProbBO();
                    if (val.clas == "ONTIME")
                    {
                        tempObj.onTime = (double)1 / (double)p;
                        tempObj.total = ((double)1 / (double)(p + q));
                    }
                    else if (val.clas == "LATE")
                    {
                        tempObj.late = (double)1 / (double)q;
                        tempObj.total = ((double)1 / (double)(p + q));
                    }
                    rain.Add(val.rain, tempObj);

                }

               
            }
        }
        static void Main(string[] args)
        {
            obj.read();
            list = obj.train();
            train(list);
            list = obj.test();
            test(list);
        }
    }
}
