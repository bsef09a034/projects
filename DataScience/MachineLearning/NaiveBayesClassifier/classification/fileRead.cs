﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace classification
{
    class fileRead
    {
        public StreamReader sr = null;
        public List<DataBO> list=new List<DataBO>();

        
        int p = 0;
        public void read()
        {
            try
            {
                
                sr = new StreamReader("input.txt");
                while (!sr.EndOfStream)
                {
                    string s=sr.ReadLine();
                    string []words=s.Split('\t');
                    DataBO temp = new DataBO();
                    temp.day = words[0];
                    temp.season = words[1];
                    temp.wind = words[2];
                    temp.rain = words[3];
                    temp.clas = words[4];

                    list.Add(temp);
                   
                }

                
            }
            catch 
            {
                Console.WriteLine("Error!");
            }
            sr.Close();


            
        }
        public List<DataBO> train()
        {
            return list.GetRange(0, 5);
        }
        public List<DataBO> test()
        {
            return list.GetRange(4,5);
        }


    }
}
