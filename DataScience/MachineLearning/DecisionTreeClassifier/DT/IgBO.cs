﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DT
{
    class IgBO
    {
        public double ig = 0;
        public string name;
        public int ind;
        public Dictionary<char, ProbBO> att;

        public IgBO()
        { }
        
        public IgBO(double i, Dictionary<char, ProbBO> a,string n,int index)
        {
            ig = i;
            att = a;
            name = n;

            ind = index;
        }
    
       
    }
}
