using System;
using System.Collections.Generic;
using System.Text;

namespace DT
{
    class Node
    {
        public int A=0, B=0, C=0; 
        public char val;
        public List<Node> childs;
        public Node() { }
        public Node(char v) { val = v; }
        public Node(char v,List<Node> l)
        {
            val = v;
            childs = l;
        }
        public void addList(List<Node> l)
        {
            childs = l;
        }
        public Node(Node n) 
        {
            A = n.A;
            B = n.B;
            C = n.C;
            val = n.val;
            childs = new List<Node>(n.childs);
        }
        public List<Node> copy(List<Node> l) 
        {
            List<Node> ret = new List<Node>();
            foreach(Node val in l)
            {
                ret.Add(new Node(val));
            }
            return ret;
        }

        
    }
}
