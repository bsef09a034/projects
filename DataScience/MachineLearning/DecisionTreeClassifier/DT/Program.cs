using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DT
{
    class Program
    {
        static File obj = new File();
        static List<DataBO> list;
        static int nA = 0, nB = 0, nC = 0;
        static int classCount = 3;
        static Dictionary<char, ProbBO> reff = new Dictionary<char, ProbBO>();
        static Dictionary<char, ProbBO> intVw = new Dictionary<char, ProbBO>();
        static Dictionary<char, ProbBO> cgpa = new Dictionary<char, ProbBO>();
        static Dictionary<char, ProbBO> fsc = new Dictionary<char, ProbBO>();
        static Dictionary<char, ProbBO> met = new Dictionary<char, ProbBO>();
        static Node root;
        static int misClass = 0, leafCount = 0;
        static double trErr = 0, tsErr = 0, pesErr = double.MaxValue;
        static List<Node> makeList(Dictionary<char, ProbBO> d)
        {
            List<Node> l = new List<Node>();
            foreach (KeyValuePair<char, ProbBO> kvp in d)
            {
                l.Add(new Node(kvp.Key));
            }

            return l;
        }
        static int indexOf(char c, List<Node> l)
        {
            bool chk = true;
            int ind = -1;
            for (int i = 0; i < l.Count && chk; i++)
            {
                if (l[i].val == c)
                {
                    ind = i;
                    chk = false;
                }
            }
            return ind;

        }
        static int newMisClass(int A, int B, int C, int a, int b, int c)
        {
            int retVal = 0;
            int Max = 0, max = 0, Sum, sum;

            Max = Math.Max(A, Math.Max(B, C));
            max = Math.Max(a, Math.Max(b, c));
            Sum = A + B + C - Max;
            sum = a + b + c - max;



            return retVal;

        }
        static double compPesErr(int n, int c)
        {
            return (((double)n + (double)leafCount * 0.5) / (double)c);
        }
        static void populateTree(List<IgBO> l)
        {
            int count1 = 0, count2 = 0;
            List<DataBO> trainList = obj.train();
            foreach (var val in trainList)
            {
                DataBO temp = val;
                char[] arr = new char[l.Count - 1];
                int j = 0;
                for (int i = l.Count - 1; i > 0; i--)
                {
                    if (l[i].name == "reff") arr[j] = temp.reff;
                    else if (l[i].name == "intVw") arr[j] = temp.intVw;

                    else if (l[i].name == "cgpa") arr[j] = temp.cgpa;
                    else if (l[i].name == "fsc") arr[j] = temp.fsc;

                    else if (l[i].name == "met") arr[j] = temp.met;
                    j++;
                }
                bool chk = true;
                Node now = root;
                Node parent;

                for (int i = 0; i < l.Count && chk; i++)
                {
                    if (temp.rank == 'A') now.A++;
                    else if (temp.rank == 'B') now.B++;
                    else if (temp.rank == 'C') now.C++;

                    parent = now;
                    int k = indexOf(arr[i], parent.childs);
                    now = parent.childs[k];
                    int n = 0;
                    if (temp.rank == 'A') n = newMisClass(now.A, now.B, now.C, now.A + 1, now.B, now.C);
                    else if (temp.rank == 'B') n = newMisClass(now.A, now.B, now.C, now.A, now.B + 1, now.C);
                    else if (temp.rank == 'C') n = newMisClass(now.A, now.B, now.C, now.A, now.B, now.C + 1);

                    double pe = 0, te = 0;
                    pe = compPesErr(n, (root.A + root.B + root.C) + 1);
                    Console.WriteLine(pe);

                    count1++;
                    if ((now.A == now.B) && (now.B == now.C) && (now.C == 0))
                    {
                        leafCount++;
                    }
                    if (pe < pesErr)
                    {
                        count2++;
                        misClass = n;
                        pesErr = pe;


                    }
                    else chk = false;


                }

            }
            Console.WriteLine("count1=" + count1 + "count2= " + count2 + "leafs" + leafCount);

        }
        static double comPE(List<Node> l)
        {
            double lc = l.Count;
            double tp = 0.5;
            double mc = 0, tempmc = 0;
            double tc = 0, temptc;
            for (int i = 0; i < l.Count; i++)
            {
                temptc = l[i].A + l[i].B + l[i].C;
                tempmc = temptc - Math.Max(l[i].A, Math.Max(l[i].B, l[i].C));
                mc = mc + tempmc;
                tc = tc + temptc;
            }
            return (mc + lc * tp) / tc;

        }
        static void makeTree(List<IgBO> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                Console.WriteLine(l[i].name + l[i].ig + "int=" + l[i].ind);
            }

            Node root = new Node();
            root.val = 'R';
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].rank == 'A') root.A++;
                else if (list[i].rank == 'B') root.B++;
                else if (list[i].rank == 'C') root.C++;
            }
            List<Node> temp = new List<Node>();
            temp.Add(root);
            double pe = comPE(temp);
            pesErr = pe;


            int count = l.Count - 1;


            temp = new List<Node>();
            Node node1 = new Node();
            node1.val = 'L';
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].cgpa == node1.val && list[i].rank == 'A') node1.A++;
                else if (list[i].cgpa == node1.val && list[i].rank == 'B') node1.B++;
                else if (list[i].cgpa == node1.val && list[i].rank == 'C') node1.C++;
            }
            temp.Add(node1);

            Node node2 = new Node();
            node2.val = 'M';
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].cgpa == node2.val && list[i].rank == 'A') node2.A++;
                else if (list[i].cgpa == node2.val && list[i].rank == 'B') node2.B++;
                else if (list[i].cgpa == node2.val && list[i].rank == 'C') node2.C++;
            }
            temp.Add(node2);

            Node node3 = new Node();
            node3.val = 'H';
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].cgpa == node3.val && list[i].rank == 'A') node3.A++;
                else if (list[i].cgpa == node3.val && list[i].rank == 'B') node3.B++;
                else if (list[i].cgpa == node3.val && list[i].rank == 'C') node3.C++;
            }
            temp.Add(node3);
        }

        static void initializeDictionaries(List < DataBO > list)
        {
                foreach (DataBO val in list)
                {
                    if (reff.ContainsKey(val.reff))
                    {
                        reff[val.reff].count++;



                    }
                    else
                    {
                        ProbBO tempObj = new ProbBO();
                        tempObj.count = 1;

                        reff.Add(val.reff, tempObj);

                    }

                    // for interView

                    if (intVw.ContainsKey(val.intVw))
                    {
                        intVw[val.intVw].count++;


                    }
                    else
                    {
                        ProbBO tempObj = new ProbBO();
                        tempObj.count = 1;


                        intVw.Add(val.intVw, tempObj);

                    }

                    // for cgpa

                    if (cgpa.ContainsKey(val.cgpa))
                    {
                        cgpa[val.cgpa].count++;



                    }
                    else
                    {
                        ProbBO tempObj = new ProbBO();
                        tempObj.count = 1;


                        cgpa.Add(val.cgpa, tempObj);

                    }

                    // for fsc
                    if (fsc.ContainsKey(val.fsc))
                    {
                        fsc[val.fsc].count++;

                    }
                    else
                    {
                        ProbBO tempObj = new ProbBO();
                        tempObj.count = 1;

                        fsc.Add(val.fsc, tempObj);

                    }

                    // for metric

                    if (met.ContainsKey(val.met))
                    {
                        met[val.met].count++;


                    }
                    else
                    {
                        ProbBO tempObj = new ProbBO();
                        tempObj.count = 1;

                        met.Add(val.met, tempObj);

                    }
                }
            }

        static void feedProbabilities(List < DataBO > list)
        {
                foreach (DataBO val in list)
                {
                    if (reff.ContainsKey(val.reff))
                    {
                        if (val.rank == 'A')
                        {
                            reff[val.reff].probA = reff[val.reff].probA + (1.0 / (double)reff[val.reff].count);
                        }
                        else if (val.rank == 'B')
                        {
                            reff[val.reff].probB = reff[val.reff].probB + (1.0 / (double)reff[val.reff].count);
                        }
                        else if (val.rank == 'C')
                        {
                            reff[val.reff].probC = reff[val.reff].probC + (1.0 / (double)reff[val.reff].count);
                        }




                    }
                    // for interView

                    if (intVw.ContainsKey(val.intVw))
                    {
                        if (val.rank == 'A')
                        {
                            intVw[val.intVw].probA = intVw[val.intVw].probA + (1.0 / (double)intVw[val.intVw].count);
                        }
                        else if (val.rank == 'B')
                        {
                            intVw[val.intVw].probB = intVw[val.intVw].probB + (1.0 / (double)intVw[val.intVw].count);
                        }
                        else if (val.rank == 'C')
                        {
                            intVw[val.intVw].probC = intVw[val.intVw].probC + (1.0 / (double)intVw[val.intVw].count);
                        }


                    }


                    // for cgpa

                    if (cgpa.ContainsKey(val.cgpa))
                    {
                        if (val.rank == 'A')
                        {
                            cgpa[val.cgpa].probA = cgpa[val.cgpa].probA + (1.0 / (double)cgpa[val.cgpa].count);
                        }
                        else if (val.rank == 'B')
                        {
                            cgpa[val.cgpa].probB = cgpa[val.cgpa].probB + (1.0 / (double)cgpa[val.cgpa].count);
                        }
                        else if (val.rank == 'C')
                        {
                            cgpa[val.cgpa].probC = cgpa[val.cgpa].probC + (1.0 / (double)cgpa[val.cgpa].count);
                        }
                    }


                    // for fsc
                    if (fsc.ContainsKey(val.fsc))
                    {
                        if (val.rank == 'A')
                        {
                            fsc[val.fsc].probA = fsc[val.fsc].probA + (1.0 / (double)fsc[val.fsc].count);
                        }
                        else if (val.rank == 'B')
                        {
                            fsc[val.fsc].probB = fsc[val.fsc].probB + (1.0 / (double)fsc[val.fsc].count);
                        }
                        else if (val.rank == 'C')
                        {
                            fsc[val.fsc].probC = fsc[val.fsc].probC + (1.0 / (double)fsc[val.fsc].count);
                        }

                    }

                    // for metric

                    if (met.ContainsKey(val.met))
                    {
                        if (val.rank == 'A')
                        {
                            met[val.met].probA = met[val.met].probA + (1.0 / (double)met[val.met].count);
                        }
                        else if (val.rank == 'B')
                        {
                            met[val.met].probB = met[val.met].probB + (1.0 / (double)met[val.met].count);
                        }
                        else if (val.rank == 'C')
                        {
                            met[val.met].probC = met[val.met].probC + (1.0 / (double)met[val.met].count);
                        }

                    }

                }
            }

        static void train(List < DataBO > list)
        {
                initializeDictionaries(list);
                feedProbabilities(list);

                List<IgBO> IG = new List<IgBO>();
                double wh = 0;

                foreach (KeyValuePair<char, ProbBO> kvp in reff)
                {
                    double[] p = new double[classCount];
                    p[0] = kvp.Value.probA;
                    p[1] = kvp.Value.probB;
                    p[2] = kvp.Value.probC;

                    double h, w;
                    if (p[0] == 0 || p[1] == 0 || p[2] == 0)
                    {
                        h = 0.0;
                    }
                    else
                    {
                        h = -1.0 * (p[0] * (Math.Log(p[0]) / Math.Log(2)) + p[1] * (Math.Log(p[1]) / Math.Log(2)) + p[2] * (Math.Log(p[2]) / Math.Log(2)));
                    }

                    w = (double)kvp.Value.count / (double)list.Count;

                    wh = wh + (w * h);

                }

                IG.Add(new IgBO(1.0 - wh, reff, "reff", 0));

                wh = 0;
                foreach (KeyValuePair<char, ProbBO> kvp in intVw)
                {
                    double[] p = new double[classCount];
                    p[0] = kvp.Value.probA;
                    p[1] = kvp.Value.probB;
                    p[2] = kvp.Value.probC;

                    double h, w;
                    if (p[0] == 0 || p[1] == 0 || p[2] == 0)
                    {
                        h = 0.0;
                    }
                    else
                        h = -1 * (p[0] * (Math.Log(p[0]) / Math.Log(2)) + p[1] * (Math.Log(p[1]) / Math.Log(2)) + p[2] * (Math.Log(p[2]) / Math.Log(2)));
                    w = (double)kvp.Value.count / (double)list.Count;

                    wh = wh + (w * h);
                }
                IG.Add(new IgBO(1.0 - wh, intVw, "intVw", 1));

                wh = 0;
                foreach (KeyValuePair<char, ProbBO> kvp in cgpa)
                {
                    double[] p = new double[classCount];
                    p[0] = kvp.Value.probA;
                    p[1] = kvp.Value.probB;
                    p[2] = kvp.Value.probC;
                    double h, w;
                    if (p[0] == 0 || p[1] == 0 || p[2] == 0)
                    {
                        h = 0.0;
                    }
                    else
                        h = -1 * (p[0] * (Math.Log(p[0]) / Math.Log(2)) + p[1] * (Math.Log(p[1]) / Math.Log(2)) + p[2] * (Math.Log(p[2]) / Math.Log(2)));
                    w = (double)kvp.Value.count / (double)list.Count;

                    wh = wh + (w * h);
                }

                IG.Add(new IgBO(1.0 - wh, cgpa, "cgpa", 2));

                wh = 0;
                foreach (KeyValuePair<char, ProbBO> kvp in fsc)
                {
                    double[] p = new double[classCount];
                    p[0] = kvp.Value.probA;
                    p[1] = kvp.Value.probB;
                    p[2] = kvp.Value.probC;


                    double h, w;
                    if (p[0] == 0 || p[1] == 0 || p[2] == 0)
                    {
                        h = 0.0;
                    }
                    else
                        h = -1 * (p[0] * (Math.Log(p[0]) / Math.Log(2)) + p[1] * (Math.Log(p[1]) / Math.Log(2)) + p[2] * (Math.Log(p[2]) / Math.Log(2)));

                    w = (double)kvp.Value.count / (double)list.Count;

                    wh = wh + (w * h);

                }

                IG.Add(new IgBO(1.0 - wh, fsc, "fsc", 3));

                wh = 0;
                foreach (KeyValuePair<char, ProbBO> kvp in met)
                {
                    double[] p = new double[classCount];
                    p[0] = kvp.Value.probA;
                    p[1] = kvp.Value.probB;
                    p[2] = kvp.Value.probC;

                    double h, w;
                    if (p[0] == 0 || p[1] == 0 || p[2] == 0)
                    {
                        h = 0.0;
                    }
                    else
                        h = -1 * (p[0] * (Math.Log(p[0]) / Math.Log(2)) + p[1] * (Math.Log(p[1]) / Math.Log(2)) + p[2] * (Math.Log(p[2]) / Math.Log(2)));
                    w = (double)kvp.Value.count / (double)list.Count;

                    wh = wh + (w * h);
                }
                IG.Add(new IgBO(1.0 - wh, met, "met", 4));


                //IG.Sort();
                var temp = from element in IG
                           orderby element.ig
                           select element;


                //makeTree(IG);
                List<IgBO> newIG = new List<IgBO>();

                foreach (var val in temp)
                {
                    newIG.Add(new IgBO(val.ig, val.att, val.name, val.ind));
                }

                makeTree(newIG);
            }

        static void Main(string[] args)
        {
                obj.read();
                list = obj.train();

                train(list);
            }

        }
    
}
