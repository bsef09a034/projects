using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DT
{
    class Program
    {
        static File obj = new File();
        static List<DataBO> list;
        //static int p = 0, q = 0,r=0;
        static int classCount=3;
        static Dictionary<char, ProbBO> reff = new Dictionary<char, ProbBO>();
        static Dictionary<char, ProbBO> intVw = new Dictionary<char, ProbBO>();
        static Dictionary<char, ProbBO> cgpa = new Dictionary<char, ProbBO>();
        static Dictionary<char, ProbBO> fsc = new Dictionary<char, ProbBO>();
        static Dictionary<char, ProbBO> met = new Dictionary<char, ProbBO>();
        static Node root;
        static int misClass = 0,leafCount=0;
        static double trErr = 0, tsErr = 0, pesErr = 0;
        static List<Node> makeList(Dictionary<char,ProbBO> d)
        {
            List<Node> l = new List<Node>();
            foreach (KeyValuePair<char,ProbBO> kvp in d)
            {
                l.Add(new Node(kvp.Key));
            }

            return l;
        }
        static int indexOf(char c, List<Node> l)
        {
            bool chk = true;
            int ind = -1;
            for (int i = 0; i < l.Count && chk; i++)
            {
                if (l[i].val == c)
                {
                    ind = i;
                    chk = false;
                }
            }
            return ind;

        }
        static void populateTree(List<IgBO> l)
        {
            
            List<DataBO> trainList = obj.train();
            foreach (var val in trainList)
            {
                DataBO temp = val;
                char[] arr = new char[l.Count];
                for (int i = 0; i < l.Count; i++)
                {
                    if (l[i].name == "reff") arr[i] = temp.reff;
                    else if (l[i].name == "intVw") arr[i] = temp.intVw;

                    else if (l[i].name == "cgpa") arr[i] = temp.cgpa;
                    else if (l[i].name == "fsc") arr[i] = temp.fsc;

                    else if (l[i].name == "met") arr[i] = temp.met;
                }
                bool chk = true;
                Node now = root;
                Node parent;
                for (int i = 0; i < l.Count&&chk; i++)
                {
                    parent = now;
                    int k = indexOf(arr[i], parent.childs);
                    now = parent.childs[k];
                    int n = 0;
                    //int n = newMisClass();
                    double pe=0, te = 0;
                    //pe = compPesErr();
                    //te = compTrErr();
                    if (pe < pesErr)
                    {
                        misClass = n;
                        pesErr = pe;
                        if ((now.A == now.B) && (now.B == now.C) && (now.C == 0))
                        {
                            leafCount++;
                        }
                        if (temp.rank == 'A') now.A++;
                        else if (temp.rank == 'B') now.B++;
                        else if (temp.rank == 'C') now.C++;
                    }
                    else chk = false;
                }
            }
        }
        static void makeTree(List<IgBO> l)
        {
            

            List<Node> temp = null;
           
            for (int i = l.Count - 1; i >= 0; i--)
            {
                
                List<Node> t = new List<Node>();
                foreach (KeyValuePair<char,ProbBO>kvp in l[i].att)
                {
                    
                    t.Add(new Node(kvp.Key, temp));
                }
                temp = t;
            }
            root = new Node('R', temp);
            //temp = root.childs[0].childs[0].childs[0].childs[0].childs;
            //temp=root.childs;
            //for (int i = 0; i < temp.Count; i++)
            //{
            //    Console.WriteLine(temp[i].val);
            //}

            //int c = root.childs.Count;
            //Console.WriteLine("count= "+c);
            //preOrder(root);
            populateTree(l);

            //Console.WriteLine(count);
        }
        static int count=0;
        static void preOrder(Node n) 
        {
            if (n!=null)
            {
                Console.Write( n.val+" ");
                count++;
                try
                {
                    foreach (var val in n.childs)
                    {
                        if (val != null)
                            preOrder(val);
                    }
                }
                catch 
                { 
                    //Console.Write(" "); 
                    
                }
                //preOrder(val);
            }
    }
        //static void func(Node n, Dictionary<char, ProbBO> d)
        //{
        //    List<Node> temp = makeList(d);
        //    n=new Node ()
        //}
        //static void AdderCountHelper(Dictionary<char, probBO> d, char c)
        //{

        //}
        static void initializeDictionaries(List<DataBO> list)
        {
            foreach (DataBO val in list)
            {
                if (reff.ContainsKey(val.reff))
                {
                    reff[val.reff].count++;
                    


                }
                else
                {
                    ProbBO tempObj = new ProbBO();
                    tempObj.count = 1;
                    
                    reff.Add(val.reff, tempObj);

                }

                // for interView

                if (intVw.ContainsKey(val.intVw))
                {
                    intVw[val.intVw].count++;
                    

                }
                else
                {
                    ProbBO tempObj = new ProbBO();
                    tempObj.count = 1;
                   
                    
                    intVw.Add(val.intVw, tempObj);

                }

                // for cgpa

                if (cgpa.ContainsKey(val.cgpa))
                {
                    cgpa[val.cgpa].count++;
                    
                    

                }
                else
                {
                    ProbBO tempObj = new ProbBO();
                    tempObj.count = 1;
                    

                    cgpa.Add(val.cgpa, tempObj);

                }

                // for fsc
                if (fsc.ContainsKey(val.fsc))
                {
                    fsc[val.fsc].count++;
                    
                }
                else
                {
                    ProbBO tempObj = new ProbBO();
                    tempObj.count = 1;
                    
                    fsc.Add(val.fsc, tempObj);

                }

                // for metric

                if (met.ContainsKey(val.met))
                {
                    met[val.met].count++;
                    

                }
                else
                {
                    ProbBO tempObj = new ProbBO();
                    tempObj.count = 1;
                    
                    met.Add(val.met, tempObj);

                }
            }
        }

        static void feedProbabilities(List<DataBO> list)
        {
            foreach (DataBO val in list)
            {
                if (reff.ContainsKey(val.reff))
                {
                    if (val.rank == 'A')
                    {
                        reff[val.reff].probA = reff[val.reff].probA + (1.0 / (double)reff[val.reff].count);
                        //reff[val.reff].total = reff[val.reff].total + ((double)1 / (double)(p + q));
                    }
                    else if (val.rank == 'B')
                    {
                        reff[val.reff].probB = reff[val.reff].probB + (1.0 / (double)reff[val.reff].count);
                    }
                    else if (val.rank == 'C')
                    {
                        reff[val.reff].probC = reff[val.reff].probC + (1.0 / (double)reff[val.reff].count);
                    }




                }
                // for interView

                if (intVw.ContainsKey(val.intVw))
                {
                    if (val.rank == 'A')
                    {
                        intVw[val.intVw].probA = intVw[val.intVw].probA + (1.0 / (double)intVw[val.intVw].count);
                    }
                    else if (val.rank == 'B')
                    {
                        intVw[val.intVw].probB = intVw[val.intVw].probB + (1.0 / (double)intVw[val.intVw].count);
                    }
                    else if (val.rank == 'C')
                    {
                        intVw[val.intVw].probC = intVw[val.intVw].probC + (1.0 / (double)intVw[val.intVw].count);
                    }


                }
                

                // for cgpa

                if (cgpa.ContainsKey(val.cgpa))
                {
                    if (val.rank == 'A')
                    {
                        cgpa[val.cgpa].probA = cgpa[val.cgpa].probA + (1.0 / (double)cgpa[val.cgpa].count);
                    }
                    else if (val.rank == 'B')
                    {
                        cgpa[val.cgpa].probB = cgpa[val.cgpa].probB + (1.0 / (double)cgpa[val.cgpa].count);
                    }
                    else if (val.rank == 'C')
                    {
                        cgpa[val.cgpa].probC = cgpa[val.cgpa].probC + (1.0 / (double)cgpa[val.cgpa].count);
                    }
                }
                

                // for fsc
                if (fsc.ContainsKey(val.fsc))
                {
                    if (val.rank == 'A')
                    {
                        fsc[val.fsc].probA = fsc[val.fsc].probA + (1.0 / (double)fsc[val.fsc].count);
                        //fsc[val.fsc].total = fsc[val.fsc].total + ((double)1 / (double)(p + q));
                    }
                    else if (val.rank == 'B')
                    {
                        fsc[val.fsc].probB = fsc[val.fsc].probB + (1.0 / (double)fsc[val.fsc].count);
                    }
                    else if (val.rank == 'C')
                    {
                        fsc[val.fsc].probC = fsc[val.fsc].probC + (1.0 / (double)fsc[val.fsc].count);
                    }

                }

                // for metric

                if (met.ContainsKey(val.met))
                {
                    if (val.rank == 'A')
                    {
                        met[val.met].probA = met[val.met].probA + (1.0 / (double)met[val.met].count);
                    }
                    else if (val.rank == 'B')
                    {
                        met[val.met].probB = met[val.met].probB + (1.0 / (double)met[val.met].count);
                    }
                    else if (val.rank == 'C')
                    {
                        met[val.met].probC = met[val.met].probC + (1.0 / (double)met[val.met].count);
                    }

                }
                
            }
        }

        static void train(List<DataBO> list)
        {
            //int reffC = 0, intC = 0, cgpaC = 0, fscC = 0,metC=0;
            initializeDictionaries(list);
            feedProbabilities(list);
            
            List<IgBO> IG = new List<IgBO>();
            double wh = 0;

            foreach(KeyValuePair<char,ProbBO> kvp in reff )
            {
                double []p=new double[classCount];
                p[0]=kvp.Value.probA;
                p[1]=kvp.Value.probB;
                p[2]=kvp.Value.probC;
                //Console.WriteLine(p[0] + "=p0");
                //Console.WriteLine(p[1] + "=p1");
                //Console.WriteLine(p[2] + "=p2");


                double h, w;
                if (p[0] == 0 || p[1] == 0 || p[2] == 0)
                {
                    h = 0.0;
                }
                else
                {
                    h = -1.0 * (p[0] * (Math.Log(p[0]) / Math.Log(2)) + p[1] * (Math.Log(p[1]) / Math.Log(2)) + p[2] * (Math.Log(p[2]) / Math.Log(2)));
                }
                //Console.WriteLine(h + "=h");
                w =(double) kvp.Value.count / (double)list.Count;
                //Console.WriteLine(w + "=w");
                wh = wh + (w * h);

                //Console.WriteLine(wh + "=wh");
            }

            //Console.WriteLine(wh + "reff\n****");
            IG.Add(new IgBO(1.0 - wh,reff,"reff"));

            wh = 0;
            foreach (KeyValuePair<char, ProbBO> kvp in intVw)
            {
                double[] p = new double[classCount];
                p[0] = kvp.Value.probA;
                p[1] = kvp.Value.probB;
                p[2] = kvp.Value.probC;

                double h, w;
                if (p[0] == 0 || p[1] == 0 || p[2] == 0)
                {
                    h = 0.0;
                }
                else
                    h = -1 * (p[0] * (Math.Log(p[0]) / Math.Log(2)) + p[1] * (Math.Log(p[1]) / Math.Log(2)) + p[2] * (Math.Log(p[2]) / Math.Log(2)));
                w = (double)kvp.Value.count / (double)list.Count;

                wh = wh + (w * h);
            }
              IG.Add(new IgBO(1.0 - wh,intVw,"intVw"));

            wh = 0;
            foreach (KeyValuePair<char, ProbBO> kvp in cgpa)
            {
                double[] p = new double[classCount];
                p[0] = kvp.Value.probA;
                p[1] = kvp.Value.probB;
                p[2] = kvp.Value.probC;
                double h, w;
                if (p[0] == 0 || p[1] == 0 || p[2] == 0)
                {
                    h = 0.0;
                }
                else
                h = -1 * (p[0] * (Math.Log(p[0]) / Math.Log(2)) + p[1] * (Math.Log(p[1]) / Math.Log(2)) + p[2] * (Math.Log(p[2]) / Math.Log(2)));
                w = (double)kvp.Value.count / (double)list.Count;

                wh = wh + (w * h);
            }

            IG.Add(new IgBO(1.0 - wh, cgpa,"cgpa"));

            wh = 0;
            foreach (KeyValuePair<char, ProbBO> kvp in fsc)
            {
                double[] p = new double[classCount];
                p[0] = kvp.Value.probA;
                p[1] = kvp.Value.probB;
                p[2] = kvp.Value.probC;

                //Console.WriteLine(p[0] + "=p0");
                //Console.WriteLine(p[1] + "=p1");
                //Console.WriteLine(p[2] + "=p2");

                double h, w;
                if (p[0] == 0 || p[1] == 0 || p[2] == 0)
                {
                    h = 0.0;
                }
                else
                h = -1 * (p[0] * (Math.Log(p[0]) / Math.Log(2)) + p[1] * (Math.Log(p[1]) / Math.Log(2)) + p[2] * (Math.Log(p[2]) / Math.Log(2)));
                //Console.WriteLine(h + "=h");
                w = (double)kvp.Value.count / (double)list.Count;
                //Console.WriteLine(w + "=w");
                wh = wh + (w * h);
                //Console.WriteLine(wh + "=wh");
            }
            //Console.WriteLine(wh + "=f\n****");
          
            IG.Add(new IgBO(1.0 - wh, fsc,"fsc"));

            wh = 0;
            foreach (KeyValuePair<char, ProbBO> kvp in met)
            {
                double[] p = new double[classCount];
                p[0] = kvp.Value.probA;
                p[1] = kvp.Value.probB;
                p[2] = kvp.Value.probC;

                double h, w;
                if (p[0] == 0 || p[1] == 0 || p[2] == 0)
                {
                    h = 0.0;
                }
                else
                h = -1 * (p[0] * (Math.Log(p[0]) / Math.Log(2)) + p[1] * (Math.Log(p[1]) / Math.Log(2)) + p[2] * (Math.Log(p[2]) / Math.Log(2)));
                w = (double)kvp.Value.count / (double)list.Count;

                wh = wh + (w * h);
            }
            IG.Add(new IgBO(1.0 - wh, met,"met"));


            //IG.Sort();
            /*var temp = from element in IG
                          orderby element.ig
                          select element;*/


            //makeTree(IG);
            List<IgBO> newIG = new List<IgBO>();
            
            foreach (var val in temp)
            {
                newIG.Add(new IgBO(val.ig, val.att,val.name));
                //Console.WriteLine(val.ig);
            }
            //Console.WriteLine("****");
            foreach (var val in newIG)
            {
                
                Console.WriteLine(val.ig+" "+val.name);
            }
            makeTree(newIG);
        }

        static void Main(string[] args)
        {
            //File obj = new File();
            obj.read();
            list = obj.train();

            train(list);
        }
    }
}
