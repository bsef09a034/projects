using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace DT
{
    class File
    {
        public StreamReader sr = null;
        public List<DataBO> list = new List<DataBO>();

        //Dictionary<string, ProbBO> day = new Dictionary<string, ProbBO>();
        //Dictionary<string, ProbBO> season = new Dictionary<string, ProbBO>();
        //Dictionary<string, ProbBO> wind = new Dictionary<string, ProbBO>();
        //Dictionary<string, ProbBO> rain = new Dictionary<string, ProbBO>();
        //int p = 0;
        public List<DataBO> read()
        {
            try
            {

                sr = new StreamReader("input.txt");
                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split('\t');
                    DataBO temp = new DataBO();
                    temp.reff = Convert.ToChar (words[0]);
                    temp.intVw = Convert.ToChar(words[1]);
                    temp.cgpa = Convert.ToChar(words[2]);
                    temp.fsc = Convert.ToChar(words[3]);
                    temp.met = Convert.ToChar(words[4]);
                    temp.rank = Convert.ToChar(words[5]);

                    list.Add(temp);
                    //if (temp.clas == "Ontime") p++;
                    //if (day.ContainsKey(temp.day))
                    //{

                    //}
                    //else
                    //{
                    //}

                }


            }
            catch
            {
                Console.WriteLine("Error! file not found.");
            }
            sr.Close();

            return list;

        }
        public List<DataBO> train()
        {
            return list.GetRange(0, 20);
        }
        //public List<DataBO> test()
        //{
        //    //return list.GetRange(4, 5);
        //}
    }
}
