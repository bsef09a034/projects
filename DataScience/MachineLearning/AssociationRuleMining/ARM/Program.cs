﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARM
{
    class Program
    {
        static File obj = new File();
        static public List<List<string>> data;
        static Dictionary<string, int> firstRec = new Dictionary<string, int>();
        static List<Node> baseRec = new List<Node>();
        static public List<List<Node>> FIS=new List<List<Node>> ();
        static public List<AGNode> AL = new List<AGNode>();
        static int count(string s, char c)
        {
            int retcount = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == c) retcount++;
            }
            return retcount;
        }
        static List<Node> GetAllCombination(List<string> data, int size,bool prunOp=true)
        {
            List<Node> retlist=new List<Node> ();
            int len = data.Count;
            string startString = "1".PadLeft(len, '0');
            while (startString != string.Empty)
            {
                if (count(startString, '1') == size)
                {
                    Node temp = new Node();
                    for (int i = 0; i < len; i++)
                    {

                        if (startString[i] == '1')
                        {
                            temp.list.Add(data[i]);
                        }

                    }
                    if (prunOp == true)
                    {
                        if (prun(ref temp))
                            retlist.Add(temp);
                    }
                    else 
                    {
                        retlist.Add(temp);
                    }
                }

                startString = GetNexString(startString);

            }
            return retlist;

        }

        static string GetNexString(string str)
        {
            int pos0 = str.LastIndexOf('0');
            if (pos0 == -1) return string.Empty;
            int len = str.Length;
            str = str.Substring(0, pos0) + "1";
            return str.PadRight(len, '0');
        }
        static void freqIS(List<string> ls, List<Node> l)
        {
            FIS.Add(l);
            int setSize = 2;
            List<Node> ln;
            do
            {
                ls.Sort();
                ln = GetAllCombination(ls, setSize);
              
                setSize++;
                ls.Clear();

                for (int i = 0; i < ln.Count; i++)
                {
                    for (int j = 0; j < ln[i].list.Count; j++)
                    {
                        if (!ls.Contains(ln[i].list[j]))
                            ls.Add(ln[i].list[j]);
                    }
                }
                FIS.Add(ln);
            } while (ln.Count > 1);

            

        }
        static int countInDB(List<string> ln)
        {
            int retVal = 0;
            bool chk = true;
            for (int i = 0; i < data.Count; i++)
            {
                chk = true;
                
                for ( int j = 0; j < ln.Count&&chk; j++)
                {
                    if (!data[i].Contains(ln[j]))
                    {
                        chk = false;
                    }
                }
                if (chk == true)
                {
                    retVal++;
                }
                
            }
            return retVal;
        }
        static bool prun(ref Node n)
        {
            double count = countInDB(n.list);
            if ((double)count / (double)data.Count > Defs.supThreash)
            {
                n.count = (int)count;
                return true;
            }
            else return false;

        }
       
        static Dictionary<string, int> prun(Dictionary<string, int> d)
        {
            Dictionary<string, int> retList = new Dictionary<string, int>();
            foreach (KeyValuePair<string,int> kvp in d)
            {
                if ((double)kvp.Value / (double)data.Count > Defs.supThreash)
                {
                    retList.Add(kvp.Key,kvp.Value);
                }
            }
            return retList;
        }
        static bool contain(List<string> a, List<string> b)
        {
            bool chk=true;
            for (int i = 0; i < b.Count&&chk; i++)
            {
                if (!a.Contains(b[i])) { chk = false; } 
            }
            return chk;
        }
        static int giveCount(List<string> l)
        {
            int retVal = -1;
            
            retVal = countInDB(l);
            return retVal;
        }
        static double computeConfidence(List<string> left, List<string> right)
        {
            
            int x = giveCount(left);
            List<string> temp = new List<string>();
            temp.AddRange(left);
            temp.AddRange(right);
            int y = giveCount(temp);
            return (double)y / (double)x;


        }
        static List<AGNode> ARGPrun(List<Node> leftResult, List<Node> rightResult)
        {
            List<AGNode> retList = new List<AGNode>();
            for (int i = 0; i < leftResult.Count;i++ )
            {
                for (int j = 0; j < rightResult.Count; j++)
                {
                    if(! (contain(leftResult[i].list,rightResult[j].list)) )
                    {
                        AGNode tempNode = new AGNode();
                        tempNode.from = leftResult[i].list;
                        tempNode.to = rightResult[j].list;

                        tempNode.confidence = computeConfidence(leftResult[i].list, rightResult[j].list);
                        if (!contain(tempNode.to, tempNode.from))
                        {

                            if ((double)tempNode.confidence >Defs.confThreash)
                            {
                                
                                retList.Add(tempNode);
                            }
                        }
                    }
                    

                }
            }
            
            return retList;
        }
        static void asRuleGen()
        {
            List<string> left, right;
            List<Node> leftResult, rightResult;
            int leftCount = 0, rightCount = 0;
            for (int i = 0; i < FIS.Count; i++)
            {
                
                for(int j=0;j<FIS[i].Count;j++)
                {
                    leftCount = FIS[i][j].list.Count - 1;
                    rightCount = 1;
                    left = FIS[i][j].list;
                    right = FIS[i][j].list;
                    while (leftCount >=1)
                    {
                        leftResult = GetAllCombination(left, leftCount, false);
                        rightResult = GetAllCombination(right, rightCount, false);
                        List<AGNode> l= ARGPrun(leftResult, rightResult);
                        
                        AL.AddRange(l);
                        
                        left.Clear();
                        right.Clear();

                        for (int ii = 0; ii < l.Count; ii++)
                        {
                            for (int jj = 0; jj < l[ii].from.Count; jj++)
                            {
                                if (!left.Contains(l[ii].from[jj])) left.Add(l[ii].from[jj]);
                            }
                        }
                        right = left;

                        left.Sort();
                        right.Sort();
                        leftCount--;
                        rightCount++;

                    }
                }
            }
        }
        static void Main(string[] args)
        {
            obj.read();
            data=obj.train();

            for (int i = 0; i < data.Count; i++)
            {
                for (int j = 0; j < data[i].Count; j++)
                {
                    if (!firstRec.ContainsKey(data[i][j]))
                    {
                        firstRec.Add(data[i][j], 1);
                    }
                    else
                    {
                        firstRec[data[i][j]]++;
                    }
                }
            }
            
            firstRec=prun(firstRec);
            
            List<string> arr=new List<string>();
           
            
            List<Node> ln = new List<Node>();
            foreach (KeyValuePair<string, int> kvp in firstRec)
            {
                Node n = new Node();
                n.count = kvp.Value;
                n.list.Add(kvp.Key);
                ln.Add(n);
                arr.Add(kvp.Key);
                
            }
            Console.WriteLine(" ******  ");
            arr.Sort();
            freqIS(arr,ln);
            for (int i = 0; i < FIS.Count; i++)
            {
                for (int j = 0; j < FIS[i].Count; j++)
                {
                    for (int k = 0; k < FIS[i][j].list.Count; k++)
                        Console.Write(FIS[i][j].list[k]);
                    Console.Write("\t");
                }
                Console.WriteLine();

            }
            Console.WriteLine(" ******  ");
            
            asRuleGen();

            foreach (var val in AL)
            {
                foreach (var v in val.from)
                {
                    Console.Write(v);
                }
                Console.Write("==>>");
                foreach (var v in val.to)
                {
                    Console.Write(v);
                }
                Console.WriteLine("\t"+val.confidence);
            }
            Console.WriteLine(" ******  ");
        }
    }
}
