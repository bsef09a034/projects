﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ARM
{
    class File
    {
        public StreamReader sr = null;
        public List<List<string>> list = new List<List<string>>();
        public List<string> data = new List<string>();

        public List<List<string>> read()
        {
            try
            {

                sr = new StreamReader("input.txt");
                sr.ReadLine();
                sr.ReadLine();

                for (int i = 0; i < 11; i++)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split(' ');
                    int c = 1;
                    data.Add(words[c]);
                }


                sr.ReadLine();
                sr.ReadLine();
                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split(',');
                    List<string> temp = new List<string>();
                    for (int i = 0; i < data.Count;i++ )
                    {
                        if(words[i]=="T")
                            temp.Add(data[i]);

                    }

                    list.Add(temp);
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();

            return list;

        }
        public List<List<string>> train()
        {
            return list.GetRange(0, 500);
        }
        public List<List<string>> test()
        {
            return list.GetRange(0, 0);
        }
    }
}
