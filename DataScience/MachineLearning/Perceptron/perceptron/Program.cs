﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace perceptron
{
    class Program
    {
        static List<double> weights = new List<double>();
        static List<List<int>> list;
        static double maximum=1,minimum=-1, alpha=0.2;
        static List<double> myY = new List<double>();
        static int stepFunc(double d)
        {
            if (d <= 0) return 0;
            else return 1;
        }
        static void test(List<List<int>> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                double sum = 0;
                for (int j = 0; j < l[i].Count-1; j++)
                {
                    sum = sum + l[i][j] * weights[j];
                }
                sum = sum - 0.2;
                sum = stepFunc(sum);
                Console.WriteLine(sum + " " + l[i][l[i].Count - 1]);

                double e = l[i][l[i].Count - 1] - sum;
                Console.WriteLine("Error= " + e);

            }
        }
        static Random r = new Random();
        static void Main(string[] args)
        {
            File obj = new File();
            obj.read();
            list=obj.train();
            for (int i = 0; i < list[0].Count-1; i++)
            {
                weights.Add(r.NextDouble() * (maximum - minimum) + minimum);
            }
            bool chk=true;
            double[] arr = new double[10];
            int arrCount = 0;
            double avg1 = 0,avg2=0;
            while (chk)
            {
                chk = false;
                for (int i = 0; i < list.Count; i++)
                {
                    double sum = 0;
                    for (int j = 0; j < list[i].Count-1; j++)
                    {
                        sum = sum + (double)list[i][j] * weights[j];
                    }
                    sum = sum - 0.2;
                    sum = stepFunc(sum);
                    Console.WriteLine(sum + " " + list[i][list[i].Count - 1]);
                    
                    if (sum != list[i][list[i].Count - 1])
                    {
                        chk = true;
                        double e = list[i][list[i].Count - 1] - sum;
                        for (int j = 0; j < list[0].Count-1; j++)
                        {
                            weights[j] = weights[j] + list[i][j] * alpha * e;

                        }
                        if (arrCount < 10)
                        {
                            arr[arrCount++] = e;

                        }
                        else
                        {
                            avg2 = arr.Average();
                            if (avg1 == avg2)
                            {
                                chk = false;
                            }
                            else
                            {
                                avg1 = avg2;
                            }
                            arrCount = 0;
                        }
                        
                    }


                }
                Console.WriteLine("*******");

                alpha = alpha /2;
                
            }
            //test(obj.test());
        }
    }
}
