﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace perceptron
{
    
    class File
    {
       
        List<List<int>> list = new List<List<int>>();
        StreamReader sr;
        public List<List<int>> read()
        {
            try
            {

                sr = new StreamReader("input.txt");
                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split('\t');
                    List<int> temp = new List<int> ();
                    foreach (var val in words)
                    {
                        temp.Add(Convert.ToInt32( val));
                    }

                    list.Add(temp);

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();
            return list;
        }
        public List<List<int>> train()
        {
            return list.GetRange(0, 4);
        }
        public List<List<int>> test()
        {
            return list.GetRange(0, 4);
        }

    }
}
