﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LR
{
    class Program
    {
        static fileRead obj = new fileRead();
        static void test(int m,int c)
        {
            double temp = 0;
            List<BO> list = obj.test();
            
            Console.WriteLine("Size ActPrice PredictedPrice");
            foreach (var val in list)
            {
                Console.WriteLine(val.size + "\t" + val.price + "\t" + (m * val.size + c));
                temp = temp + Math.Abs(val.price - (m * val.size + c));
            }
            Console.WriteLine("avg=" +(temp/list.Count));
        }
        static void Main(string[] args)
        {
            List<BO> list;
            
            obj.read();
            list = obj.train();


            int minm, minc,maxm,maxc;
            double avg=double.MaxValue,error=0;
            int resM=0,resC=0,x1,y1;

            Console.WriteLine("Enter min m");
            minm = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter min c");
            minc = Convert.ToInt32(Console.ReadLine());
            
            Console.WriteLine("Enter max m");
            maxm = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter max c");
            maxc = Convert.ToInt32(Console.ReadLine());

            for (int i = minm; i < maxm; i++)
            {
                
                for (int j = minc; j < maxc; j++)
                {
                    error = 0;
                    for (int k = 0; k < list.Count; k++)
                    {
                        x1 = list[k].size;
                        y1 = i * x1 + j;
                        error = error + (Math.Abs(y1 - list[k].price));
                                            

                    }
                    error = error / list.Count;
                    if (avg > error)
                    {
                        avg = error;
                        resM = i;
                        resC = j;
                    }
                }
            }

            Console.WriteLine("avg is " + avg);
            Console.WriteLine("m is " + resM);
            Console.WriteLine("c is " + resC);

            test(resM, resC);
        }
    }
}
