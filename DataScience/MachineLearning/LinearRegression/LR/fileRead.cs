﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace LR
{
    class fileRead
    {
        public StreamReader sr = null;
        public List<BO> list=new List<BO>();
        public void read()
        {
            try
            {
                list = new List<BO>();
                sr = new StreamReader("input.txt");
                while (!sr.EndOfStream)
                {
                    string s=sr.ReadLine();
                    string []words=s.Split('\t');
                    BO temp = new BO();
                    temp.size = Convert.ToInt32(words[0]);
                    temp.price = Convert.ToInt32(words[1]);
                    list.Add(temp);
                    
                }

                
            }
            catch 
            {
                Console.WriteLine("Error!");
            }
            sr.Close();
            
        }
        public List<BO> train()
        {
            return list.GetRange(0, 5);
        }
        public List<BO> test()
        {
            return list.GetRange(4,5);
        }


    }
}
