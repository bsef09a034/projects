﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MNN
{
    class Program
    {
        static Random r = new Random();

        static public List<List<double>> ID;
        static public List<List<double>> OD;
        static File obj = new File();
        static List<Node> IL, HL, OL;
        static List<double> X,Y,gradX,gradY;
        static double[] arr = new double[10];
        static int arrCount = 0;
        static double avg1=0,avg2=0,error=0;
        static List<Node> initializeWeights(int a,int b)
        {
            List<Node> retList = new List<Node>();
            double rno = 0;
            for (int i = 0; i < a; i++)
            {
                Node temp = new Node();
                for (int j = 0; j < b; j++)
                {
                    rno = r.NextDouble() * (Def.max - Def.min) + Def.min;
                    temp.weight.Add(rno);
                }
                retList.Add(temp);
            }
            return retList;

        }
        static void fill(ref List<Node> l, List<double> d)
        {
            
            for (int i = 0; i < l.Count ; i++)
            {
                l[i].val=d[i];
            }

        }
        static double computeSum(List<Node> l, int n,double threash=0)
        {
            double sum = 0;
            for (int i = 0; i < l.Count; i++)
            {
                sum=sum+(l[i].val*l[i].weight[n]);
            }
            return sum;
        }
        static double sum(Node n,List<double> grad)
        {
            double sum = 0;
            for (int i = 0; i < n.weight.Count; i++)
            {
                try
                {
                    sum = sum + (grad[i] * n.weight[i]);
                }
                catch { }
            }
            return sum;
        }
        static double sigmoid(double x)
        {
            return 1.0 / (1.0 + (double)Math.Exp(-x));
        }
        static void test()
        {
            Console.WriteLine("actual" + "predicted");
            for (int i = 0; i < ID.Count; i++)
            {
                
                fill(ref IL, ID[i]);
                X = new List<double>();
                for (int j = 0; j < Def.HLCount; j++)
                {
                    double s = computeSum(IL, j);
                    X.Add(s);
                    HL[j].val = sigmoid(s);
                }
                Y = new List<double>();
                
                for (int j = 0; j < Def.OLCount; j++)
                {
                    
                    double s = computeSum(HL, j, Def.threash);
                    if (!double.IsNaN(s))
                    {
                        Y.Add(s);
                        if (!double.IsNaN(sigmoid(s)))
                        {
                            OL[j].val = sigmoid(s);
                            double error = OD[i][j] - OL[j].val;
                            char a = ' ', p = ' ';
                            if (OD[i][j] >= 0 && OD[i][j] <= 0.33) a = 'L';
                            else if (OD[i][j] > 0.33 && OD[i][j] <= 0.66) a = 'M';
                            else if (OD[i][j] > 0.66 && OD[i][j] <= 0.99) a = 'H';

                            if (OL[j].val >= 0 && OL[j].val <= 0.33) p = 'L';
                            else if (OL[j].val > 0.33 && OL[j].val <= 0.66) p = 'M';
                            else if (OL[j].val > 0.66 && OL[j].val <= 0.99) p = 'H';

                            Console.WriteLine(a + "\t" + p);
                            
                        }
                    }
                }
                
            }
        }
        static void createGraph()
        {
            bool chk = true;
            List<int> adj=new List<int> ();
            
            IL = initializeWeights(Def.ILCount, Def.HLCount);
            HL = initializeWeights(Def.HLCount, Def.OLCount);
            OL = initializeWeights(Def.OLCount, 0);
            do
            {
            for (int i = 0; i < ID.Count; i++)
            {
                fill(ref IL, ID[i]);
                X = new List<double>();
                for (int j = 0; j < Def.HLCount; j++)
                {
                    double s = computeSum(IL, j);
                    X.Add(s);
                    HL[j].val = sigmoid(s);  
                }
                Y = new List<double>();
                adj = new List<int>();
                gradY = new List<double>();
                for (int j = 0; j < Def.OLCount; j++)
                {
                    double s = computeSum(HL, j,Def.threash);
                    if (!double.IsNaN(s))
                    {
                        Y.Add(s);

                        if (!double.IsNaN(sigmoid(s)))
                        {
                            OL[j].val = sigmoid(s);
                            error = OD[i][j] - OL[j].val;
                            //Console.WriteLine(OD[i][j] + "\t" + OL[j].val);
                            if (error != 0.0)
                            {
                                adj.Add(j);
                                gradY.Add(Y[j] * (1.0 - Y[j] * error));
                            }
                        }
                    }
                }
                for (int j = 0; j < gradY.Count; j++)
                {
                    for (int k = 0; k < Def.HLCount; k++)
                    {
                        HL[k].weight[adj[j]] = HL[k].weight[adj[j]] + HL[k].val * Def.alpha * gradY[j];
                    }
                }
                gradX = new List<double>();

                for (int j = 0; j < X.Count; j++)
                {
                    double s = sum(HL[j],gradY);
                    gradX.Add(X[j]*(1.0-X[j]*s));
                }

                for (int j = 0; j < Def.ILCount; j++)
                {
                    for (int k = 0; k <IL[j].weight.Count; k++)
                    {
                        IL[j].weight[k] = IL[j].weight[k] + IL[k].val * Def.alpha * gradX[k];
                    }
                }


            }
            Def.alpha = Def.alpha / 2;
            if (arrCount < 10)
            {
                arr[arrCount++] = error;

            }
            else
            {
                avg2 = arr.Average();
                if (avg1 == avg2)
                {
                    chk = false;
                }
                else
                {
                    avg1 = avg2;
                }
                arrCount = 0;
            }
            }while(adj.Count>0&&chk);
            


        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter threshhold");
            Def.threash =double.Parse( Console.ReadLine());

            //Console.WriteLine("Enter threshhold");
            //Def.threash = int.Parse(Console.ReadLine());

            obj.read();
            ID = obj.IDtrain();
            OD = obj.ODtrain();

            createGraph();
            test();
        }
    }
}
