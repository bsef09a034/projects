using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace MNN
{
    class File
    {
        
        public StreamReader sr = null;
        public List<List<double>> ID = new List<List<double>>();
        public List<List<double>> OD = new List<List<double>>();
        
        public void read()
        {
            try
            {

                sr = new StreamReader("input.txt");
                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] words = s.Split('\t');
                    List<double> t1 = new List<double>();

                    int i=0;
                    for (i = 0; i < Def.ILCount;i++ )
                    {
                        double temp=0.0;
                        char c = Convert.ToChar(words[i]);
                        if (c == 'H') temp = 0.99;
                        else if (c == 'M') temp = 0.66;
                        else if (c == 'L') temp = 0.33;
                        t1.Add(temp);
                    }

                    ID.Add(t1);

                    List<double> t2 = new List<double>();

                    for (; i < Def.OLCount; i++)
                    {
                        double temp = 0.0;
                        char c = Convert.ToChar(words[i]);
                        if (c == 'H') temp = 0.99;
                        else if (c == 'M') temp = 0.66;
                        else if (c == 'L') temp = 0.33;
                        t2.Add(temp);
                    }

                    OD.Add(t2);
                }


            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            sr.Close();


        }
        public List<List<double>> ODtrain()
        {
            return ID.GetRange(0, 6);
        }
        public List<List<double>> IDtrain()
        {
            return ID.GetRange(0, 6);
        }
        public List<List<double>> ODtest()
        {
            return ID.GetRange(6, 6);
        }
        public List<List<double>> IDtest()
        {
            return ID.GetRange(6, 6);
        }
    }
}
